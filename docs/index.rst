Welcome to Meleager's documentation!
====================================

.. toctree::
   :caption: Installation and usage
   :maxdepth: 4

   usage/installation
   usage/customization

.. toctree::
   :caption: Model reference

   models

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
