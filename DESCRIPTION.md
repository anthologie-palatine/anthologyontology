
# Organisation du model :

Le model de données à été concu pour :
    - Fonctionner avec le modèle de données de Zach
    - Permettre à Marcello d'importer ses données depuis son ancienne application `anthologie-API`

Au début, le modèle a été concu dans cette optique : cf [diagram](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZXJEaWFncmFtXG4gICAgICAgIEFCU1RSQUNUQkFTRU1PREVMIHx8LS1veyBVU0VSIDogXCJjcmVhdG9yXCJcbiAgICAgICAgVVNFUiB9fC0tb3sgVVNFUi1HUk9VUDogXCJpcyBpbiBncm91cFwiXG4gICAgICAgIFVTRVItR1JPVVAgfW8tLXx8IFBFUk1JU1NJT04gOiBcImZvciBncm91cFwiXG4gICAgICAgIENPTExFQ1RJT04gfW8tLW97IEFCU1RSQUNUQkFTRU1PREVMOiBcImNvbXBvc2VkIG9mXCJcbiAgICAgICAgUEVSTUlTU0lPTiB8fC0tb3sgQ09MTEVDVElPTjogXCJvbiBjb2xsZWN0aW9uXCJcbiAgICAgICAgQUJTVFJBQ1RCQVNFTU9ERUwgfHwtLW98IFZBTElEQVRJT04tTEVWRUw6IFwiY3VycmVudCB2YWxpZGF0aW9uIGxldmVsXCJcbiAgICAgICAgUEVSTUlTU0lPTiB8fC0tb3sgVkFMSURBVElPTi1MRVZFTDogXCJ2YWxpZGF0aW9uIGxldmVsXCJcblxuXG5cblxuICAgICAgICBBQlNUUkFDVFBBU1NBR0UgfXwtLW97IFRFWFQ6IFwidGV4dHNcIiAgICAgICAgXG4gICAgICAgIEFCU1RSQUNUUEFTU0FHRSB9fC0tb3sgS0VZV09SRDogXCJrZXl3b3Jkc1wiIFxuICAgICAgICBBQlNUUkFDVFBBU1NBR0UgfXwtLW97IENPTU1FTlQ6IFwiY29tbWVudHNcIiBcbiAgICAgICAgQUJTVFJBQ1RQQVNTQUdFIH18LS1veyBJTUFHRTogXCJpbWFnZXNcIlxuICAgICAgICBBQlNUUkFDVFBBU1NBR0UgfXwtLW97IElNQUdFOiBcImltYWdlIGR1IG1hbnVzY3JpcHRcIlxuICAgICAgICBBQlNUUkFDVFBBU1NBR0UgfXwtLW97IENJVFk6IFwiY2l0ZWRfY2l0aWVzXCJcbiAgICAgICAgQUJTVFJBQ1RQQVNTQUdFIHx8LS18fCBQQVNTQUdFOiBcImlzIHN1cGVydHlwZSBvZlwiXG4gICAgICAgIEFCU1RSQUNUUEFTU0FHRSB8fC0tfHwgU0NIT0xJT046IFwiaXMgc3VwZXJ0eXBlIG9mXCJcbiAgICAgICAgUEFTU0FHRSB8fC0tb3sgRlJBR01FTlQgOiBcInRleHQgZnJhZ21lbnQgYm91bmRhcmllc1wiXG5cbiAgICAgICAgUEFTU0FHRSB8fC0tfHsgV09SSzogXCJ3b3JrXCJcblxuICAgICAgICBQQVNTQUdFIH1vLS18eyBBTFRFUk5BVElWRVVSSTogXCJhbHRlcm5hdGl2ZV91cmlzXCJcbiAgICAgICAgUEFTU0FHRSB8fC0tfHsgQVVUSE9SOiBcImF1dGhvclwiXG5cbiAgICAgICAgUEFTU0FHRSB9fC0tb3sgRVhURVJOQUxSRUY6IFwiZXh0ZXJuYWxfcmVmZXJlbmNlc1wiXG5cbiAgICAgICAgUEFTU0FHRSB9fC0tb3sgU0NIT0xJT046IFwic2Nob2xpZXNcIlxuXG4gICAgICAgIFBBU1NBR0UgfXwtLW97IFBBU1NBR0U6IFwiaW50ZXJuYWxfcmVmZXJlbmNlc1wiXG4gICAgICAgIFxuICAgICAgICBXT1JLIH18LS1veyBERVNDUklQVElPTiA6IFwiZGVzY3JpcHRpb24gdGFibGVcIlxuICAgICAgICBURVhUIHx8LS1veyBFRElUSU9OIDogXCJlZGl0aW9uXCJcbiAgICAgICAgVEVYVCB8fC0tfHsgQ09NTUVOVCA6IFwiY29tbWVudFwiXG5cblxuICAgICAgICBFRElUSU9OIH18LS1veyBERVNDUklQVElPTiA6IFwiZGVzY3JpcHRpb25zXCJcbiAgICAgICAgRURJVElPTiB9fC0tb3sgRURJVE9SOiBcImVkaXRvcnNcIlxuXG4gICAgICAgIENPTU1FTlQgfXwtLW97IERFU0NSSVBUSU9OIDogXCJkZXNjcmlwdGlvbnNcIlxuICAgICAgICBDT01NRU5UIHxvLS1veyBGUkFHTUVOVCA6IFwidGFyZ2V0XCJcbiAgICAgICAgQ09NTUVOVCB8fC0tb3sgRURJVElPTjogXCJlZGl0aW9uXCJcblxuICAgICAgICBcbiAgICAgICAgQVVUSE9SIHx8LS1veyBDSVRZOiBcImNpdHlfYm9yblwiXG4gICAgICAgIEFVVEhPUiB8fC0tb3sgQ0lUWTogXCJjaXR5X2RpZWRcIlxuICAgICAgICBBVVRIT1IgfXwtLW97IERFU0NSSVBUSU9OIDogXCJkZXNjcmlwdGlvbnNcIlxuICAgICAgICBDSVRZIH1vLS1veyBJTUFHRTogXCJpbWFnZXNcIlxuXG4gICAgICAgIENJVFkgfXwtLW97IERFU0NSSVBUSU9OOiBcImRlc2NyaXB0aW9uXCJcbiAgICAgICAgQ0lUWSB9by0tb3sgQUxURVJOQVRJVkVVUkk6IFwiYWx0ZXJuYXRpdmVfdXJpc1wiXG5cbiAgICAgICAgQUxJR05NRU5UIHx8LS1veyBURVhUOiBcInRleHQxXCJcbiAgICAgICAgQUxJR05NRU5UIHx8LS1veyBURVhUOiBcInRleHQyXCJcbiAgICAgICAgSU1BR0UgfXwtLW97IERFU0NSSVBUSU9OOiBcImRlc2NyaXB0aW9uXCJcbiAgICAgICAgS0VZV09SRCB9by0tb3sgS0VZV09SRENBVEVHT1JZOiBcImNhdGVnb3JpZXNcIlxuICAgICAgICBLRVlXT1JEIH18LS1veyBERVNDUklQVElPTjogXCJkZXNjcmlwdGlvbnNcIlxuICAgICAgICBLRVlXT1JEIH18LS1veyBJTUFHRTogXCJpbWFnZXNcIlxuICAgICAgICBLRVlXT1JEIHxvLS1veyBBTFRFUk5BVElWRVVSSTogXCJhbHRlcm5hdGl2ZV91cmlzXCJcbiAgICAgICAgS0VZV09SRENBVEVHT1JZICB9fC0tb3sgREVTQ1JJUFRJT046IFwiZGVzY3JpcHRpb25zXCJcbiAgICAgICAgRVhURVJOQUxSRUYgfXwtLW97IERFU0NSSVBUSU9OOiBcImRlc2NyaXB0aW9uc1wiXG4gICAgICAgICIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0IiwidGhlbWVWYXJpYWJsZXMiOnsiYmFja2dyb3VuZCI6IndoaXRlIiwicHJpbWFyeUNvbG9yIjoiI0VDRUNGRiIsInNlY29uZGFyeUNvbG9yIjoiI2ZmZmZkZSIsInRlcnRpYXJ5Q29sb3IiOiJoc2woODAsIDEwMCUsIDk2LjI3NDUwOTgwMzklKSIsInByaW1hcnlCb3JkZXJDb2xvciI6ImhzbCgyNDAsIDYwJSwgODYuMjc0NTA5ODAzOSUpIiwic2Vjb25kYXJ5Qm9yZGVyQ29sb3IiOiJoc2woNjAsIDYwJSwgODMuNTI5NDExNzY0NyUpIiwidGVydGlhcnlCb3JkZXJDb2xvciI6ImhzbCg4MCwgNjAlLCA4Ni4yNzQ1MDk4MDM5JSkiLCJwcmltYXJ5VGV4dENvbG9yIjoiIzEzMTMwMCIsInNlY29uZGFyeVRleHRDb2xvciI6IiMwMDAwMjEiLCJ0ZXJ0aWFyeVRleHRDb2xvciI6InJnYig5LjUwMDAwMDAwMDEsIDkuNTAwMDAwMDAwMSwgOS41MDAwMDAwMDAxKSIsImxpbmVDb2xvciI6IiMzMzMzMzMiLCJ0ZXh0Q29sb3IiOiIjMzMzIiwibWFpbkJrZyI6IiNFQ0VDRkYiLCJzZWNvbmRCa2ciOiIjZmZmZmRlIiwiYm9yZGVyMSI6IiM5MzcwREIiLCJib3JkZXIyIjoiI2FhYWEzMyIsImFycm93aGVhZENvbG9yIjoiIzMzMzMzMyIsImZvbnRGYW1pbHkiOiJcInRyZWJ1Y2hldCBtc1wiLCB2ZXJkYW5hLCBhcmlhbCIsImZvbnRTaXplIjoiMTZweCIsImxhYmVsQmFja2dyb3VuZCI6IiNlOGU4ZTgiLCJub2RlQmtnIjoiI0VDRUNGRiIsIm5vZGVCb3JkZXIiOiIjOTM3MERCIiwiY2x1c3RlckJrZyI6IiNmZmZmZGUiLCJjbHVzdGVyQm9yZGVyIjoiI2FhYWEzMyIsImRlZmF1bHRMaW5rQ29sb3IiOiIjMzMzMzMzIiwidGl0bGVDb2xvciI6IiMzMzMiLCJlZGdlTGFiZWxCYWNrZ3JvdW5kIjoiI2U4ZThlOCIsImFjdG9yQm9yZGVyIjoiaHNsKDI1OS42MjYxNjgyMjQzLCA1OS43NzY1MzYzMTI4JSwgODcuOTAxOTYwNzg0MyUpIiwiYWN0b3JCa2ciOiIjRUNFQ0ZGIiwiYWN0b3JUZXh0Q29sb3IiOiJibGFjayIsImFjdG9yTGluZUNvbG9yIjoiZ3JleSIsInNpZ25hbENvbG9yIjoiIzMzMyIsInNpZ25hbFRleHRDb2xvciI6IiMzMzMiLCJsYWJlbEJveEJrZ0NvbG9yIjoiI0VDRUNGRiIsImxhYmVsQm94Qm9yZGVyQ29sb3IiOiJoc2woMjU5LjYyNjE2ODIyNDMsIDU5Ljc3NjUzNjMxMjglLCA4Ny45MDE5NjA3ODQzJSkiLCJsYWJlbFRleHRDb2xvciI6ImJsYWNrIiwibG9vcFRleHRDb2xvciI6ImJsYWNrIiwibm90ZUJvcmRlckNvbG9yIjoiI2FhYWEzMyIsIm5vdGVCa2dDb2xvciI6IiNmZmY1YWQiLCJub3RlVGV4dENvbG9yIjoiYmxhY2siLCJhY3RpdmF0aW9uQm9yZGVyQ29sb3IiOiIjNjY2IiwiYWN0aXZhdGlvbkJrZ0NvbG9yIjoiI2Y0ZjRmNCIsInNlcXVlbmNlTnVtYmVyQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvciI6InJnYmEoMTAyLCAxMDIsIDI1NSwgMC40OSkiLCJhbHRTZWN0aW9uQmtnQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvcjIiOiIjZmZmNDAwIiwidGFza0JvcmRlckNvbG9yIjoiIzUzNGZiYyIsInRhc2tCa2dDb2xvciI6IiM4YTkwZGQiLCJ0YXNrVGV4dExpZ2h0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0RGFya0NvbG9yIjoiYmxhY2siLCJ0YXNrVGV4dE91dHNpZGVDb2xvciI6ImJsYWNrIiwidGFza1RleHRDbGlja2FibGVDb2xvciI6IiMwMDMxNjMiLCJhY3RpdmVUYXNrQm9yZGVyQ29sb3IiOiIjNTM0ZmJjIiwiYWN0aXZlVGFza0JrZ0NvbG9yIjoiI2JmYzdmZiIsImdyaWRDb2xvciI6ImxpZ2h0Z3JleSIsImRvbmVUYXNrQmtnQ29sb3IiOiJsaWdodGdyZXkiLCJkb25lVGFza0JvcmRlckNvbG9yIjoiZ3JleSIsImNyaXRCb3JkZXJDb2xvciI6IiNmZjg4ODgiLCJjcml0QmtnQ29sb3IiOiJyZWQiLCJ0b2RheUxpbmVDb2xvciI6InJlZCIsImxhYmVsQ29sb3IiOiJibGFjayIsImVycm9yQmtnQ29sb3IiOiIjNTUyMjIyIiwiZXJyb3JUZXh0Q29sb3IiOiIjNTUyMjIyIiwiY2xhc3NUZXh0IjoiIzEzMTMwMCIsImZpbGxUeXBlMCI6IiNFQ0VDRkYiLCJmaWxsVHlwZTEiOiIjZmZmZmRlIiwiZmlsbFR5cGUyIjoiaHNsKDMwNCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGUzIjoiaHNsKDEyNCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIiwiZmlsbFR5cGU0IjoiaHNsKDE3NiwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU1IjoiaHNsKC00LCAxMDAlLCA5My41Mjk0MTE3NjQ3JSkiLCJmaWxsVHlwZTYiOiJoc2woOCwgMTAwJSwgOTYuMjc0NTA5ODAzOSUpIiwiZmlsbFR5cGU3IjoiaHNsKDE4OCwgMTAwJSwgOTMuNTI5NDExNzY0NyUpIn19LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

Cependant, lors de l'importation des données de Marcello, de nombreux changements ont été
fait pour accomoder les données existantes. 

Le modèle résultant est similaire au modèle précédent avec les changements suivants:
 - plus de table `versions` : cette ancienne table contenait à la fois des traductions, des descriptions et des textes. Le nouveau modèle sépare ces objets.
 - Ajout d'une table `edition` : les éditions dans l'ancien modèle n'était pas explicite. 
    

Le code du model se sépare en plusieurs parties : 
    - les mixin `models.mixin` : les mixins sont des classes abstraites qui sont utilisées par un grand nombre de modèles
    - les autres modèles dans `models.models`.
    
Chaque Mixin définie deux classes :
    - une classe pour le type de données que le Mixin référence
    - la classe abstraite qui définie la relation vers la classe précédente.
  
Les dépendances entre les mixins sont explicitées dans `models.mixin.__init__.py`
  
J'utilise `serializers.HyperlinkedModelSerializer` comme serializer pour faciliter la navigation dans les données, 
l'ensemble des serializers se trouvent dans le module `serializers.serializers.py`.
L'ensembles des vues se trouvent dans `views.py`.
Pour le moment, il y a une vue par modèle. 


Génération d'une visualisation du modèle : 
`python manage.py graph_models -a -g -o model_visualization.png`

## Choix de PostgresSQL

Pour les positions des villes dans `models.mixin.cities_localized_mixin.py`.






# Taches restantes :
 - verification des URNs dans les passages et les alternativesURN
 - calcul des URN perseids
 - definition de `models.Contribution`
 - mettre en place les niveaux de validations : `ValidableResourceMixin`.