migrate:
	docker-compose exec web python manage.py migrate

migrations:
	docker-compose exec web python manage.py makemigrations

rm_migrations:
	find . -type f -path 'django/*/migrations/*.py' -not -name '__init__.py' -print0 | xargs -0 rm -f

rm_db:
	docker-compose stop db
	docker-compose rm -f db

migrations_from_scratch: rm_db rm_migrations migrations

import:
	docker-compose exec web python manage.py import_ap --save --local

flushdb:
	docker-compose exec web python manage.py flush --noinput
	docker-compose exec web python manage.py loaddata admin_user

reimport: flushdb import

admin_user:
	docker-compose exec web python manage.py loaddata admin_user

bash_root:
	docker-compose exec -u root web bash
bash:
	docker-compose exec web bash
shell:
	docker-compose exec web python manage.py shell_plus

integration_tests:
	pip install -r integration/requirements.txt
	python -m playwright install
	pytest integration --browser firefox --browser chromium --browser webkit --base-url http://127.0.0.1:8000
