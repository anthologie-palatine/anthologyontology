def test_submitting_passage_description_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    page.hover("#descriptions h2")
    page.click("#descriptions h2 a >> text='Add'")
    modal = page.wait_for_selector("#description-create")
    description_textarea = modal.wait_for_selector("#id_description")
    description_textarea.fill("Description")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.


def test_submitting_scholium_description_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    page.hover("#descriptions h2")
    page.click("#descriptions h2 a >> text='Add'")
    modal = page.wait_for_selector("#description-create")
    description_textarea = modal.wait_for_selector("#id_description")
    description_textarea.fill("Description")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
