def test_first_tab_is_selected(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert (
        page.text_content("#alignments-tabs .tab-container .selected").strip() == "eng"
    )


def test_first_content_is_visible(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert (
        page.inner_text(
            "#alignments-tabs #alignment-content-1-eng #align-original-1 blockquote"
        )
        .strip()
        .startswith("νῦν πλέον ἢ τὸ πάροιθε")
    )


def test_click_on_french_tab(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    tab = page.wait_for_selector('#alignments-tabs [href="#alignment-content-2-fra"]')
    tab.click()
    assert (
        page.text_content("#alignments-tabs .tab-container .selected").strip() == "fra"
    )
    assert (
        page.inner_text(
            "#alignments-tabs #alignment-content-2-fra #align-translation-2 blockquote"
        )
        .strip()
        .startswith("Maintenant plus qu ' auparavant , garde , triple chien")
    )
