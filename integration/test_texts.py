def test_submitting_passage_text_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    page.hover("#texts h2")
    page.click("#texts h2 a >> text='Add'")
    modal = page.wait_for_selector("#text-create")
    title_input = modal.wait_for_selector("#id_text")
    title_input.fill("Text content")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.


def test_submitting_scholium_text_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    page.hover("#texts h2")
    page.click("#texts h2 a >> text='Add'")
    modal = page.wait_for_selector("#text-create")
    title_input = modal.wait_for_selector("#id_text")
    title_input.fill("Text content")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
