def test_submitting_passage_author_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.71/")
    page.hover("#authors h2")
    page.click("#authors h2 a >> text='Add'")
    modal = page.wait_for_selector("#author-create")
    author_select = modal.wait_for_selector("#id_author")
    author_select.select_option(label="Adaeus, ᾿Αδαῖος, Addée ou Adaios de Macédoine")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
