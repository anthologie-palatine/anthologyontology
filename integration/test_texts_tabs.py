def test_first_tab_is_selected(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert (
        page.text_content("#texts-tabs-first .tab-container .selected").strip() == "grc"
    )


def test_first_content_is_visible(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert (
        page.text_content("#texts-tabs-first #text-content-first-1-grc")
        .strip()
        .startswith("νῦν πλέον ἢ τὸ πάροιθε")
    )


def test_click_on_french_tab(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    tab = page.wait_for_selector('#texts-tabs-first [href="#text-content-first-3-fra"]')
    tab.click()
    assert (
        page.text_content("#texts-tabs-first .tab-container .selected").strip() == "fra"
    )
    assert (
        page.text_content("#texts-tabs-first #text-content-first-3-fra")
        .strip()
        .startswith("Maintenant plus qu'auparavant")
    )
