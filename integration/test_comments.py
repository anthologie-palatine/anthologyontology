def test_open_close_comment_form_modal(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")

    # The opacity of the modal should be 0 by default.
    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "0"

    page.hover("#comments h2")
    page.click("#comments h2 a >> text='Add'")
    # Manually waiting for the .3s CSS transition.
    page.wait_for_timeout(350)

    # Once opened, the opacity is set to 1.
    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "1"
    assert (
        modal.wait_for_selector(".modal-title").text_content().strip()
        == "Add a new comment"
    )

    # The Cancel link is closing the modal.
    cancel_link = modal.wait_for_selector("text=Cancel")
    cancel_link.click()
    # Manually waiting for the .3s CSS transition.
    page.wait_for_timeout(350)

    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "0"


def test_open_close_with_escape_key_comment_form_modal(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")

    # The opacity of the modal should be 0 by default.
    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "0"

    page.hover("#comments h2")
    page.click("#comments h2 a >> text='Add'")
    # Manually waiting for the .3s CSS transition.
    page.wait_for_timeout(350)

    # Once opened, the opacity is set to 1.
    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "1"
    assert (
        modal.wait_for_selector(".modal-title").text_content().strip()
        == "Add a new comment"
    )

    # The escape key is closing the modal.
    page.keyboard.press("Escape")
    # Manually waiting for the .3s CSS transition.
    page.wait_for_timeout(350)

    modal = page.wait_for_selector("#comment-create")
    opacity = modal.evaluate("e => window.getComputedStyle(e).opacity")
    assert opacity == "0"


def test_submitting_passage_comment_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    page.hover("#comments h2")
    page.click("#comments h2 a >> text='Add'")
    modal = page.wait_for_selector("#comment-create")
    description_textarea = modal.wait_for_selector("#id_description")
    description_textarea.fill("Description")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.


def test_submitting_scholium_comment_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    page.hover("#comments h2")
    page.click("#comments h2 a >> text='Add'")
    modal = page.wait_for_selector("#comment-create")
    description_textarea = modal.wait_for_selector("#id_description")
    description_textarea.fill("Description")
    language_select = modal.wait_for_selector("#id_language")
    language_select.select_option("fra")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
