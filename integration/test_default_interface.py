def test_site_title(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert page.text_content("header h1").strip() == "Anthologia Graeca"


def test_passage_title(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    assert page.text_content("section h1").strip() == "Epigram 7.70"


def test_scholium_title(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    assert page.text_content("section h1").strip() == "Scholium 7.70.1"
