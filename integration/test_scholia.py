def test_submitting_scholium_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.71/")
    page.hover("#scholia h2")
    page.click("#scholia h2 a >> text='Add'")
    modal = page.wait_for_selector("#scholium-create")
    number_input = modal.wait_for_selector("#id_number")
    assert number_input.get_attribute("value") == "2"
    number_input.fill("2")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
