def test_submitting_passage_manuscript_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    page.click("#manuscripts a >> text='Add'")
    modal = page.wait_for_selector("#manuscript-create")
    manuscript_title = modal.wait_for_selector("#id_title")
    manuscript_title.fill("title")
    manuscript_url = modal.wait_for_selector("#id_url")
    manuscript_url.fill(
        "https://digi.ub.uni-heidelberg.de/iiif/2/cpgraec23:049.jpg/pct:71.94367850809665,13.06538505399276,15.729793177496104,4.061230865354575/full/0/default.jpg"
    )
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.


def test_submitting_scholium_manuscript_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    page.click("#manuscripts a >> text='Add'")
    modal = page.wait_for_selector("#manuscript-create")
    manuscript_title = modal.wait_for_selector("#id_title")
    manuscript_title.fill("title")
    manuscript_url = modal.wait_for_selector("#id_url")
    manuscript_url.fill(
        "https://digi.ub.uni-heidelberg.de/iiif/2/cpgraec23:049.jpg/pct:71.94367850809665,13.06538505399276,15.729793177496104,4.061230865354575/full/0/default.jpg"
    )
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
