from django.urls import path

from web.views.history import VersionList

urlpatterns = [
    path("", VersionList.as_view(), name="version-list"),
]
