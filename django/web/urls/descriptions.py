from django.urls import path
from web.views.descriptions import (PassageDescriptionCreate,
                                    PassageDescriptionDelete,
                                    PassageDescriptionUpdate,
                                    ScholiumDescriptionCreate,
                                    ScholiumDescriptionDelete,
                                    ScholiumDescriptionUpdate)

passage_patterns = [
    path(
        "create/", PassageDescriptionCreate.as_view(), name="passage-description-create"
    ),
    path(
        "update/<int:description_pk>/",
        PassageDescriptionUpdate.as_view(),
        name="passage-description-update",
    ),
    path(
        "delete/<int:description_pk>/",
        PassageDescriptionDelete.as_view(),
        name="passage-description-delete",
    ),
]

scholium_patterns = [
    path(
        "create/",
        ScholiumDescriptionCreate.as_view(),
        name="scholium-description-create",
    ),
    path(
        "update/<int:description_pk>/",
        ScholiumDescriptionUpdate.as_view(),
        name="scholium-description-update",
    ),
    path(
        "delete/<int:description_pk>/",
        ScholiumDescriptionDelete.as_view(),
        name="scholium-description-delete",
    ),
]
