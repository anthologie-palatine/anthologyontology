from django.urls import include, path, re_path
from web.views.passage import PassageDetail, PassageHistory
from web.views.scholia import ScholiumCreate, ScholiumDetail, ScholiumHistory

from .alignments import passage_patterns as alignments_urls
from .authors import passage_patterns as authors_urls
from .cities import passage_patterns as cities_urls
from .comments import passage_patterns as comments_urls
from .descriptions import passage_patterns as desc_urls
from .keywords import passage_patterns as keywords_urls
from .manuscripts import passage_patterns as manuscripts_urls
from .external_references import passage_patterns as external_references_urls
from .internal_references import passage_patterns as internal_references_urls
from .media import passage_patterns as media_urls
from .texts import passage_patterns as texts_urls


PASSAGE_REGEXP = r"urn:cts:greekLit:tlg7000.tlg001.ag:(?P<book>\d+).(?P<fragment>\d+)(?P<sub_fragment>[^/]*)"
SCHOLIUM_REGEXP = r"urn:cts:greekLit:tlg5011.tlg001.sag:(?P<book>\d+).(?P<fragment>\d+)(?P<sub_fragment>[^/.]*).(?P<number>\d+)"

urlpatterns = [
    re_path(
        rf"{PASSAGE_REGEXP}/history/", PassageHistory.as_view(), name="passage-history"
    ),
    re_path(rf"{PASSAGE_REGEXP}/", PassageDetail.as_view(), name="passage-detail"),
    re_path(
        rf"{SCHOLIUM_REGEXP}/history/",
        ScholiumHistory.as_view(),
        name="scholium-history",
    ),
    re_path(rf"{SCHOLIUM_REGEXP}/", ScholiumDetail.as_view(), name="scholium-detail"),
    path("<int:passage_pk>/descriptions/", include(desc_urls)),
    path("<int:passage_pk>/authors/", include(authors_urls)),
    path("<int:passage_pk>/cities/", include(cities_urls)),
    path("<int:passage_pk>/comments/", include(comments_urls)),
    path("<int:passage_pk>/keywords/", include(keywords_urls)),
    path("<int:passage_pk>/texts/", include(texts_urls)),
    path("<int:passage_pk>/alignments/", include(alignments_urls)),
    path("<int:passage_pk>/manuscripts/", include(manuscripts_urls)),
    path("<int:passage_pk>/internal_references/", include(internal_references_urls)),
    path("<int:passage_pk>/external_references/", include(external_references_urls)),
    path("<int:passage_pk>/media/", include(media_urls)),
    path(
        "<int:passage_pk>/scholium/create/",
        ScholiumCreate.as_view(),
        name="scholium-create",
    ),
]
