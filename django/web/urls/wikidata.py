from django.urls import path
from web.views.wikidata import WikidataJSON

urlpatterns = [
    path("info/", WikidataJSON.as_view(), name="wikidata-info"),
]
