from django.urls import path
from web.views.keywords import (
    KeywordCreateNames,
    KeywordCreateWikidataURL,
    KeywordDetail,
    KeywordList,
    PassageKeywordCreate,
    PassageKeywordDelete,
    ScholiumKeywordCreate,
    ScholiumKeywordDelete,
)

urlpatterns = [
    path("", KeywordList.as_view(), name="keyword-list"),
    path("<int:pk>/", KeywordDetail.as_view(), name="keyword-detail"),
    path("create/", KeywordCreateNames.as_view(), name="keyword-create-names"),
    path(
        "create/wikidata/",
        KeywordCreateWikidataURL.as_view(),
        name="keyword-create-wikidata",
    ),
]


passage_patterns = [
    path("create/", PassageKeywordCreate.as_view(), name="passage-keyword-create"),
    path(
        "delete/<int:keyword_pk>/",
        PassageKeywordDelete.as_view(),
        name="passage-keyword-delete",
    ),
]
scholium_patterns = [
    path("create/", ScholiumKeywordCreate.as_view(), name="scholium-keyword-create"),
    path(
        "delete/<int:keyword_pk>/",
        ScholiumKeywordDelete.as_view(),
        name="scholium-keyword-delete",
    ),
]
