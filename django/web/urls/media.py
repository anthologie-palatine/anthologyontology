from django.urls import path
from web.views.media import MediumCreate, MediumDelete

passage_patterns = [
    path(
        "create/",
        MediumCreate.as_view(),
        name="passage-medium-create",
    ),
    path(
        "delete/<int:medium_pk>/",
        MediumDelete.as_view(),
        name="passage-medium-delete",
    ),
]
