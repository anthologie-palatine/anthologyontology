from django.urls import path
from web.views.authors import (AuthorDetail, AuthorList, PassageAuthorCreate,
                               PassageAuthorDelete)

urlpatterns = [
    path("", AuthorList.as_view(), name="author-list"),
    path("<int:pk>/", AuthorDetail.as_view(), name="author-detail"),
]

passage_patterns = [
    path("create/", PassageAuthorCreate.as_view(), name="passage-author-create"),
    path(
        "delete/<int:author_pk>/",
        PassageAuthorDelete.as_view(),
        name="passage-author-delete",
    ),
]
