from django.urls import path
from web.views.texts import (
    PassageTextCreate,
    PassageTextUpdate,
    PassageTextDelete,
    ScholiumTextCreate,
    ScholiumTextDelete,
    ScholiumTextUpdate,
)

passage_patterns = [
    path("create/", PassageTextCreate.as_view(), name="passage-text-create"),
    path(
        "update/<int:text_pk>/",
        PassageTextUpdate.as_view(),
        name="passage-text-update",
    ),
    path(
        "delete/<int:text_pk>/",
        PassageTextDelete.as_view(),
        name="passage-text-delete",
    ),
]
scholium_patterns = [
    path("create/", ScholiumTextCreate.as_view(), name="scholium-text-create"),
    path(
        "update/<int:text_pk>/",
        ScholiumTextUpdate.as_view(),
        name="scholium-text-update",
    ),
    path(
        "delete/<int:text_pk>/",
        ScholiumTextDelete.as_view(),
        name="scholium-text-delete",
    ),
]
