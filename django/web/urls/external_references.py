from django.urls import path
from web.views.external_references import (
    ExternalReferenceCreate,
    ExternalReferenceDelete,
)

passage_patterns = [
    path(
        "create/",
        ExternalReferenceCreate.as_view(),
        name="passage-external-reference-create",
    ),
    path(
        "delete/<int:external_reference_pk>/",
        ExternalReferenceDelete.as_view(),
        name="passage-external-reference-delete",
    ),
]
