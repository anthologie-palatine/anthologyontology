from django.urls import path
from web.views.internal_references import (
    InternalReferenceCreate,
    InternalReferenceDelete,
)

passage_patterns = [
    path(
        "create/",
        InternalReferenceCreate.as_view(),
        name="passage-internal-reference-create",
    ),
    path(
        "delete/<int:internal_reference_pk>/",
        InternalReferenceDelete.as_view(),
        name="passage-internal-reference-delete",
    ),
]
