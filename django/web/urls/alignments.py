from django.urls import path
from web.views.alignments import (
    PassageAlignmentCreate,
    PassageAlignmentDelete,
    PassageAlignmentUpdate,
)

passage_patterns = [
    path("create/", PassageAlignmentCreate.as_view(), name="passage-alignment-create"),
    path(
        "update/<int:alignment_pk>/",
        PassageAlignmentUpdate.as_view(),
        name="passage-alignment-update",
    ),
    path(
        "delete/<int:alignment_pk>/",
        PassageAlignmentDelete.as_view(),
        name="passage-alignment-delete",
    ),
]
