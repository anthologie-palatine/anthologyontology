from django.urls import path

from web.views.pages import Page

urlpatterns = [
    path("<str:name>/", Page.as_view(), name="pages"),
]
