from django import forms
from django.core import validators
from django.utils.translation import gettext as _


class AlternativeURNField(forms.CharField):
    widget = forms.URLInput(
        attrs={
            "data-wikidata-target": "urn",
        }
    )
    default_validators = [
        validators.URLValidator(),
        validators.URLValidator(
            regex=r"https?://.+\.?wikidata.org/wiki/(?P<wiki_id>Q\d+)",
            message=_("Only wikidata URLs are supported."),
            code="invalid",
        ),
    ]
