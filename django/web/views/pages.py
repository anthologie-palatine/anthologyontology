from django.conf import settings
from django.utils.translation import get_language
from django.views.generic import TemplateView

import markdown
import frontmatter


class MarkdownMixin:
    def get_markdown_page(self, path, language="fr"):
        markdown_path = (
            settings.BASE_DIR / "web" / "content" / language / "pages" / path
        )
        metadata, content = frontmatter.parse(markdown_path.read_text())
        html = markdown.markdown(content)
        return metadata, html


class Page(MarkdownMixin, TemplateView):
    template_name = "web/pages/page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        language = get_language()
        # Fallback to English and for the moment only French is available
        # as an alternative.
        if language != "fr":
            language = "en"
        metadata, content = self.get_markdown_page(f"{kwargs['name']}.md", language)
        context["page_title"] = metadata["title"]
        context["content"] = content
        return context
