from urllib.parse import urlencode

from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import generic
import reversion
from reversion.views import RevisionMixin

from meleager.helpers.unicode import strip_accents
from meleager.forms.passage_m2m import KeywordForm
from meleager.models import Keyword, KeywordCategory, KeywordPassage, KeywordScholium
from meleager.models.name import sort_names_by_languages
from meleager.views.m2m import M2MDeleteView, M2MFormView
from meleager.views.mixins import (
    AddContentPermReqMixin,
    PassageRelatedMixin,
    ScholiumRelatedMixin,
)

from ..forms.keywords import KeywordCreateNamesForm, KeywordCreateWikidataURLForm


class KeywordList(generic.ListView):
    model = Keyword
    template_name = "web/keyword/list.html"
    context_object_name = "keywords"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = _("Keywords")
        kw_without_cat, kw_with_cat = self.get_queryset().get_keyword_choices(
            together=False
        )
        # It's more performant to regroup categories manually in Python
        # instead of running a huge SQL query. Here we aggregate results
        # by Category given their first name because `get_keyword_choices`
        # cannot do that and we end up having one category group by Name
        # which results in duplicates for each Name translation.
        # A bit tedious but we reduce drastically the number of SQL
        # queries this way!
        category_keywords = dict(kw_with_cat)
        categories_with_keywords = {
            cat.current_language_name(): category_keywords.get(
                str(cat.names.first().name)
            )
            for cat in sorted(
                KeywordCategory.objects.all(),
                key=lambda cat: strip_accents(cat.current_language_name()),
            )
        }
        context["categories_with_keywords"] = categories_with_keywords

        return context


class KeywordDetail(generic.DetailView):
    model = Keyword
    template_name = "web/keyword/detail.html"
    context_object_name = "keyword"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        keyword = context["keyword"]
        context["keyword_names"] = sort_names_by_languages(
            keyword.names.values("name", "language__code", "language__code2"),
            key="language__code2",
        )
        context["page_title"] = context["keyword_names"][0]["name"]
        context["passages"] = keyword.passages.all()
        context["scholia"] = keyword.scholia.all()
        return context


class KeywordCreateWikidataURL(AddContentPermReqMixin, RevisionMixin, generic.FormView):
    model = Keyword
    template_name = "web/keyword/create_wikidata.html"
    context_object_name = "keyword"
    form_class = KeywordCreateWikidataURLForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create_url"] = reverse("web:keyword-create-wikidata")
        context["wikidata_info_url"] = self.request.build_absolute_uri(
            reverse("web:wikidata-info")
        )
        context["next_url"] = self.request.GET.get("next")
        return context

    def get_success_url(self):
        next_url = self.request.POST.get("next_url")
        if next_url and "#" in next_url:
            url, anchor = next_url.split("#", 1)
            return f"{url}?keyword_created_pk={self.object.pk}#{anchor}"
        else:
            return self.object.get_absolute_url()

    def form_valid(self, form):
        category = form.cleaned_data["category"]
        auto_url = form.cleaned_data["alternative_urn_url_auto"]
        keyword_or_none = Keyword.objects.get_or_create_from_url(auto_url, category)
        if keyword_or_none is None:
            return super().form_invalid(form)
        else:
            self.object = keyword_or_none

        comment = urlencode(
            {
                "app_label": "meleager",
                "model": self.object.__class__.__name__,
                "pk": self.object.pk,
                "action_type": "create",
                "relation_type": "",
            }
        )
        reversion.set_comment(comment)

        return super().form_valid(form)


class KeywordCreateNames(AddContentPermReqMixin, generic.FormView):
    model = Keyword
    template_name = "web/keyword/create.html"
    context_object_name = "keyword"
    form_class = KeywordCreateNamesForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create_url"] = reverse("web:keyword-create-names")
        return context

    def get_success_url(self):
        return self.object.get_absolute_url()

    def form_valid(self, form):
        category = form.cleaned_data["category"]
        manual_url = form.cleaned_data["alternative_urn_url"]
        source = form.cleaned_data["alternative_urn_source"]
        name = form.cleaned_data["name"]
        name_lang = form.cleaned_data["name_lang"]

        self.object = Keyword.objects.get_or_create_from_names(
            category=category,
            source=source,
            url=manual_url,
            names=[
                (name_lang, name),
            ],
        )

        return super().form_valid(form)


class PassageKeywordCreate(PassageRelatedMixin, M2MFormView):
    model = KeywordPassage
    form_class = KeywordForm
    template_name = "web/keyword/keyword_create.html"
    related_name = "keyword"
    action = "create"
    success_url_anchor_name = "keywords"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        base_object = self.get_base_object()
        related = base_object.keywords.all()
        already_linked = related.values_list("pk", flat=True)
        kwargs["queryset"] = Keyword.objects.exclude(pk__in=already_linked)
        kwargs["next_url"] = f"{base_object.get_absolute_url()}#passage-keyword-create"
        kwargs["initial"] = {"keyword": self.request.GET.get("keyword_created_pk")}
        return kwargs


class PassageKeywordDelete(PassageRelatedMixin, M2MDeleteView):
    model = KeywordPassage
    template_name = "web/keyword/keyword_delete.html"
    related_name = "keyword"
    success_url_anchor_name = "keywords"


class ScholiumKeywordCreate(ScholiumRelatedMixin, M2MFormView):
    model = KeywordScholium
    form_class = KeywordForm
    template_name = "web/keyword/keyword_create.html"
    related_name = "keyword"
    action = "create"
    success_url_anchor_name = "keywords"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        base_object = self.get_base_object()
        related = base_object.keywords.all()
        already_linked = related.values_list("pk", flat=True)
        kwargs["queryset"] = Keyword.objects.exclude(pk__in=already_linked)
        kwargs["next_url"] = f"{base_object.get_absolute_url()}#scholium-keyword-create"
        kwargs["initial"] = {"keyword": self.request.GET.get("keyword_created_pk")}
        return kwargs


class ScholiumKeywordDelete(ScholiumRelatedMixin, M2MDeleteView):
    model = KeywordScholium
    template_name = "web/keyword/keyword_delete.html"
    related_name = "keyword"
    success_url_anchor_name = "keywords"
