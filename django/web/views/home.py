from django.shortcuts import render
from django.utils.translation import gettext as _
from meleager.models import Book


def home(request):
    books = Book.objects.all().prefetch_related("passages")
    return render(
        request, "web/index.html", {"books": books, "page_title": _("Passages")}
    )
