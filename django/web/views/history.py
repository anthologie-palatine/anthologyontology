from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.views.generic import ListView
from django.utils.translation import gettext as _
from django.core.paginator import Paginator

from reversion.models import Version


class VersionList(ListView):
    template_name = "web/history/list.html"
    context_object_name = "versions"
    paginate_by = 50
    paginator_class = Paginator

    def get_queryset(self):
        qs = (
            Version.objects.filter(revision__user__isnull=False)
            .filter(
                Q(
                    content_type=ContentType.objects.get(
                        app_label="meleager", model="passage"
                    )
                )
                | Q(
                    content_type=ContentType.objects.get(
                        app_label="meleager", model="scholium"
                    )
                )
                | Q(
                    content_type=ContentType.objects.get(
                        app_label="meleager", model="keyword"
                    )
                )
                | Q(
                    content_type=ContentType.objects.get(
                        app_label="meleager", model="city"
                    )
                )
            )
            .select_related("revision__user")
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = _("Modifications")
        return context
