from django.http import HttpResponseBadRequest, JsonResponse
from django.views import generic

from meleager.helpers.wikidata import get_wiki_names_from_url


class WikidataJSON(generic.View):
    def get(self, request, *args, **kwargs):
        wiki_url = request.GET.get("url")
        if not wiki_url:
            return HttpResponseBadRequest("No (or invalid) URL provided")

        return JsonResponse(get_wiki_names_from_url(wiki_url))
