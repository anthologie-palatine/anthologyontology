from urllib.parse import urlencode

from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import generic
import reversion
from reversion.views import RevisionMixin

from meleager.forms.passage_m2m import CityForm
from meleager.models import City, CityPassage, CityScholium
from meleager.models.name import sort_names_by_languages
from meleager.views.m2m import M2MDeleteView, M2MFormView
from meleager.views.mixins import (
    AddContentPermReqMixin,
    PassageRelatedMixin,
    ScholiumRelatedMixin,
)

from ..forms.city import CityCreateNamesForm, CityCreateWikidataURLForm


class CityList(generic.ListView):
    model = City
    template_name = "web/city/list.html"
    context_object_name = "cities"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = _("Cities")
        context["cities"] = (
            self.get_queryset()
            .exclude(Q(longitude=None) | Q(latitude=None))
            .prefetch_related(
                "authors_born_at", "authors_died_at", "passages", "passages__book"
            )
        )
        context["cities_pk_names"] = self.get_queryset().get_name_choices()
        return context


class CityDetail(generic.DetailView):
    model = City
    template_name = "web/city/detail.html"
    context_object_name = "city"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        city = context["city"]
        context["city_names"] = sort_names_by_languages(
            city.names.values("name", "language__code", "language__code2"),
            key="language__code2",
        )
        context["page_title"] = context["city_names"][0]["name"]
        context["passages"] = city.passages.all()
        context["scholia"] = city.scholia.all()
        context["authors_born_at"] = city.authors_born_at.all()
        context["authors_died_at"] = city.authors_died_at.all()
        return context


class CityCreateWikidataURL(AddContentPermReqMixin, RevisionMixin, generic.FormView):
    model = City
    template_name = "web/city/create_wikidata.html"
    context_object_name = "city"
    form_class = CityCreateWikidataURLForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create_url"] = reverse("web:city-create-wikidata")
        context["wikidata_info_url"] = self.request.build_absolute_uri(
            reverse("web:wikidata-info")
        )
        context["next_url"] = self.request.GET.get("next")
        return context

    def get_success_url(self):
        next_url = self.request.POST.get("next_url")
        if next_url and "#" in next_url:
            url, anchor = next_url.split("#", 1)
            return f"{url}?city_created_pk={self.object.pk}#{anchor}"
        else:
            return self.object.get_absolute_url()

    def form_valid(self, form):
        auto_url = form.cleaned_data["alternative_urn_url_auto"]
        city_or_none = City.objects.get_or_create_from_url(auto_url)

        if city_or_none is None:
            return super().form_invalid(form)
        else:
            self.object = city_or_none

        comment = urlencode(
            {
                "app_label": "meleager",
                "model": self.object.__class__.__name__,
                "pk": self.object.pk,
                "action_type": "create",
                "relation_type": "",
            }
        )
        reversion.set_comment(comment)

        return super().form_valid(form)


class CityCreateNames(AddContentPermReqMixin, generic.FormView):
    model = City
    template_name = "web/city/create.html"
    context_object_name = "city"
    form_class = CityCreateNamesForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create_url"] = reverse("web:city-create-names")
        return context

    def get_success_url(self):
        return self.object.get_absolute_url()

    def form_valid(self, form):
        manual_url = form.cleaned_data["alternative_urn_url"]
        source = form.cleaned_data["alternative_urn_source"]
        name = form.cleaned_data["name"]
        name_lang = form.cleaned_data["name_lang"]

        self.object = City.objects.get_or_create_from_names(
            source=source,
            url=manual_url,
            names=[
                (name_lang, name),
            ],
        )

        return super().form_valid(form)


class PassageCityCreate(PassageRelatedMixin, M2MFormView):
    model = CityPassage
    form_class = CityForm
    template_name = "web/city/city_create.html"
    related_name = "city"
    base_field = "cities"
    action = "create"
    success_url_anchor_name = "cities"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        base_object = self.get_base_object()
        related = base_object.cities.all()
        already_linked = related.values_list("pk", flat=True)
        kwargs["queryset"] = City.objects.exclude(pk__in=already_linked)
        kwargs["next_url"] = f"{base_object.get_absolute_url()}#passage-city-create"
        kwargs["initial"] = {"city": self.request.GET.get("city_created_pk")}
        return kwargs


class PassageCityDelete(PassageRelatedMixin, M2MDeleteView):
    model = CityPassage
    template_name = "web/city/city_delete.html"
    related_name = "city"
    success_url_anchor_name = "cities"


class ScholiumCityCreate(ScholiumRelatedMixin, M2MFormView):
    model = CityScholium
    form_class = CityForm
    template_name = "web/city/city_create.html"
    related_name = "city"
    base_field = "cities"
    action = "create"
    success_url_anchor_name = "cities"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        base_object = self.get_base_object()
        related = base_object.cities.all()
        already_linked = related.values_list("pk", flat=True)
        kwargs["queryset"] = City.objects.exclude(pk__in=already_linked)
        kwargs["next_url"] = f"{base_object.get_absolute_url()}#scholium-city-create"
        kwargs["initial"] = {"city": self.request.GET.get("city_created_pk")}
        return kwargs


class ScholiumCityDelete(ScholiumRelatedMixin, M2MDeleteView):
    model = CityScholium
    template_name = "web/city/city_delete.html"
    related_name = "city"
    success_url_anchor_name = "cities"
