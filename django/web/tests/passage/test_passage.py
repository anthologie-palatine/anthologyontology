from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.fixture
def list_url():
    return reverse("web:index")


@pytest.fixture
def detail_url(book_1, passage_1):
    return reverse(
        "web:passage-detail",
        args=(book_1.number, passage_1.fragment, passage_1.sub_fragment),
    )


@pytest.mark.django_db
class TestPassageAnonymous:
    def test_can_access_passages_page(self, client, list_url, passage_1):
        response = client.get(list_url)
        assert response.status_code == HTTPStatus.OK
        assert "Epigrams" in str(response.content)
        assert passage_1.get_absolute_url() in str(response.content)

    def test_can_access_passage_page(self, client, detail_url, passage_1):
        response = client.get(detail_url)
        assert response.status_code == HTTPStatus.OK
        assert str(passage_1) in str(response.content)

    def test_can_access_passage_page_with_subfragment(
        self, client, detail_url, passage_1
    ):
        passage_1.sub_fragment = "a"
        passage_1.save()
        assert (
            passage_1.get_absolute_url()
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1a/"
        )
        response = client.get(
            reverse(
                "web:passage-detail",
                args=(
                    passage_1.book.number,
                    passage_1.fragment,
                    passage_1.sub_fragment,
                ),
            )
        )
        assert response.status_code == HTTPStatus.OK
        assert str(passage_1) == "Epigram 1.1.a"
        assert str(passage_1) in str(response.content)
