import json
from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-alignment-create", args=(passage_1.pk,))


@pytest.fixture
def update_url(passage_1, alignment_1):
    return reverse("web:passage-alignment-update", args=(passage_1.pk, alignment_1.pk))


@pytest.fixture
def delete_url(passage_1, alignment_1):
    return reverse("web:passage-alignment-delete", args=(passage_1.pk, alignment_1.pk))


@pytest.fixture
def alignment_payload(text_1, text_2):
    return {
        "text_1": text_1.pk,
        "text_2": text_2.pk,
        "alignment_data": json.dumps([{"foo": "bar"}, {"baz": "quux"}]),
    }


class TestPassageTextAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, alignment_payload):
        response = client.post(create_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_update_page(self, client, update_url):
        response = client.get(update_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_update_page(self, client, update_url, alignment_payload):
        response = client.post(update_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageTextEditor:
    def test_cannot_access_create_page_if_less_than_two_texts(
        self, client_editor, create_url, text_1
    ):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_can_access_create_page(self, client_editor, create_url, text_1, text_2):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_update_page(self, client_editor, update_url, text_1, text_2):
        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_alignment(
        self, client_editor, create_url, alignment_payload, passage_1
    ):
        from meleager.models import Alignment

        assert passage_1.texts.count() == 2
        response = client_editor.post(create_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#alignments"
        )
        created_alignment = Alignment.objects.get(
            text_1=alignment_payload["text_1"], text_2=alignment_payload["text_2"]
        )
        assert created_alignment.alignment_data == json.loads(
            alignment_payload["alignment_data"]
        )

    def test_create_alignment_generate_versions(
        self, client_editor, create_url, alignment_payload, passage_1, editor
    ):
        from meleager.models import Alignment

        response = client_editor.post(create_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#alignments"
        )
        created_alignment = Alignment.objects.get(
            text_1=alignment_payload["text_1"], text_2=alignment_payload["text_2"]
        )
        versions = Version.objects.get_for_object(passage_1)
        assert versions[0].revision.get_comment() == (
            f"app_label=meleager&model=Alignment&pk={created_alignment.pk}"
            "&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_update_alignment(
        self, client_editor, update_url, alignment_payload, alignment_1, text_1
    ):
        assert text_1.meleager_alignment_text_1.first().alignment_data == [[], []]
        response = client_editor.post(update_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#alignments"
        )
        assert text_1.meleager_alignment_text_1.first().alignment_data == [
            {"foo": "bar"},
            {"baz": "quux"},
        ]

    def test_update_alignment_generate_versions(
        self,
        client_editor,
        update_url,
        alignment_payload,
        alignment_1,
        passage_1,
        editor,
    ):
        response = client_editor.post(update_url, alignment_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert versions[0].revision.get_comment() == (
            f"app_label=meleager&model=Alignment&pk={alignment_1.pk}"
            "&action_type=modify&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_update_alignment_if_not_creator(
        self, client_editor, update_url, alignment_payload, text_1, alignment_1, regular
    ):
        assert text_1.meleager_alignment_text_1.first().pk == alignment_1.pk
        alignment_1.creator = regular
        alignment_1.save()

        response = client_editor.post(update_url, alignment_payload)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert text_1.meleager_alignment_text_1.first().pk == alignment_1.pk

    def test_can_delete_alignment(self, client_editor, delete_url, alignment_1, text_1):
        assert text_1.meleager_alignment_text_1.first().pk == alignment_1.pk
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#alignments"
        )
        assert text_1.meleager_alignment_text_1.count() == 0

    def test_delete_alignment_generate_versions(
        self, client_editor, delete_url, alignment_1, passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert versions[0].revision.get_comment() == (
            f"app_label=meleager&model=Alignment&pk={alignment_1.pk}"
            "&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_alignment_if_not_creator(
        self, client_editor, delete_url, text_1, alignment_1, regular
    ):
        assert text_1.meleager_alignment_text_1.first().pk == alignment_1.pk
        alignment_1.creator = regular
        alignment_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert text_1.meleager_alignment_text_1.first().pk == alignment_1.pk
