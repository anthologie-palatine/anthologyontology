from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-internal-reference-create", args=(passage_1.pk,))


@pytest.fixture
def delete_url(passage_1, internal_reference_1):
    return reverse(
        "web:passage-internal-reference-delete",
        args=(passage_1.pk, internal_reference_1.pk),
    )


@pytest.fixture
def internal_reference_payload(passage_2):
    return {
        "to_passage": passage_2.pk,
        "reference_type": "RHETORIC",
    }


class TestPassageInternalReferenceAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(
        self, client, create_url, internal_reference_payload
    ):
        response = client.post(create_url, internal_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageInternalReferenceEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_internal_reference(
        self, client_editor, create_url, internal_reference_payload, passage_1
    ):
        from meleager.models import PassageInternalReference

        assert passage_1.internal_references.count() == 0
        response = client_editor.post(create_url, internal_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_internal_reference = PassageInternalReference.objects.get(
            to_passage_id=internal_reference_payload["to_passage"]
        )
        assert response.url == (
            "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#internal-references"
        )
        assert (
            created_internal_reference
            in passage_1.internal_references.through.objects.filter(
                from_passage=passage_1
            )
        )
        assert created_internal_reference.reference_type == "RHETORIC"

    def test_create_internal_reference_generate_versions(
        self,
        client_editor,
        create_url,
        internal_reference_payload,
        passage_1,
        passage_2,
        editor,
    ):
        response = client_editor.post(create_url, internal_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Passage&pk_from={passage_1.pk}&pk_to={passage_2.pk}&action_type=add&relation_type=self"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_internal_reference(
        self, client_editor, delete_url, internal_reference_1, passage_1
    ):
        assert (
            internal_reference_1
            in passage_1.internal_references.through.objects.filter(
                from_passage=passage_1
            )
        )
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#internal-references"
        )
        assert (
            internal_reference_1
            not in passage_1.internal_references.through.objects.filter(
                from_passage=passage_1
            )
        )

    def test_delete_internal_reference_generate_versions(
        self,
        client_editor,
        delete_url,
        internal_reference_1,
        passage_1,
        passage_2,
        editor,
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Passage&pk_from={passage_1.pk}&pk_to={passage_2.pk}&action_type=delete&relation_type=self"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_internal_reference_if_not_creator(
        self, client_editor, delete_url, internal_reference_1, passage_1, regular
    ):
        assert (
            internal_reference_1
            in passage_1.internal_references.through.objects.filter(
                from_passage=passage_1
            )
        )
        internal_reference_1.creator = regular
        internal_reference_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert (
            internal_reference_1
            in passage_1.internal_references.through.objects.filter(
                from_passage=passage_1
            )
        )
