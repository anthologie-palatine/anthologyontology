from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
@pytest.mark.django_db
def text_1(passage_1, language_english, editor):
    from meleager.models import Text

    text = Text.objects.create(
        text="Text content.", language=language_english, unique_id=1, creator=editor
    )
    passage_1.texts.add(text)
    return text


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-text-create", args=(passage_1.pk,))


@pytest.fixture
def update_url(passage_1, text_1):
    return reverse("web:passage-text-update", args=(passage_1.pk, text_1.pk))


@pytest.fixture
def delete_url(passage_1, text_1):
    return reverse("web:passage-text-delete", args=(passage_1.pk, text_1.pk))


@pytest.fixture
def text_payload(language_french):
    return {"text": "New text content.", "language": language_french.code}


class TestPassageTextAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, text_payload):
        response = client.post(create_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_update_page(self, client, update_url):
        response = client.get(update_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_update_page(self, client, update_url, text_payload):
        response = client.post(update_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageTextEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_update_page(self, client_editor, update_url):
        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK

    def test_update_page_default_no_warning(self, client_editor, update_url):
        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK
        assert "Warning" not in str(response.content)

    def test_update_page_warning_if_existing_alignment(
        self, client_editor, update_url, text_1
    ):
        from meleager.models import Alignment

        Alignment.objects.create(text_1=text_1, text_2=text_1, unique_id=1)

        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK
        assert "Warning" in str(response.content)

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_delete_page_default_no_warning(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK
        assert "Warning" not in str(response.content)

    def test_delete_page_warning_if_existing_alignment(
        self, client_editor, delete_url, text_1
    ):
        from meleager.models import Alignment

        Alignment.objects.create(text_1=text_1, text_2=text_1, unique_id=1)

        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK
        assert "Warning" in str(response.content)

    def test_can_create_text(self, client_editor, create_url, text_payload, passage_1):
        from meleager.models import Text

        assert passage_1.texts.count() == 0
        response = client_editor.post(create_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#texts"
        created_text = Text.objects.get(text=text_payload["text"])
        assert created_text in passage_1.texts.all()

    def test_create_text_generate_versions(
        self, client_editor, create_url, text_payload, passage_1, text_1, editor
    ):
        from meleager.models import Text

        response = client_editor.post(create_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_text = Text.objects.get(text=text_payload["text"])
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Text&pk={created_text.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_create_text_with_edition(
        self,
        client_editor,
        create_url,
        text_payload,
        passage_1,
        work_1,
        language_french,
    ):
        from meleager.models import Description, Edition, Text

        edition_name = "Mon édition"
        description = Description.objects.create(
            description=edition_name, language=language_french
        )
        created_edition = Edition.objects.create(
            work=work_1, edition_type=Edition.EditionType.MANUSCRIPT
        )
        created_edition.descriptions.add(description)
        text_payload["edition"] = created_edition.pk

        assert passage_1.texts.count() == 0
        response = client_editor.post(create_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#texts"
        created_text = Text.objects.get(text=text_payload["text"])
        assert created_text in passage_1.texts.all()
        assert created_text.edition == created_edition

    def test_can_update_text(
        self, client_editor, update_url, text_payload, passage_1, text_1
    ):
        passage_1.texts.add(text_1)
        assert text_1.text == "Text content."
        response = client_editor.post(update_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#texts"
        text_1.refresh_from_db()
        assert text_1.text == "New text content."
        assert text_1 in passage_1.texts.all()

    def test_update_text_generate_versions(
        self, client_editor, update_url, text_payload, passage_1, text_1, editor
    ):
        passage_1.texts.add(text_1)
        response = client_editor.post(update_url, text_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Text&pk={text_1.pk}&action_type=modify&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_text(self, client_editor, delete_url, text_1, passage_1):
        assert text_1 in passage_1.texts.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#texts"
        assert text_1 not in passage_1.texts.all()

    def test_delete_text_generate_versions(
        self, client_editor, delete_url, text_1, passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Text&pk={text_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_text_with_alignment(
        self, client_editor, delete_url, text_1, passage_1
    ):
        from meleager.models import Alignment

        Alignment.objects.create(text_1=text_1, text_2=text_1, unique_id=1)

        assert text_1 in passage_1.texts.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#texts"
        assert text_1 not in passage_1.texts.all()

    def test_cannot_delete_text_if_not_creator(
        self, client_editor, delete_url, text_1, passage_1, regular
    ):
        assert text_1 in passage_1.texts.all()
        text_1.creator = regular
        text_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert text_1 in passage_1.texts.all()
