from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-external-reference-create", args=(passage_1.pk,))


@pytest.fixture
def delete_url(passage_1, external_reference_1):
    return reverse(
        "web:passage-external-reference-delete",
        args=(passage_1.pk, external_reference_1.pk),
    )


@pytest.fixture
def external_reference_payload(language_french):
    return {
        "title": "New external reference content.",
        "url": "https://example.org",
    }


class TestPassageExternalReferenceAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(
        self, client, create_url, external_reference_payload
    ):
        response = client.post(create_url, external_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageExternalReferenceEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_external_reference(
        self, client_editor, create_url, external_reference_payload, passage_1
    ):
        from meleager.models import ExternalReference

        assert passage_1.external_references.count() == 0
        response = client_editor.post(create_url, external_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_external_reference = ExternalReference.objects.get(
            title=external_reference_payload["title"]
        )
        assert response.url == (
            "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/"
            f"#external-reference-{created_external_reference.pk}"
        )
        assert created_external_reference in passage_1.external_references.all()

    def test_create_external_reference_generate_versions(
        self, client_editor, create_url, external_reference_payload, passage_1, editor
    ):
        from meleager.models import ExternalReference

        response = client_editor.post(create_url, external_reference_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        created_external_reference = ExternalReference.objects.get(
            title=external_reference_payload["title"]
        )
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=ExternalReference&pk={created_external_reference.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_external_reference(
        self, client_editor, delete_url, external_reference_1, passage_1
    ):
        assert external_reference_1 in passage_1.external_references.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#external-references"
        )
        assert external_reference_1 not in passage_1.external_references.all()

    def test_delete_external_reference_generate_versions(
        self, client_editor, delete_url, external_reference_1, passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=ExternalReference&pk={external_reference_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_external_reference_if_not_creator(
        self, client_editor, delete_url, external_reference_1, passage_1, regular
    ):
        assert external_reference_1 in passage_1.external_references.all()
        external_reference_1.creator = regular
        external_reference_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert external_reference_1 in passage_1.external_references.all()
