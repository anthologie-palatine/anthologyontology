from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
@pytest.mark.django_db
def manuscript_1(passage_1, editor):
    from meleager.models import Manuscript

    manuscript = Manuscript.objects.create(
        title="Manuscript content.",
        url="https://example.com",
        credit="©",
        creator=editor,
    )
    passage_1.manuscripts.add(manuscript)
    return manuscript


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-manuscript-create", args=(passage_1.pk,))


@pytest.fixture
def delete_url(passage_1, manuscript_1):
    return reverse(
        "web:passage-manuscript-delete", args=(passage_1.pk, manuscript_1.pk)
    )


@pytest.fixture
def manuscript_payload():
    return {
        "title": "New manuscript content.",
        "url": "https://digi.ub.uni-heidelberg.de/iiif/2/cpgraec23:049.jpg/pct:71.94367850809665,13.06538505399276,15.729793177496104,4.061230865354575/full/0/default.jpg",
    }


class TestPassageManuscriptAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, manuscript_payload):
        response = client.post(create_url, manuscript_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageManuscriptEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_manuscript(
        self, client_editor, create_url, manuscript_payload, passage_1
    ):
        from meleager.models import Manuscript

        assert passage_1.manuscripts.count() == 0
        response = client_editor.post(create_url, manuscript_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#manuscripts"
        )
        created_manuscript = Manuscript.objects.get(title=manuscript_payload["title"])
        assert created_manuscript in passage_1.manuscripts.all()

    def test_create_manuscript_generate_versions(
        self, client_editor, create_url, manuscript_payload, passage_1, editor
    ):
        from meleager.models import Manuscript

        response = client_editor.post(create_url, manuscript_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        created_manuscript = Manuscript.objects.get(title=manuscript_payload["title"])
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Manuscript&pk={created_manuscript.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_manuscript(
        self, client_editor, delete_url, manuscript_1, passage_1
    ):
        assert manuscript_1 in passage_1.manuscripts.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#manuscripts"
        )
        assert manuscript_1 not in passage_1.manuscripts.all()

    def test_delete_manuscript_generate_versions(
        self, client_editor, delete_url, manuscript_1, passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Manuscript&pk={manuscript_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_manuscript_if_not_creator(
        self, client_editor, delete_url, manuscript_1, passage_1, regular
    ):
        assert manuscript_1 in passage_1.manuscripts.all()
        manuscript_1.creator = regular
        manuscript_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert manuscript_1 in passage_1.manuscripts.all()
