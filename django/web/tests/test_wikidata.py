from http import HTTPStatus

import pytest
from django.urls import reverse
import responses


@pytest.fixture
def wikidata_info_url():
    return reverse("web:wikidata-info")


@responses.activate
def test_wikidata_info_without_url(client, wikidata_info_url):
    response = client.get(wikidata_info_url)
    assert response.status_code == HTTPStatus.BAD_REQUEST


@responses.activate
@pytest.mark.django_db
def test_wikidata_info(client, wikidata_info_url, wikidata_json, languages):
    responses.add(
        responses.GET,
        "https://www.wikidata.org/wiki/Special:EntityData/Q23482.json",
        json=wikidata_json("Q23482"),
        status=HTTPStatus.OK,
    )
    response = client.get(
        wikidata_info_url, {"url": "https://www.wikidata.org/wiki/Q23482"}
    )
    assert response.json()["ita"] == "Marsiglia"
