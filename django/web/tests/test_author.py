from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.fixture
def list_url():
    return reverse("web:author-list")


@pytest.fixture
def detail_url(author_1):
    return reverse("web:author-detail", args=(author_1.pk,))


@pytest.mark.django_db
class TestAuthorAnonymous:
    def test_can_access_authors_page(self, client, list_url, author_1):
        response = client.get(list_url)
        assert response.status_code == HTTPStatus.OK
        assert "Authors" in str(response.content)
        assert author_1.names.first().name in str(response.content)

    def test_can_access_author_page(self, client, detail_url, author_1):
        response = client.get(detail_url)
        assert response.status_code == HTTPStatus.OK
        assert author_1.names.first().name in str(response.content)
