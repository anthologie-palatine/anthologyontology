import json
from pathlib import Path

import pytest

from meleager.tests.conftest import *  # noqa: fixtures from mealeager.


@pytest.fixture
def client_superuser(client, superuser):
    client.login(username=superuser.username, password="superuser")
    return client


@pytest.fixture
def client_editor(client, editor):
    client.login(username=editor.username, password="editor")
    return client


@pytest.fixture
def client_regular(client, regular):
    client.login(username=regular.username, password="regular")
    return client


@pytest.fixture
def wikidata_json():
    def load_wikidata_json(wiki_id):
        return json.loads(
            (Path(__file__).parent / "data" / f"{wiki_id}.json").read_text()
        )

    return load_wikidata_json
