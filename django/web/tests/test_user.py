from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.fixture
def profile_url():
    return reverse("profile")


@pytest.fixture
def history_url(regular):
    return reverse("user-history", args=(regular.pk,))


@pytest.mark.django_db
class TestUser:
    def test_user_page_without_versions(self, client_regular, profile_url):
        response = client_regular.get(profile_url)
        assert response.status_code == HTTPStatus.OK
        assert "See all modifications" not in str(response.content)

    def test_user_page_with_versions(
        self, client_regular, regular, profile_url, passage_1, language_english
    ):
        import reversion
        from meleager.models import Text

        with reversion.create_revision():
            Text.objects.create(text="Added text", language=language_english)
            reversion.set_user(regular)
            reversion.add_to_revision(passage_1, model_db=None)
        response = client_regular.get(profile_url)
        assert response.status_code == HTTPStatus.OK
        assert "See all modifications" in str(response.content)

    def test_user_profile_page(self, client_regular, history_url):
        response = client_regular.get(history_url)
        assert response.status_code == HTTPStatus.OK
        assert "Modifications by user regular" in str(response.content)
