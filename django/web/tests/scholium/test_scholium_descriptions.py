from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(scholium_1):
    return reverse("web:scholium-description-create", args=(scholium_1.pk,))


@pytest.fixture
def update_url(scholium_1, description_1):
    return reverse(
        "web:scholium-description-update", args=(scholium_1.pk, description_1.pk)
    )


@pytest.fixture
def delete_url(scholium_1, description_1):
    return reverse(
        "web:scholium-description-delete", args=(scholium_1.pk, description_1.pk)
    )


@pytest.fixture
def description_1(language_french, editor):
    from meleager.models import Description

    description = Description.objects.create(
        description="Description 1 FR", creator=editor, language=language_french
    )
    return description


@pytest.fixture
def description_payload(language_french):
    return {"description": "Description FR", "language": language_french.code}


class TestScholiumDescriptionAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, description_payload):
        response = client.post(create_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_update_page(self, client, update_url):
        response = client.get(update_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_update_page(self, client, update_url, description_payload):
        response = client.post(update_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url, description_1):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url, description_1):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestScholiumDescriptionEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_update_page(self, client_editor, update_url, description_1):
        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url, description_1):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_description(
        self,
        client_editor,
        create_url,
        description_payload,
        scholium_1,
        description_1,
        language_french,
    ):
        from meleager.models import Description

        assert scholium_1.descriptions.count() == 0
        response = client_editor.post(create_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_description = Description.objects.get(
            description=description_payload["description"]
        )
        assert (
            response.url
            == f"/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#description-{created_description.pk}"
        )
        assert created_description in scholium_1.descriptions.all()

    def test_create_description_generate_versions(
        self, client_editor, create_url, description_payload, scholium_1, editor
    ):
        from meleager.models import Description

        response = client_editor.post(create_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_description = Description.objects.get(
            description=description_payload["description"]
        )
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Description&pk={created_description.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_update_description(
        self, client_editor, update_url, description_payload, scholium_1, description_1
    ):
        scholium_1.descriptions.add(description_1)
        assert description_1.description == "Description 1 FR"
        response = client_editor.post(update_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == f"/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#description-{description_1.pk}"
        )
        assert scholium_1.descriptions.first().description == "Description FR"
        assert description_1 in scholium_1.descriptions.all()

    def test_update_description_generate_versions(
        self,
        client_editor,
        update_url,
        description_payload,
        scholium_1,
        description_1,
        editor,
    ):
        scholium_1.descriptions.add(description_1)
        response = client_editor.post(update_url, description_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Description&pk={description_1.pk}&action_type=modify&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_description(
        self, client_editor, delete_url, scholium_1, description_1
    ):
        scholium_1.descriptions.add(description_1)
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#descriptions"
        )
        assert description_1 not in scholium_1.descriptions.all()

    def test_delete_description_generate_versions(
        self, client_editor, delete_url, scholium_1, description_1, editor
    ):
        scholium_1.descriptions.add(description_1)
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Description&pk={description_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_description_if_not_creator(
        self, client_editor, delete_url, scholium_1, description_1, regular
    ):
        description_1.creator = regular
        description_1.save()
        scholium_1.descriptions.add(description_1)

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert description_1 in scholium_1.descriptions.all()
