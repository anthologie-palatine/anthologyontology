from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(scholium_1):
    return reverse("web:scholium-city-create", args=(scholium_1.pk,))


@pytest.fixture
def delete_url(scholium_1, city_1):
    return reverse("web:scholium-city-delete", args=(scholium_1.pk, city_1.pk))


@pytest.fixture
def city_scholium_1(scholium_1, city_1, editor):
    from meleager.models import CityScholium

    city_scholium = CityScholium.objects.create(
        user=editor, scholium_id=scholium_1.pk, city_id=city_1.pk
    )

    return city_scholium


@pytest.fixture
def city_payload(city_1):
    return {"city": city_1.pk}


class TestScholiumCityAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, city_payload):
        response = client.post(create_url, city_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url, city_scholium_1):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url, city_scholium_1):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestScholiumCityEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url, city_scholium_1):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_city(
        self,
        client_editor,
        create_url,
        city_payload,
        scholium_1,
        city_1,
        language_french,
    ):
        from meleager.models import City

        assert scholium_1.cities.count() == 0
        response = client_editor.post(create_url, city_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#cities"
        )
        associated_city = City.objects.get(names__name=city_1.names.all().first().name)
        assert associated_city in scholium_1.cities.all()

    def test_create_city_generate_versions(
        self, client_editor, create_url, city_payload, city_1, scholium_1, editor
    ):
        response = client_editor.post(create_url, city_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=City&pk={city_1.pk}&action_type=associate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_city(
        self, client_editor, delete_url, city_1, scholium_1, city_scholium_1
    ):
        assert city_1 in scholium_1.cities.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#cities"
        )
        assert city_1 not in scholium_1.cities.all()

    def test_delete_city_generate_versions(
        self, client_editor, delete_url, city_1, scholium_1, city_scholium_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=City&pk={city_1.pk}&action_type=deassociate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_city_if_not_creator(
        self, client_editor, delete_url, city_1, scholium_1, city_scholium_1, regular
    ):
        assert city_1 in scholium_1.cities.all()
        city_scholium_1.user = regular
        city_scholium_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert city_1 in scholium_1.cities.all()
