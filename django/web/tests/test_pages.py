from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.mark.django_db
class TestPage:
    def test_fallback_to_english_pages(self, client):
        response = client.get(
            reverse("web:pages", args=("index",)), HTTP_ACCEPT_LANGUAGE="it"
        )
        assert response.status_code == HTTPStatus.OK
        assert "Documentazione" in str(response.content)
        assert (
            "Pour une édition numérique collaborative de l’<em>Anthologie grecque</em>"
            in response.content.decode("utf-8")
        )
        assert (
            '<li><a href="../a-propos/">General informations about the project</a></li>'
            in response.content.decode("utf-8")
        )

    def test_can_display_french_pages(self, client):
        response = client.get(
            reverse("web:pages", args=("index",)), HTTP_ACCEPT_LANGUAGE="fr"
        )
        assert response.status_code == HTTPStatus.OK
        assert "Documentation" in str(response.content)
        assert (
            "Pour une édition numérique collaborative de l’<em>Anthologie grecque</em>"
            in response.content.decode("utf-8")
        )
        assert (
            '<li><a href="../a-propos/">Des informations générales sur le projet</a></li>'
            in response.content.decode("utf-8")
        )

    def test_can_display_english_pages(self, client):
        response = client.get(
            reverse("web:pages", args=("index",)), HTTP_ACCEPT_LANGUAGE="en"
        )
        assert response.status_code == HTTPStatus.OK
        assert "Documentation" in str(response.content)
        assert (
            "Pour une édition numérique collaborative de l’<em>Anthologie grecque</em>"
            in response.content.decode("utf-8")
        )
        assert (
            '<li><a href="../a-propos/">General informations about the project</a></li>'
            in response.content.decode("utf-8")
        )
