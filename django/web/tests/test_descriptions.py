import pytest

from django.urls import reverse


@pytest.fixture
def passage_description_create_url(passage_1):
    return reverse("web:passage-description-create", args=(passage_1.pk,))


@pytest.mark.django_db
def test_render_description_markdown(
    client_editor, passage_description_create_url, passage_1, language_english
):
    passage_1.descriptions.clear()
    client_editor.post(
        passage_description_create_url,
        data={
            "description": """
# Main title

A list:

* foo
* bar

## Second title

Something **strong** and with *emphasis*.
            """,
            "language": language_english.code,
        },
    )

    first_description = passage_1.descriptions.first()
    assert (
        first_description.description_html()
        == """<h3>Main title</h3>
<p>A list:</p>
<ul>
<li>foo</li>
<li>bar</li>
</ul>
<h4>Second title</h4>
<p>Something <strong>strong</strong> and with <em>emphasis</em>.</p>"""
    )


@pytest.mark.django_db
def test_render_description_not_in_whitelist(
    client_editor, passage_description_create_url, passage_1, language_english
):
    passage_1.descriptions.clear()
    client_editor.post(
        passage_description_create_url,
        data={
            "description": """
[a link](http://example.org)
<script>evilness</script>
            """,
            "language": language_english.code,
        },
    )

    first_description = passage_1.descriptions.first()
    assert first_description.description_html() == """<p>a link</p>\nevilness"""
