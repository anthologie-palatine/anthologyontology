from http import HTTPStatus

import pytest
import responses
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def list_url():
    return reverse("web:city-list")


@pytest.fixture
def detail_url(city_1):
    return reverse("web:city-detail", args=(city_1.pk,))


@pytest.fixture
def create_url_wikidata():
    return reverse("web:city-create-wikidata")


@pytest.fixture
def city_payload():
    def city_data(wiki_id):
        """Return a dict payload with a Wikidata URL (parameter)"""
        return {
            "alternative_urn_url_auto": f"https://www.wikidata.org/wiki/{wiki_id}",
        }

    return city_data


@pytest.mark.django_db
class TestCityAnonymous:
    def test_can_access_cities_page(self, client, list_url, city_1):
        response = client.get(list_url)
        assert response.status_code == HTTPStatus.OK
        assert "Cities" in str(response.content)
        assert city_1.names.first().name in str(response.content)

    def test_can_access_city_page(self, client, detail_url, city_1):
        response = client.get(detail_url)
        assert response.status_code == HTTPStatus.OK
        assert city_1.names.first().name in str(response.content)

    def test_cannot_access_create_page_wikidata(self, client, create_url_wikidata):
        response = client.get(create_url_wikidata)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page_wikidata(
        self, client, create_url_wikidata, city_payload
    ):
        response = client.post(create_url_wikidata, city_payload("Q23482"))
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestCityEditor:
    def test_can_access_create_page_wikidata(self, client_editor, create_url_wikidata):
        response = client_editor.get(create_url_wikidata)
        assert response.status_code == HTTPStatus.OK

    @responses.activate
    def test_can_create_city_from_wikidata(
        self,
        client_editor,
        create_url_wikidata,
        city_payload,
        language_english,
        language_italian,
        wikidata_json,
    ):
        from meleager.models import City, URN

        assert City.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q23482.json",
            json=wikidata_json("Q23482"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, city_payload("Q23482"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/cities/")
        assert City.objects.count() == 1

        urn = URN.objects.get(urn="https://www.wikidata.org/wiki/Q23482")
        assert urn.source == "wikidata"

        city_marseille = City.objects.get(
            names__name="Marseille", names__language=language_english
        )
        assert urn in city_marseille.alternative_urns.all()
        assert City.objects.get(
            names__name="Marsiglia", names__language=language_italian
        )

    @responses.activate
    def test_can_create_city_from_wikidata_with_next_url(
        self,
        client_editor,
        create_url_wikidata,
        city_payload,
        language_english,
        language_italian,
        wikidata_json,
        book_1,
        passage_1,
    ):
        from meleager.models import City

        assert City.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q23482.json",
            json=wikidata_json("Q23482"),
            status=HTTPStatus.OK,
        )
        payload = city_payload("Q23482")
        passage_url = reverse(
            "web:passage-detail",
            args=(book_1.number, passage_1.fragment, passage_1.sub_fragment),
        )
        payload["next_url"] = f"{passage_url}#passage-city-create"
        response = client_editor.post(create_url_wikidata, payload)

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith(
            "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/?city_created_pk="
        )
        assert response.url.endswith("#passage-city-create")
        assert City.objects.count() == 1

    @responses.activate
    def test_cannot_create_duplicate_city_from_wikidata(
        self,
        client_editor,
        create_url_wikidata,
        city_payload,
        language_english,
        language_italian,
        wikidata_json,
    ):
        from meleager.models import City, URN

        assert City.objects.count() == 0

        # This first request should create a City (setup step).
        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q23482.json",
            json=wikidata_json("Q23482"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, city_payload("Q23482"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/cities/")
        assert City.objects.count() == 1

        # We post the same request again, should not create any
        # duplicate or modification.
        response = client_editor.post(create_url_wikidata, city_payload("Q23482"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/cities/")
        assert City.objects.count() == 1

        urn = URN.objects.get(urn="https://www.wikidata.org/wiki/Q23482")
        assert urn.source == "wikidata"

        city_marseille = City.objects.get(
            names__name="Marseille", names__language=language_english
        )
        assert urn in city_marseille.alternative_urns.all()
        assert City.objects.get(
            names__name="Marsiglia", names__language=language_italian
        )

    @responses.activate
    def test_can_create_city_from_wikidata_generate_version(
        self,
        client_editor,
        create_url_wikidata,
        city_payload,
        language_english,
        wikidata_json,
        languages,
    ):
        from meleager.models import City

        assert City.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q23482.json",
            json=wikidata_json("Q23482"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, city_payload("Q23482"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/cities/")
        assert City.objects.count() == 1

        city = City.objects.get(
            names__name="Marseille", names__language=language_english
        )
        versions = Version.objects.get_for_object(city)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=City&pk={city.pk}&action_type=create&relation_type="
        )
