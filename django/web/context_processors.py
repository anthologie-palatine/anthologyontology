from meleager.models import Language


def preferred_languages(request):
    return {"preferred_languages": Language.objects.preferred()}
