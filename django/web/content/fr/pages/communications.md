---
title: Communications 
---

## Événements à venir 

- [Horizons de la philologie numérique. L’*Anthologie grecque* pour repenser [formats], [paradigmes] et [collaboration]](https://horizons.ecrituresnumeriques.ca/) : la CRCEN s'associe au Dipartimento di Studi Umanistici (Università degli Studi di Napoli Federico II) pour organiser trois journées d'étude -- des 16 au 18 avril à la Federico II -- consacrées à l'actualité de la philologie à l'ère du numérique. Les journées d'étude se concentreront sur l'étude de cas de l'*Anthologie grecque*, permettant de valoriser la plateforme et les données issues de la plateforme [Anthologia graeca](https://anthologiagraeca.org/). Chaque journée sera dédiée à un des trois axes de l’événement : Paléographie, HTR et reconstruction numérique ; Philologie collaborative ; Épistémologie des textes : édition critique de textes classiques. Les matinées seront dédiées à des conférences et les après-midi à des ateliers pratiques permettant d’explorer concrètement les concepts discutés et d’appliquer certaines des méthodologies présentées. 

## Évenements 

- [L’Édition numérique collaborative de l’Anthologie grecque : ateliers pratiques](https://www.crihn.org/nouvelles/2023/02/26/ledition-numerique-collaborative-de-lanthologie-grecque-ateliers-pratiques/) : Dans le cadre du partenariat mis au point dès 2015 avec le Liceo classico Luca De Samuele Cagnazzi (Altamura), la CRCEN (Marcello Vitali-Rosati et Mathilde Verstraete), avec le concours de Servanne Monjour ([CELLF](https://cellf.cnrs.fr/), Sorbonne Université), a organisé trois journées d'ateliers (les 4, 6 et 7 mars 2023), dont l’objectif est d’apporter une réflexion théorique et concrète sur les enjeux et potentialités pratiques offertes par L’édition numérique collaborative de l’Anthologie grecque. 

- [Navigations anthologiques : l'*Anthologie grecque* à l'heure des Digital Classics](https://navigations.ecrituresnumeriques.ca/) : La CRCEN organise, les 27, 28, et 29 octobre trois journées d'étude sur l'actualité de l'*Anthologie grecque*. Cette rencontre a pour but d’appeler la communauté des *Digital Classicists* à réfléchir autour des enjeux scientifiques liés à la diffusion du patrimoine des langues et littératures en grec ancien et plus généralement aux impacts méthodologiques et épistémologiques des *Digital Humanities* dans le cadre des études hellénistiques, à partir du cas d’étude de l'*Anthologie grecque*. Tout en s’articulant autour du corpus anthologique, cet événement a pour but de faire la passerelle entre les termes « digital » et « classicists », de provoquer une rencontre entre des chercheur·euse·s, professeur·e·s et programmeur·euse·s de profils divers et aux intérêts parfois éloignés pour que se crée une discussion pérenne et qu’émanent de nouvelles potentialités de recherche. 
Le samedi 29 octobre 2022 sera consacré à une journée de hackathon : les participant présenter des prototypes utilisant des méthodes algorithmiques afin d’explorer et d’analyser le corpus de l’*Anthologie grecque*. Cette dernière journée permettra d’utiliser comme tremplin ce qui a été produit pendant le projet « *Anthologie grecque* » de la CRCEN d’une part, pendant les journées d’étude d’autre part, tout en faisant émerger de nouvelles potentialités de recherche. 
L'inscription est recommandée pour les deux premières journées, obligatoire pour le hackathon du 29 octobre (date limite : 10 octobre 2022). Toutes les informations sont sur [ici](https://navigations.ecrituresnumeriques.ca/). 
Les [archives](https://navigations.ecrituresnumeriques.ca/archives/) (vidéos des conférences et photos) ont été ajoutées sur le site ! 

## Articles 

- Dossier « Miscellanea anthologica », Mathilde Verstraete (dir.), Sens public, 2024 : 
  - Simone Beta, « Il piede nella poesia enigmistica greca: tema e variazioni » ; 
  - Lucia Floridi, « La rosa non è una rosa.  Per una valorizzazione della tradizione testuale delle Sillogi Minori » ; 
  - Émile Caron et Maxime Guénette, « Éditer la plateforme *Anthologia Graeca* : vecteur de changements dans la pratique des *classics* » ; 
  - Marcello Vitali-Rosati, « De l’intelligence artificielle aux modèles de définition de l’intelligence. Le cas des variations dans l’Anthologie Grecque » ; 
  - Antoine Fauchié, Lena Krause, Margot Mellet, Enzo Poggio et Marianne Reboul, « Le projet Graph ton Anthologie Grecque (GAG).  Restitution d’une expérience de Hackathon ».
- Verstraete Mathilde et Mellet Margot, « Passés et présents anthologiques. Modèles de valorisation et d’appropriation de patrimoines ancien. Le projet d’édition numérique collaborative de l’*Anthologie grecque* », dans Severo Marta et Sauret Nicolas (dir.), *Communautés et pratiques d’écritures des patrimoines et des mémoires*, Presses Universitaires de Paris Nanterre, 2024. 
- Antoine Fauchié, Enrico Agostini-Marchese, Timothée Guicherd, [et al.], « L’épopée numérique de l’*Anthologie grecque* : entre questions épistémologiques, modèles techniques et dynamiques collaboratives », Sens Public, 2021, [https://sens-public.org/articles/1603/](https://sens-public.org/articles/1603/). 
- Margot Mellet, « Penser le palimpseste numérique. Le projet d’édition numérique collaborative de l’Anthologie palatine », Captures, vol. 5 / 1, mai 2020, [https://revuecaptures.org/node/4382/](https://revuecaptures.org/node/4382/). 
- Marcello Vitali-Rosati, Servanne Monjour, Joana Casenave, [et al.], « Editorializing the Greek Anthology: The palatin manuscript as a collective imaginary », Digital Humanities Quarterly, vol. 014 / 1, 2020, [http://www.digitalhumanities.org/dhq/vol/14/1/000447/000447.html](http://www.digitalhumanities.org/dhq/vol/14/1/000447/000447.html). 

## Conférences en vidéo 

- Journées d'étude « [Navigations anthologiques : l'*Anthologie grecque* à l'heure des Digital Classics](https://navigations.ecrituresnumeriques.ca/) », Montréal, 27-29/10/2022 : https://navigations.ecrituresnumeriques.ca/archives/.

- Verstraete Mathilde, « Les figures de l'auteur au sein du projet d'édition numérique collaborative de l'*Anthologie grecque* », La Sapienza, 6/5/2022 : https://nakala.fr/10.34847/nkl.cb0fj4y7.

- Enrico Agostini-Marchese, Elsa Bouchard, Joana Casenave, Arthur Juchereau, Nicolas Sauret et Marcello Vitali-Rosati, « Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'Anthologie Palatine », ECRIDIL, Montréal, 30/04/2018
<iframe width="560" height="315" src="https://www.youtube.com/embed/lQ_cqfWLbQ0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- Mellet Margot, « A.P. POP », Colloque du CRIHN, Montréal, 27/10/2018 
<iframe width="560" height="315" src="https://www.youtube.com/embed/YK21jideSTI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Présentations et diapositives

- [A decade of the Greek Anthology project.Achievements, successes, challenges](http://vitalirosati.net/slides/2024/conf-2024-04-16horizon.html#title) - Horizons de la philologie numérique - Napoli - 16/04/2024 
- [D'une plateforme d'édition collaborative à un projet d'intelligence artificielle littéraire. Collaboration et production du savoir : pour une herméneutique des structures](https://www.vitalirosati.net/slides/2023/conf-2023-05-30csdhAP.html#/title-slide) - CSDH - York University - 30/05/2023
- [Uno sguardo digitale sull'*Antologia greca*.Piattaforme, metodi e strumenti dell'informatica umanistica](https://www.vitalirosati.net/slides/2023/conf-2023-03-28apnapoli.html#/title-slide) - Napoli - 28/03/2023 
- [The Anthology on the Web : *quid novi* ?](https://www.vitalirosati.net/slides/2022/conf-2022-10-27apNavigations.html#/title-slide) - Navigations Anthologiques - Montréal - 27/10/2022 
- [Le projet Anthologie grecque. Entre choix épistémologiques et code](https://www.vitalirosati.net/slides/2022/conf-2022-09-27ap.html#/title-slide) - Montréal - 27/09/2022 
- [Technical thinking: the "Greek Anthology Project" between epistemological choices and code](https://www.vitalirosati.net/slides/2022/conf-2022-05-23apParma.html#/title-slide) - ENCODE - Parma - 23/05/2022
- [Des algorithmes pour définir nos concepts littéraires](https://demo.hedgedoc.org/p/DhjWUJB_R#/) - Humanistica - Montréal - 21/05/2022 
- [Dall'informatica umanistica all'edizione collaborativa dell’Antologia Greca](https://www.vitalirosati.net/slides/2022/conf-2022-03-18apnapoli.html#/title-slide) - Napoli - 18/03/2022 
- [Seconde navigation: nouvelles et développements du projet d’édition collaborative numérique de l’Anthologie Palatine](https://demo.hedgedoc.org/p/1JfrRXQ0k#/) - Humanistica - 10/05/2021
- [The Palatine Anthology project: an API allowing new interactions with the Greek epigrams](http://vitalirosati.net/slides/conf-2018-05-27apRegina.html#/) - Regina - 27/05/18
- [Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'Anthologie Palatine](https://www.vitalirosati.net/slides/conf-2018-04-30apEcridil.html#/) - ECRIDIL - Montréal - 30/04/2018
- [Une API pour l'*Anthologie grecque*: repenser le codex Palatinus à l'époque du numérique](http://vitalirosati.net/slides/conf-2018-03-15ap.html#/) - CNRS - Paris - 15/03/2018
- [La genèse de l'Anthologie: pour une édition participative du codex palatinus](http://vitalirosati.net/slides/conf-2018-02-28ap.html#/)  - Rouen - 28/02/2018
- [Éditer l’imaginaire anthologique: une api pour le codex palatinus](http://vitalirosati.net/slides/conf-2018-01-26ap.html) - Vitrine sur les Humanités Numériques, Université de Montréal 26/01/2018
- [Pour une version numérique de l’Anthologie Palatine](http://vitalirosati.net/slides/conf-2017-01-25-anthologie.html#/) - Vitrine DH McGill - 26/01/17
- [Per una versione digitale dell’Antologia Palatina](http://vitalirosati.net/slides/conf-2016-12-09-anthologie.html#/) - 09/12/16
- [Pour une version numérique de l'Anthologie Palatine](http://vitalirosati.net/slides/conf-2015-04-12-anthologie.html) - Atelier sur les éditions critiques en contexte numérique, organisé par le Centre de recherche Virtuoso sur les usages, cultures et documents numériques. Montréal, 3/12/2015

## Billets de blogue

- [Une API pour l’*Anthologie grecque*](https://movi.hypotheses.org/237)
- [Pour une édition participative de l’Anthologie Palatine](http://blog.sens-public.org/marcellovitalirosati/pour-une-edition-participative-de-lanthologie-palatine/)
- [Midnight in Paris, l’Anthologie Palatine et la conjoncture médiatrice](http://blog.sens-public.org/marcellovitalirosati/midnight-in-paris-lanthologie-palatine-et-la-conjoncture-mediatrice/)

## Autre

Vidéo réalisée par le Lycée d'État N. Cagnazzi d'Altamura présentant le projet *Antologia Palatina* en collaboration avec la Chaire de recherche du Canada sur les écritures numériques de l'Université de Montréal
<iframe width="560" height="315" src="https://archive.org/details/videoantologiapalatinadef" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>