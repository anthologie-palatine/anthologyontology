---
title: Équipe et partenaires
---

## L'équipe 

Ce projet réuni plusieurs chercheur·e·s dont : 

### Chercheur·e·s princiaux·ales :
- [Marcello Vitali-Rosati](http://ecrituresnumeriques.ca/fr/Equipe/Marcello-Vitali-Rosati-) 
- [Elsa Bouchard](https://www.crihn.org/membres/bouchard-elsa/)
- [Christian Raschle](https://cetcl.umontreal.ca/repertoire-departement/professeurs/professeur/in/in15420/sg/Christian%20Raschle/)

### Coordinatrice du projet :
- [Mathilde Verstraete](https://ecrituresnumeriques.ca/fr/Equipe/Mathilde-Verstraete)

### Développement technique : 
- David Larlet
- Sarah Rubio
- Timothée Guicherd

### Éditeur·trice·s 
- Luiz Capelo 
- William Bouchard
- Maxime Guenette
- Epheline Bernaer 

### Collaborateur·trice·s  
- [Margot Mellet](http://ecrituresnumeriques.ca/fr/Equipe/Margot-Mellet) 
- [Enrico Agostini-Marchese](http://ecrituresnumeriques.ca/fr/Equipe/Enrico-Agostini-Marchese)
- [Nicolas Sauret](http://ecrituresnumeriques.ca/fr/Equipe/Nicolas-Sauret)
- [Servanne Monjour](http://ecrituresnumeriques.ca/fr/Equipe/Servanne-Monjour)
- [Arthur Juchereau](http://ecrituresnumeriques.ca/fr/Equipe/Arthur-Juchereau)
- [Joana Casenave](http://ecrituresnumeriques.ca/fr/Equipe/Joana-Casenave)
- [Marie-Christine Corbeil](http://ecrituresnumeriques.ca/fr/Equipe/Marie-Christine-Corbeil), 
- Katerina Tzotzi
- [Gregory Crane](http://www.dh.uni-leipzig.de/wo/gregory-crane/)

### Nos ancien·ne·s stagiaires *Mitacs* 
- Matilda Chapman 
- Claire Delaigle 

## Les partenaires

- [La Chaire de recherche du Canada sur les écritures numériques - Université de Montéal](http://ecrituresnumeriques.ca)
- [CRIHN](http://crihn.org/)
- [GREN](https://gren.openum.ca/)
- [Perseus](http://www.perseus.tufts.edu/hopper/)
- [Perseids](https://www.perseids.org/)
- [Hetic](https://www.hetic.net/) : collaboration pour le site de la [POP](http://pop.anthologiegrecque.org/#/)
- [Liceo Classico Cagnazzi](http://www.liceocagnazzi.gov.it) : traduction et mise en ligne de textes, sous le regard attentif de leur professeure, Annalisa di Vincenzo.

## Financements 

- 2019 - 2025 
Le projet a reçu un financement ["Subventions savoir"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_grants-subventions_savoir-fra.aspx) du Conseil de Recherches en Sciences Humaines du Canada [CRSH](http://www.sshrc-crsh.gc.ca).

- 2017 - 2019 
Le projet a reçu un financement ["Subventions de développement Savoir"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-subventions_de_developpement_savoir-fra.aspx) du Conseil de Recherches en Sciences Humaines du Canada [CRSH](http://www.sshrc-crsh.gc.ca).

## Financements de projets liés 

- L'organisation des journées d'études [Navigations anthologiques : l'*Anthologie grecque* à l'heure des Digital Classics](https://navigations.ecrituresnumeriques.ca/) (27-29 octobre 2022), a reçu un financement ["Subvention Connexion 2022-2024"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/connection_grants-subventions_connexion-fra.aspx) du Conseil de Recherches en Sciences Humaines du Canada [CRSH](http://www.sshrc-crsh.gc.ca).

- Le projet *Intelligence Artificielle Littéraire (IAL) : pour un modèle algorithmique de la variation dans l'Anthologie grecque* a reçu un financement ["Subventions de développement Savoir 2022-2024"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-subventions_de_developpement_savoir-fra.aspx) du Conseil de Recherches en Sciences Humaines du Canada [CRSH](http://www.sshrc-crsh.gc.ca).


## Nous contacter 

Pour plus d'informations, vous pouvez nous contacter à l'[adresse email de la chaire](mailto:crc.ecrituresnumeriques@gmail.com).

Nous prendrons en considération des demandes spécifiques d'adaptation de l'API à des exigences de recherche particulières - par exemple l'adaptation à un autre corpus.