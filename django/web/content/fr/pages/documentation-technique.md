---
title: Prise en main de la plateforme
---

## La plateforme Anthologia Graeca

### Création de compte et connexion

L'utilisateur·trice type sera désigné·e sous le nom *Méléagre*. Par défaut, la plateforme est en français mais elle est disponible en plusieurs langues : en haut de page se trouve l'onglet « Langage » qui permet de sélectionner une autre langue d'interface. 

Méléagre se connecte sur la plateforme [*Anthologia Graeca*](http://anthologiagraeca.org/). 
Sur la page d'accueil, il retrouve la liste des épigrammes de l'*Anthologie grecque* regroupées par livres. 

Pour éditer les informations de la plateforme, Méléagre doit se connecter avec un compte d'utilisateur·trice. 

En haut de la page, à droite, il a accès à l'onglet « Compte » pour créer un nouveau compte ou se connecter à un compte déjà existant. 

Pour créer un nouveau compte, Méléagre doit faire la demande en communiquant avec [la CRCEN](mailto:crc.ecrituresnumeriques@gmail.com).

<!--Pour la création d'un nouveau compte, Méléagre doit renseigner : 

- Son username - obligatoire
- Son prénom - facultatif
- Son nom - facultatif
- Son adresse mail - facultatif
- Son mot de passe - obligatoire
- Son institution - facultatif
- Sa langue préférée - facultatif - à implémenter dans le modèle user?
- Son édition par défaut - facultatif - à implémenter dans le modèle user?

Une fois qu'il a renseigné les champs obligatoire il peut automatiquement créer son compte et se connecter (il aura des permissions d'utilisateur normal). -->

Une fois connecté, Méléagre peut désormais participer à l'édition numérique de l'*Anthologie grecque* !

<!--a accès à sa [page user] et--> 
Pour se déconnecter, il lui suffira de cliquer sur son nom d'utilisateur (apparu en haut à droite à la place de l'onglet « Compte ») et de sélectionner « Déconnexion ». 


<!--## Liste des tâches 

Méléagre se voit proposer des tâches à remplir pour participer à la plateforme. -->

### Page Épigramme 

Chaque épigramme correspond à une entité abstraite, et chaque entité possède sa propre page sur la plateforme. Dès lors, une épigramme = une entité = une page. 

Sur la page d'une épigramme, Méléagre trouve plusieurs informations : 

- le titre de l'épigramme comprenant sa localisation dans l'*Anthologie grecque*. 

> exemple : l'[épigramme 1.19](https://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.19/) est l'épigramme 19 du livre 1.

- l'URN de l'épigramme : c'est l'identifiant de l'épigramme dans la plateforme [Perseus](http://www.perseus.tufts.edu/hopper/) à partir de laquelle les transcritpions du texte en grec ont été récupérées.

> exemple : l'épigramme 1.19 a l'URN "urn:cts:greekLit:tlg7000.tlg001" et est indexée dans le catalogue de Perseus ici : [https://catalog.perseus.org/catalog/urn:cts:greekLit:tlg7000.tlg001](https://catalog.perseus.org/catalog/urn:cts:greekLit:tlg7000.tlg001).

- trois autres indications sont présentes directement sous le titre d'une épigramme : "créé le", "par", "mis à jour le". Il s'agit des informations de création de cette entité/épigramme sur cette plateforme. 

- la section description, qui permet de décrire (brièvement) le contenu d'une épigramme. 

> exemple : l'[épigramme 5.75](https://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.ag:5.75/) à été baptisée "la fille mise à mal" par Marguerite Yourcenar. 

- une image du manuscrit *Codex Palatinus 23* où se trouve l'épigramme. La légende de l'image renvoie à la page de l'épigramme dans le manuscrit.

- la section textes présente les différents matériaux textuels de l'épigramme : les versions grecques et les traductions (grc pour le grec, fra pour le français, eng pour l'anglais, it pour l'italien, port pour le portugais). En sélectionnant les différents onglets des versions, Méléagre peut personnaliser sa lecture. 

- l'auteur : l'auteur (antique) de l'épigramme. Quand l'attribution est douteuse, nous préconisons de laisser l'auteur mais de faire un commentaire (plus bas) à ce sujet. 

- la ville : les lieux associés à ou mentionnés par l'épigramme.

- les mots-clefs : les mots-clefs associés à l'épigramme organisés par catégories. En cliquant sur un mot-clef, Méléagre accède à la page du mot-clef qui liste les épigrammes communes. Pour ajouter un mot-clé, Méléagre doit le classer dans une catégorie et en donner l'URL wikidata (= son identifiant). 

- la section scholia : liste les scholies de l'épigramme et les images des scholies dans le *Codex Palatinus 23*. En cliquant sur le titre de la scholie, Méléagre accède à la page de la scholie où se trouvent sa transcription et ses traductions. Méléagre peut également y ajouter une ville, des mots-clés, et des commentaires.

- la section commentaire : Méléagre est libre de faire tout commentaire dans cette section (questions relevant de la datation ou de l'autorité de l'épigramme, anecdotes sur le sujet, commentaires quant à une référence proposée plus bas,...)

- la section alignement permet de lier les mots entre différentes versions de l'épigramme. En sélectionnant les onglets, Méléagre peut choisir l'alignement à lire. Pour ajouter un alignement, Méléagre choisit les deux versions à aligner. Pour chaque (groupe de) mots à aligner, il clique sur chacune des versions et presse "align" ou la touche "y" de son clavier. Après avoir soumis son alignements, les mots qui n'ont pas été alignés sont légèrement soulignés. Il n'est possible d'aligner que deux textes à la fois. 

- les références internes présentent les références entre les épigrammes. En cliquant sur le titre de l'épigramme en référence interne, Méléagre accède à la page de l'épigramme associée. Cette rubrique est particulièrement utilisée dans la développement du projet pilote *Intelligence Artificelle Littéraire* et font référence à des variations entre diverses épigrammes. Méléagre peut choisir quatre type de références internes : 
    - default s'il ne désire pas la typer 
    - stylistique : la variation stylistique porte sur les mots et leur agencement, le lexique ou le style
    - rhétorique : dans la variation rhétorique, l’objet persiste mais la structure change
    - paradigmatique : dans une variation paradigmatique, le sujet lui-même varie mais la structure reste relativement la même 

- les références externes sont les liens entre l'épigrammes et des contenus culturels externes à l'*Anthologie grecque*. Méléagre saisit un titre et l'URL de l'objet lié. Méléagre peut développer ce lien dans la rubrique commentaire. 

- media : la section media permet à Méléagre d'ajouter un media (image, audio, video) de l'épigramme. Il s'agit de représentations effectives d'une épigramme : une image de l'inscription, un enregistrement ou une vidéo de l'épigramme récitée ou déclamée,...

Méléagre peut consulter les dernières modifications apportées à une épigramme et qui en est l'auteur. 

Méléagre peut, en cliquant sur la barre de navigation en bas de page, accéder à l'épigramme précédente ou suivante. 

### Menu 

Dans l'en-tête, un onglet de menu vous permet :

- de revenir à la page d'accueil listant les épigrammes par livre
- de consulter la liste des auteur·e·s de l'*Anthologie* : Méléagre peut facilement retrouver les épigrammes écrites par chaque auteur·e·s (à condition que le nom de l'auteur·e ait été ajouté sur la page de l'épigramme).
- de consulter la liste des villes mentionnées dans l'*Anthologie*. Quand une ville est éditée, Méléagre y trouve les épigrammes et scholies qui l'évoquent, ainsi que les auteurs qui y sont nés ou morts. 
- de consulter la liste des mots-clés, classés par catégorie, ainsi que les épigrammes qui sont caractérisées par chacun des mots-clés.

### Édition 

Sur le côté de chacune des sections, une icône "+" permet à Méléagre d'ajouter des contenus. Tout utilisateur peut ajouter et éditer des contenus. Il n'est pas possible de modifier les informations créées par un autre utilisateur (à moins d'avoir des privilèges d'administrateur). 

Si vous désirez annoter et ajouter les images du manuscrit, veuillez prendre contact avec [la CRCEN](mailto:crc.ecrituresnumeriques@gmail.com)


### À vous de jouer 

À vous de prendre en main la plateforme !

Pour toute question, remarque, ou commentaire, vous pouvez contacter la [Chaire de Recherche du Canada sur les Écritures Numériques](mailto:crc.ecrituresnumeriques@gmail.com).

## L'API 

Méléagre peut faire des requêtes et interroger l'[API](https://anthologiagraeca.org/api/) en JSON.