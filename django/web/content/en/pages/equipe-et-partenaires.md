---
title: Team and partners
---

## The team 

This project brings together several researchers including :

### Main researchers :
- [Marcello Vitali-Rosati](http://ecrituresnumeriques.ca/fr/Equipe/Marcello-Vitali-Rosati-) 
- [Elsa Bouchard](https://www.crihn.org/membres/bouchard-elsa/)
- [Christian Raschle](https://cetcl.umontreal.ca/repertoire-departement/professeurs/professeur/in/in15420/sg/Christian%20Raschle/)

### Project coordinator :
- [Mathilde Verstraete](https://ecrituresnumeriques.ca/fr/Equipe/Mathilde-Verstraete)

### Technical development : 
- David Larlet
- Sarah Rubio
- Timothée Guicherd

### Editors :  
- Luiz Capelo 
- William Bouchard
- Maxime Guenette
- Epheline Bernaer 

### Collaborators :  
- [Margot Mellet](http://ecrituresnumeriques.ca/fr/Equipe/Margot-Mellet) 
- [Enrico Agostini-Marchese](http://ecrituresnumeriques.ca/fr/Equipe/Enrico-Agostini-Marchese)
- [Nicolas Sauret](http://ecrituresnumeriques.ca/fr/Equipe/Nicolas-Sauret)
- [Servanne Monjour](http://ecrituresnumeriques.ca/fr/Equipe/Servanne-Monjour)
- [Arthur Juchereau](http://ecrituresnumeriques.ca/fr/Equipe/Arthur-Juchereau)
- [Joana Casenave](http://ecrituresnumeriques.ca/fr/Equipe/Joana-Casenave)
- [Marie-Christine Corbeil](http://ecrituresnumeriques.ca/fr/Equipe/Marie-Christine-Corbeil), 
- Katerina Tzotzi
- [Gregory Crane](http://www.dh.uni-leipzig.de/wo/gregory-crane/)

### Our former *Mitacs* trainees  
- Matilda Chapman 
- Claire Delaigle 

## Partners

- [The Canada Research Chair on Digital Textualities - University of Montreal](http://ecrituresnumeriques.ca)
- [CRIHN](http://crihn.org/)
- [GREN](https://gren.openum.ca/)
- [Perseus](http://www.perseus.tufts.edu/hopper/)
- [Perseids](https://www.perseids.org/)
- [Hetic](https://www.hetic.net/) : collaboration for the [POP's Website](http://pop.anthologiegrecque.org/#/)
- [Liceo Classico Cagnazzi](http://www.liceocagnazzi.gov.it) : translation and publication of texts, under the watchful eye of their teacher, Annalisa di Vincenzo. 

## Fundings 

- 2019 - 2025 
The project has received an ["Insight Grant"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_grants-subventions_savoir-eng.aspx) from the Social Sciences and Humanities Research Council [SSHRC](https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx).

- 2017 - 2019 
The project has received an ["Insight Development Grant"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-subventions_de_developpement_savoir-eng.aspx) from the Social Sciences and Humanities Research Council [SSHRC](https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx). 

## Funding of related projects

- The organization of the workshop [Anthological Navigations : The Greek Anthology in the Era of Digital Classics](http://navigations.digitaltextualities.ca/) (27<sup>th</sup>-29<sup>th</sup> of October, 2022), has received a["Connection Grant 2022-2024"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/connection_grants-subventions_connexion-eng.aspx) from the Social Sciences and Humanities Research Council [SSHRC](https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx). 

- The projet *Intelligence Artificielle Littéraire (IAL) : for an algorithmic model of variation in the Greek Anthology* has received an ["Insight Development Grant 2022-2024"](https://www.sshrc-crsh.gc.ca/funding-financement/programs-programmes/insight_development_grants-subventions_de_developpement_savoir-eng.aspx) from the Social Sciences and Humanities Research Council [SSHRC](https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx). 


## Contact us

For more information, you can contact us at [the CRCDT's email address](mailto:crc.ecrituresnumeriques@gmail.com).

We will consider specific requests to adapt the API to particular research requirements - for example, adaptation to another corpus.