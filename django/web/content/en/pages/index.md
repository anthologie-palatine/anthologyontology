---
title: Index
--- 

## The projet

“Pour une édition numérique collaborative de l’*Anthologie grecque*” is a project of the [Canada Research Chair on Digital Textualities](http://ecrituresnumeriques.ca/en/) directed by Marcello Vitali-Rosati in collaboration with Elsa Bouchard and Christian Raschle.

## This documentation

On this site you will find :

1. [General informations about the project](../a-propos/)
2. [A list and description of the different resources of the project](../ressources/)
3. [A guide on how to use the use of the platform and the API](../documentation-technique/)
4. [Events, conferences, and articles related to the project](../communications/)
5. [Information about our team and our partners, as well as our contacts](../equipe-et-partenaires/)

