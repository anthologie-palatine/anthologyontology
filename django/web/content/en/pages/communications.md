---
title: Communications 
---

## Upcoming events 

- [Horizons of digital philology. The *Greek Anthology* for rethinking [formats], [paradigms] and [collaboration]](https://horizons.ecrituresnumeriques.ca/en/) : CRCDT is teaming up with the Dipartimento di Studi Umanistici (Università degli Studi di Napoli Federico II) to organize three study days -- from April 16 to 18 at the Federico II -- dedicated to the current landscape of philology in the digital era. This workshop will focus on the case study of the *Greek Anthology*, serving as a compelling case study while leveraging the platform and data of the [*Anthologia graeca* project](https://anthologiagraeca.org/). Each day will be devoted to one of the three main topics of the event: Paleography, HTR and Digital Reconstruction; Collaborative philology; Epistemology of texts; critical edition of classical texts. Lectures will take place on the mornings and the afternoons will offer hands-on workshops allowing concrete exploration of the concepts discussed and application of some of the methodologies presented during the morning sessions. 

## Events 

- [The Collaborative Digital Edition of the Greek Anthology: practical workshops](https://www.crihn.org/nouvelles/2023/02/26/ledition-numerique-collaborative-de-lanthologie-grecque-ateliers-pratiques/): As part of the partnership developed back in 2015 with the Liceo classico Luca De Samuele Cagnazzi (Altamura), the CRCDT (Marcello Vitali-Rosati and Mathilde Verstraete), with the contribution of Servanne Monjour ([CELLF](https://cellf.cnrs. fr/), Sorbonne University), has organized three days of workshops (March 4, 6 and 7, 2023), with the aim of providing a theoretical and concrete reflection on the issues and practical potential offered by the collaborative digital edition of the Greek Anthology. 

- [Anthological Navigations : The Greek Anthology in the Era of Digital Classics](http://navigations.digitaltextualities.ca/) : The CRCDT is hosting, on October 27<sup>th</sup>, 28<sup>th</sup>, and 29<sup>th</sup>, a three-day workshop dedicated to the *Greek Anthology*'s actuality. The aim of this meeting is to call upon the community of *Digital Classicists* to reflect on the scientific issues related to the dissemination of the heritage of ancient Greek languages and literatures and, more generally, on the methodological and epistemological impacts of the *Digital Humanities* within the framework of Hellenistic studies, starting from the case study of the *Greek Anthology*. While focusing on the anthological corpus, the three days of the conference aim to bridge the gap between the terms “digital” and “classicists” and to prompt a meeting between researchers, professors and programmers with diverse profiles and sometimes distant interests, so that a perennial discussion can be created and new research potentialities can emerge. 
Saturday the 28<sup>th</sup> of October 2022 will be held as a hackathon: participants will present prototypes using algorithmic methods to explore and analyze the corpus of the *Greek Anthology*. This last day will allow to use as a springboard what has been produced during the CRCDT's "Greek Anthology" project on the one hand, and during the workshop on the other hand, while bringing out new research potentialities.
Registration is recommended for the first two days, and is mandatory for the hackathon on October 29 (deadline: October 10, 2022). All information is available [here](http://navigations.digitaltextualities.ca/). 
The [archives](https://navigations.ecrituresnumeriques.ca/en/archives/) (conference videos and photos) have been added to the site !

## Papers

- Dossier « Miscellanea anthologica », Mathilde Verstraete (dir.), Sens public, 2024 : 
  - Simone Beta, « Il piede nella poesia enigmistica greca: tema e variazioni » ; 
  - Lucia Floridi, « La rosa non è una rosa.  Per una valorizzazione della tradizione testuale delle Sillogi Minori » ; 
  - Émile Caron and Maxime Guénette, « Éditer la plateforme *Anthologia Graeca* : vecteur de changements dans la pratique des *classics* » ; 
  - Marcello Vitali-Rosati, « De l’intelligence artificielle aux modèles de définition de l’intelligence. Le cas des variations dans l’Anthologie Grecque » ; 
  - Antoine Fauchié, Lena Krause, Margot Mellet, Enzo Poggio and Marianne Reboul, « Le projet Graph ton Anthologie Grecque (GAG).  Restitution d’une expérience de Hackathon ».
- Verstraete Mathilde et Mellet Margot, « Passés et présents anthologiques. Modèles de valorisation et d’appropriation de patrimoines ancien. Le projet d’édition numérique collaborative de l’*Anthologie grecque* », dans Severo Marta et Sauret Nicolas (dir.), *Communautés et pratiques d’écritures des patrimoines et des mémoires*, Presses Universitaires de Paris Nanterre, 2024. 
- Antoine Fauchié, Enrico Agostini-Marchese, Timothée Guicherd, [et al.], « L’épopée numérique de l’*Anthologie grecque* : entre questions épistémologiques, modèles techniques et dynamiques collaboratives », Sens Public, 2021, [https://sens-public.org/articles/1603/](https://sens-public.org/articles/1603/). 
- Margot Mellet, « Penser le palimpseste numérique. Le projet d’édition numérique collaborative de l’Anthologie palatine », Captures, vol. 5 / 1, mai 2020, [https://revuecaptures.org/node/4382/](https://revuecaptures.org/node/4382/). 
- Marcello Vitali-Rosati, Servanne Monjour, Joana Casenave, [et al.], « Editorializing the Greek Anthology: The palatin manuscript as a collective imaginary », Digital Humanities Quarterly, vol. 014 / 1, 2020, [http://www.digitalhumanities.org/dhq/vol/14/1/000447/000447.html](http://www.digitalhumanities.org/dhq/vol/14/1/000447/000447.html). 

## Talks (video)

- Study days « [Anthological Navigations. The Greek Anthology in the Era of Digital Classics](https://navigations.ecrituresnumeriques.ca/) », Montréal, 27-29/10/2022 : https://navigations.ecrituresnumeriques.ca/en/archives/.

- Verstraete Mathilde, « Les figures de l'auteur au sein du projet d'édition numérique collaborative de l'*Anthologie grecque* », La Sapienza, 6/5/2022 : https://nakala.fr/10.34847/nkl.cb0fj4y7.

- Enrico Agostini-Marchese, Elsa Bouchard, Joana Casenave, Arthur Juchereau, Nicolas Sauret et Marcello Vitali-Rosati, « Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'Anthologie Palatine », ECRIDIL, Montréal, 30/04/2018
<iframe width="560" height="315" src="https://www.youtube.com/embed/lQ_cqfWLbQ0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- Mellet Margot, « A.P. POP », Colloque du CRIHN, Montréal, 27/10/2018 
<iframe width="560" height="315" src="https://www.youtube.com/embed/YK21jideSTI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Presentations and slides

- [A decade of the Greek Anthology project.Achievements, successes, challenges](http://vitalirosati.net/slides/2024/conf-2024-04-16horizon.html#title) - Horizons de la philologie numérique - Napoli - 16/04/2024 
- [D'une plateforme d'édition collaborative à un projet d'intelligence artificielle littéraire. Collaboration et production du savoir : pour une herméneutique des structures](https://www.vitalirosati.net/slides/2023/conf-2023-05-30csdhAP.html#/title-slide) - CSDH - York University - 30/05/2023
- [Uno sguardo digitale sull'*Antologia greca*.Piattaforme, metodi e strumenti dell'informatica umanistica](https://www.vitalirosati.net/slides/2023/conf-2023-03-28apnapoli.html#/title-slide) - Napoli - 28/03/2023 
- [Uno sguardo digitale sull'*Antologia greca*.Piattaforme, metodi e strumenti dell'informatica umanistica](https://www.vitalirosati.net/slides/2023/conf-2023-03-28apnapoli.html#/title-slide) - Napoli - 28/03/2023 
- [The Anthology on the Web : *quid novi* ?](https://www.vitalirosati.net/slides/2022/conf-2022-10-27apNavigations.html#/title-slide) - Navigations Anthologiques - Montréal - 27/10/2022 
- [Le projet Anthologie grecque. Entre choix épistémologiques et code](https://www.vitalirosati.net/slides/2022/conf-2022-09-27ap.html#/title-slide) - Montréal - 27/09/2022 
- [Technical thinking: the "Greek Anthology Project" between epistemological choices and code](https://www.vitalirosati.net/slides/2022/conf-2022-05-23apParma.html#/title-slide) - ENCODE - Parma - 23/05/2022
- [Des algorithmes pour définir nos concepts littéraires](https://demo.hedgedoc.org/p/DhjWUJB_R#/) - Humanistica - Montréal - 21/05/2022 
- [Dall'informatica umanistica all'edizione collaborativa dell’Antologia Greca](https://www.vitalirosati.net/slides/2022/conf-2022-03-18apnapoli.html#/title-slide) - Napoli - 18/03/2022 
- [Seconde navigation: nouvelles et développements du projet d’édition collaborative numérique de l’Anthologie Palatine](https://demo.hedgedoc.org/p/1JfrRXQ0k#/) - Humanistica - 10/05/2021
- [The Palatine Anthology project: an API allowing new interactions with the Greek epigrams](http://vitalirosati.net/slides/conf-2018-05-27apRegina.html#/) - Regina - 27/05/18
- [Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'Anthologie Palatine](https://www.vitalirosati.net/slides/conf-2018-04-30apEcridil.html#/) - ECRIDIL - Montréal - 30/04/2018
- [Une API pour l'*Anthologie grecque*: repenser le codex Palatinus à l'époque du numérique](http://vitalirosati.net/slides/conf-2018-03-15ap.html#/) - CNRS - Paris - 15/03/2018
- [La genèse de l'Anthologie: pour une édition participative du codex palatinus](http://vitalirosati.net/slides/conf-2018-02-28ap.html#/)  - Rouen - 28/02/2018
- [Éditer l’imaginaire anthologique: une api pour le codex palatinus](http://vitalirosati.net/slides/conf-2018-01-26ap.html) - Vitrine sur les Humanités Numériques, Université de Montréal 26/01/2018
- [Pour une version numérique de l’Anthologie Palatine](http://vitalirosati.net/slides/conf-2017-01-25-anthologie.html#/) - Vitrine DH McGill - 26/01/17
- [Per una versione digitale dell’Antologia Palatina](http://vitalirosati.net/slides/conf-2016-12-09-anthologie.html#/) - 09/12/16
- [Pour une version numérique de l'Anthologie Palatine](http://vitalirosati.net/slides/conf-2015-04-12-anthologie.html) - Atelier sur les éditions critiques en contexte numérique, organisé par le Centre de recherche Virtuoso sur les usages, cultures et documents numériques. Montréal, 3/12/2015

## Blog posts

- [Une API pour l’*Anthologie grecque*](https://movi.hypotheses.org/237)
- [Pour une édition participative de l’Anthologie Palatine](http://blog.sens-public.org/marcellovitalirosati/pour-une-edition-participative-de-lanthologie-palatine/)
- [Midnight in Paris, l’Anthologie Palatine et la conjoncture médiatrice](http://blog.sens-public.org/marcellovitalirosati/midnight-in-paris-lanthologie-palatine-et-la-conjoncture-mediatrice/)

## Other

Video realized by the "liceo classico N. Cagnazzi" (Altamura) presenting the project *Antologia Palatina* in collaboration with the Canada Research Chair on Digital Textualities of the University of Montreal
<iframe width="560" height="315" src="https://archive.org/details/videoantologiapalatinadef" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>