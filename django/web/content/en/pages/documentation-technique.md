---
title: Getting started with the platform
---

## The platform Anthologia Graeca

### Sign up and sign in 

The typical user will be referred to as *Meleager*. By default, the platform is in French, but it is available in several languages: at the top of the page is the "Language" tab which allows you to select another language for the interface.

Meleager connects to the platform [*Anthologia Graeca*](http://anthologiagraeca.org/).
On the home page, they finds a list of the *Greek Anthology* epigrams grouped by books.

To edit the information on the platform, Meleager must log in with their user account.

At the top of the page, on the right, they have access to the "Account" tab to create a new account or to sign in to an existing account.

To create a new account, Meleager must send a sign up request by contacting [the Canada Research Chair on Digital Textualities](mailto:crc.ecrituresnumeriques@gmail.com).

<!--Pour la création d'un nouveau compte, Méléagre doit renseigner : 

- Son username - obligatoire
- Son prénom - facultatif
- Son nom - facultatif
- Son adresse mail - facultatif
- Son mot de passe - obligatoire
- Son institution - facultatif
- Sa langue préférée - facultatif - à implémenter dans le modèle user?
- Son édition par défaut - facultatif - à implémenter dans le modèle user?

Une fois qu'il a renseigné les champs obligatoire il peut automatiquement créer son compte et se connecter (il aura des permissions d'utilisateur normal). -->

Once signed in, Meleager can now contribute to the digital edition of the *Greek Anthology*!

<!--a accès à sa [page user] et--> 
To log out, they will simply click on his username (which appears in the top right corner instead of the "Account" tab) and select "Log Out". 


<!--## Liste des tâches 

Méléagre se voit proposer des tâches à remplir pour participer à la plateforme. -->

### Epigram pages 

Each epigram is considered to be an abstract entity, and each entity has its own page on the platform. Therefore, one epigram = one entity = one page. 

On a given epigram's page, Meleager finds several pieces of information: 

- title : including the epigram's location in the *Greek Anthology*. 

> example : [epigram 1.19](https://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.19/) is epigram 19 of book 1.

- URN : it is the epigram's identifier on the [Perseus](http://www.perseus.tufts.edu/hopper/) platform, from which the Greek transcriptions of the text were recovered.

> example : epigram 1.19 has the URN "urn:cts:greekLit:tlg7000.tlg001" and is indexed in the Perseus catalog : [https://catalog.perseus.org/catalog/urn:cts:greekLit:tlg7000.tlg001](https://catalog.perseus.org/catalog/urn:cts:greekLit:tlg7000.tlg001).

- three other uploading indications: the fields "created on", "by" and "updated on" are present directly under the epigram's title.

- description section: describes briefly the epigram's content.

> example : the [epigram 5.75](https://anthologiagraeca.org/passages/urn:cts:greekLit:tlg7000.tlg001.ag:5.75/) has been named "la fille mise à mal" by Marguerite Yourcenar. 

- image: taken from the manuscript *Codex Palatinus 23*; the caption refers to the page where the epigram is found.

- text section: both the source material and its translations (grc for Greek, fra for French, eng for English, it for Italian, port for Portuguese). By selecting different languages, Meleager can personalize their reading.

- author: the original author of the epigram. When the attribution is doubtful, we recommend adding the author but also a comment about it.

- city: the place or places associated with or mentioned by the epigram.

- keywords: the keywords associated to the epigram, organized by categories. By clicking on a keyword, Meleager accesses the keyword page, which lists epigrams that share the chosen keyword. To add a keyword, Meleager must classify it in a category and provide its Wikidata URL (= its identifier).

- scholia: lists the epigram's scholia and its images from the *Codex Palatinus 23*. By clicking on the scholia's title, Meleager accesses its page, where the transcription and translations are found. Meleager can also add a city, keywords, and comments.

- comments: Meleager is free to make any comment in this section (questions about the dating or the authority of the epigram, anecdotes about the subject, comments about a reference, etc.).

- alignment: this section allows users to link words with the same meaning between different versions of the epigram. Meleager selects two versions or translations of the text to align and, for each group of words they wish to align, they click on them and press "align" or the "y" key on their keyboard. Words which have not been aligned are slightly underlined. It is only possible to align two texts at a time.

- internal references: this section shows references between epigrams. By clicking on the epigram's title in this section, Meleager accesses the internal references page of the associated epigram. This section is particularly used in the development of the *Intelligence Artificielle Littéraire* pilot project and refers to variations between different epigrams. Meleager can choose four types of internal references:
    - default if he does not wish to characterize it
    - stylistic: stylistic variation refers to words and their arrangement, to lexicon or style
    - rhetorical: in rhetorical variation, two epigrams with different structures share the same topic
    - paradigmatic: in paradigmatic variation, the topic varies between two epigrams, but their structure is relatively the same 

- external references: these are the links between the epigram and cultural contents external to the *Greek Anthology*. Meleager enters a title and the URL of the linked object. Meleager can expand on this link in the comment section.

- media: the media section allows Meleager to add media files (image, audio, video) of the epigram. These are actual representations of an epigram: an image of the inscription, a recording or a video of the epigram being recited,...

Meleager can consult the last modifications made to an epigram and their author.

By clicking on the navigation bar at the bottom of the page, Meleager can access the previous or next epigram.

### Menu

In the header, a menu tab allows Meleager :

- to return to the home page listing the epigrams by book
- to consult the list of authors of the *Anthology*: Meleager can easily find the epigrams written by each author (as long as the author's name has been added on the page of the epigram).
- to consult the list of cities mentioned in the *Anthology*. When a city is added, Meleager finds on its page the epigrams and scholia mentioning it, as well as any author who was born or died there (as long as that information has been added to an epigram's page)
- to consult the list of keywords, classified by category, as well as the epigrams associated with those keywords.

### Edition

On the side of each section, a "+" icon allows Meleager to add new content. Any user can add and edit content. It is not possible to modify information created by another user unless you have administrator privileges.

If you wish to annotate and add images to the manuscript, please contact [the Canada Research Chair on Digital Textualities](mailto:crc.ecrituresnumeriques@gmail.com)

### Your turn!

It is now up to you to explore, use and get involved with the platform!

If you have any questions, comments, or concerns, please contact the [the CRCDT](mailto:crc.ecrituresnumeriques@gmail.com). 

## The API

Meleager can make requests and query the [API](https://anthologiagraeca.org/api/) in JSON.