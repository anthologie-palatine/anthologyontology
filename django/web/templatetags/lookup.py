from django import template

register = template.Library()


@register.filter
def ensure_point(value):
    return str(value).replace(",", ".")


@register.filter
def lookup(dict_, key):
    return dict_.get(key)


@register.filter
def tuples_to_dict(list_of_tuples):
    return dict(list_of_tuples)
