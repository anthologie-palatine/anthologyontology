from django import template

register = template.Library()


@register.inclusion_tag("web/alignment/alignment.html")
def render_alignment(text, alignment_data, position):
    return {"data": alignment_data[position]}
