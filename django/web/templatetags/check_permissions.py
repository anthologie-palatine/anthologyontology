from django import template

register = template.Library()


@register.filter
def has_attach_permission(user):
    """All authenticated users can attach an instance to a Passage/Scholium."""
    if user is None:
        return False
    return user.is_authenticated


@register.filter
def has_detach_permission(user, attachment):
    """Only users creators of the given attachment or staff."""
    if user is None:
        return False
    # TODO: make the correct check, using AuthorPassage intermediate
    # model for instance, for the moment we use the dummy permission
    # to still be able to detach an object during our tests.
    # It will probably require to pass the `obj=attachment` parameter.
    is_attachment_creator = user.has_perm("meleager.can_create_mlgr_content")
    return (user.is_authenticated and is_attachment_creator) or user.is_staff


@register.filter
def can_add_mlgr(user):
    """All authenticated users can create an instance of Comment/Description/etc."""
    if not user.is_authenticated:
        return False

    return user.has_perm("meleager.can_add_mlgr")


@register.filter
def can_change(user, instance):
    """Only users creators of the given instance or admin editors."""
    if not user.is_authenticated:
        return False

    if user.has_perm("meleager.manage_all_mlgr"):
        return True

    if user.has_perm("meleager.manage_own_mlgr") and instance.creator == user:
        return True

    return False


@register.filter
def can_delete(user, instance):
    """Only users creators of the given instance or staff."""
    return can_change(user, instance)
