class Alignments extends Stimulus.Controller {
    static get targets() {
        return ['alignment']
    }

    highlight(event) {
        const id = event.target.dataset.id
        const matchElements = this.alignmentTarget.querySelectorAll(`[data-id="${id}"]`)
        // In case there is no alignment, we only have one element matching
        // from the reference text. We do not want to highlight it.
        if (matchElements.length > 1) {
            Array.from(matchElements).forEach((element) => {
                if (!element.dataset.selected) {
                    element.classList.add('tag--warning')
                }
            })
        }
    }

    lowlight(event) {
        const id = event.target.dataset.id
        const matchElements = this.alignmentTarget.querySelectorAll(`[data-id="${id}"]`)
        Array.from(matchElements).forEach((element) => {
            element.classList.remove('tag--warning')
        })
    }
}
