class Modals extends Stimulus.Controller {
    static get targets() {
        return ['closebtn']
    }

    _triggerTurboFetch(hash) {
        const targetModal = document.querySelector(hash)
        targetModal.dataset.turboFetchValue = true
        const url = new URL(document.URL)
        targetModal.dataset.turboSearchValue = url.search
    }

    openOnHash(event) {
        const currentHash = document.location.hash
        if (
            currentHash === '#login' ||
            currentHash.endsWith('-create') ||
            currentHash.includes('-update-') ||
            currentHash.includes('-delete-')
        ) {
            this._triggerTurboFetch(currentHash)
        }
    }

    open(event) {
        const targetURL = new URL(event.currentTarget.href)
        this._triggerTurboFetch(targetURL.hash)
    }

    closeAll(event) {
        const ESCAPE = 27
        if (event.keyCode === ESCAPE) {
            this.closebtnTargets.forEach((closebtn) => closebtn.click())
        }
    }
}
