class Datetimes extends Stimulus.Controller {
    static get targets() {
        return ['datetime']
    }

    connect() {
        Array.from(this.datetimeTargets).forEach((element) => {
            let utcDatetime = element.getAttribute('datetime')
            const datetime = new Date(utcDatetime)
            element.textContent = `[${datetime.toLocaleString()}]`
        })
    }
}
