class Wikidata extends Stimulus.Controller {
    static get targets() {
        return ['urn', 'container', 'feedback', 'error', 'submit']
    }
    static values = { infoUrl: String }

    populate(event) {
        if (event.target.tagName === 'SELECT') return
        const wikiUrl = this.urnTarget.value.trim()
        if (!wikiUrl) {
            this.submitTarget.setAttribute('disabled', true)
        } else {
            this.submitTarget.removeAttribute('disabled')
            this._fetch(wikiUrl)
        }
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            const error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }

    _error() {
        this.submitTarget.setAttribute('disabled', true)
        this.containerTarget.setAttribute('hidden', true)
        this.errorTarget.removeAttribute('hidden')
    }

    _fetch(wikiUrl) {
        const url = new URL(this.infoUrlValue)
        url.search = new URLSearchParams({ url: wikiUrl }).toString()
        fetch(url)
            .then(this._checkStatus)
            .then((response) => response.json())
            .then((json) => {
                if (json && Object.keys(json).length === 0) {
                    this._error()
                    return
                }
                this.errorTarget.setAttribute('hidden', true)
                this.containerTarget.removeAttribute('hidden')
                this.feedbackTarget.innerHTML = Object.keys(json)
                    .map((lang) => `${json[lang]} (${lang})`)
                    .join(' ; ')
            })
            .catch((error) => {
                const e = new Error(`${error.message} ${url}`)
                Object.assign(e, error, { url })
                console.log(e)
                this._error()
            })
    }
}
