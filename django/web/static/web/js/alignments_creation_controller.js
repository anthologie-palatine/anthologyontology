class AlignmentsCreation extends Stimulus.Controller {
    static get targets() {
        return [
            'data',
            'passageText',
            'card',
            'left',
            'leftTerms',
            'rightTerms',
            'textPlaceholder-id_text_1',
            'textPlaceholder-id_text_2',
        ]
    }

    display(event) {
        const textId = event.target.value || this.leftTarget.value
        const selectedText = this.passageTextTargets.find(
            (passageText) => passageText.id == `passage-text-${textId}`
        )
        if (!selectedText) return

        const textPosition = event.target.name?.slice('text_'.length) || 1
        this[`textPlaceholder-id_text_${textPosition}Target`].innerHTML =
            selectedText.innerHTML
    }

    toggleSelect(event) {
        const element = event.target
        this.#toggleSelect(element)
    }

    #toggleSelect(element) {
        if (element.dataset.selected === 'true') {
            element.removeAttribute('data-selected')
            element.classList.remove('tag--black')
        } else {
            element.dataset.selected = true
            element.classList.remove('tag--warning')
            element.classList.add('tag--black')
        }
        this.#updateCard()
    }

    #updateCard() {
        const leftTerms = []
        const text1 = this['textPlaceholder-id_text_1Target']
        const text2 = this['textPlaceholder-id_text_2Target']
        for (const leftTerm of text1.querySelectorAll('[data-selected]')) {
            leftTerms.push(leftTerm.textContent)
        }
        this.leftTermsTarget.innerHTML = this.#joinTags(leftTerms)
        const rightTerms = []
        for (const rightTerm of text2.querySelectorAll('[data-selected]')) {
            rightTerms.push(rightTerm.textContent)
        }
        this.rightTermsTarget.innerHTML = this.#joinTags(rightTerms)
        if (leftTerms.length && rightTerms.length) {
            this.#showCard()
        } else {
            this.#hideCard()
        }
    }

    #joinTags(terms) {
        if (terms.length) {
            return `<span class="tag tag--alignment tag--warning">${terms.join(
                '</span>, <span class="tag tag--alignment tag--warning">'
            )}</span>`
        } else {
            return ''
        }
    }

    #generateAlignment(text, text1, text2) {
        const alignment = []
        const paragraphs = Array.from(text.querySelectorAll('p'))
        for (const [index, paragraph] of paragraphs.entries()) {
            let paragraphItems = []
            for (const child of paragraph.children) {
                if (child.tagName === 'BR') {
                    alignment.push(paragraphItems)
                    paragraphItems = []
                } else {
                    const span = child
                    if (span.dataset.counter !== '-1') {
                        let positions = []
                        if (span.hasAttribute('data-id')) {
                            const text1Ids = Array.from(
                                text1.querySelectorAll(`[data-id="${span.dataset.id}"]`)
                            ).map((item) => Number(item.dataset.counter))
                            const text2Ids = Array.from(
                                text2.querySelectorAll(`[data-id="${span.dataset.id}"]`)
                            ).map((item) => Number(item.dataset.counter))
                            positions = [text1Ids, text2Ids]
                        }
                        paragraphItems.push({
                            t: span.textContent,
                            h: positions,
                            pos: `[${span.dataset.passage}][${span.dataset.counter}]`,
                            parent: Number(span.dataset.passage),
                            children: Number(span.dataset.counter),
                        })
                    } else {
                        paragraphItems.push({
                            p: span.textContent,
                        })
                    }
                }
            }
            if (paragraphItems.length) {
                alignment.push(paragraphItems)
            }
            if (index < paragraphs.length - 1) {
                alignment.push([])
            }
        }

        return alignment
    }

    #resetIds(leftSelectedTerms, rightSelectedTerms) {
        // 1. Collect data-ids from data-selected items.
        const dataIds = []
        ;[].concat(leftSelectedTerms, rightSelectedTerms).forEach((selectedTerm) => {
            if (selectedTerm.dataset.id) {
                dataIds.push(selectedTerm.dataset.id)
            }
            selectedTerm.removeAttribute('data-id')
        })
        // 2. For each data-id, loop over text elements to reset their data-id.
        dataIds.forEach((dataId) => {
            const text1 = this['textPlaceholder-id_text_1Target']
            const text2 = this['textPlaceholder-id_text_2Target']
            const leftTerms = Array.from(
                text1.querySelectorAll(`[data-id="${dataId}"]`)
            )
            const rightTerms = Array.from(
                text2.querySelectorAll(`[data-id="${dataId}"]`)
            )
            ;[].concat(leftTerms, rightTerms).forEach((term) => {
                term.removeAttribute('data-id')
            })
        })
    }

    #updateData(text1, text2) {
        const text1Items = this.#generateAlignment(text1, text1, text2)
        const text2Items = this.#generateAlignment(text2, text1, text2)
        this.dataTarget.value = JSON.stringify([text1Items, text2Items])
    }

    align(event) {
        event.preventDefault()
        const text1 = this['textPlaceholder-id_text_1Target']
        const text2 = this['textPlaceholder-id_text_2Target']
        const leftSelectedTerms = Array.from(text1.querySelectorAll('[data-selected]'))
        const rightSelectedTerms = Array.from(text2.querySelectorAll('[data-selected]'))
        this.#resetIds(leftSelectedTerms, rightSelectedTerms)
        const ids = leftSelectedTerms.map(
            (leftSelectedTerm) => leftSelectedTerm.dataset.counter
        )
        const alignmentId = `align-[${ids.join(', ')}]`
        ;[].concat(leftSelectedTerms, rightSelectedTerms).forEach((selectedTerm) => {
            this.#toggleSelect(selectedTerm)
            selectedTerm.dataset.id = alignmentId
        })
        this.#updateData(text1, text2)
    }

    alignByKey(event) {
        if (event.code === 'KeyY') {
            this.align(event)
        }
    }
    cancel(event) {
        event.preventDefault()
        const text1 = this['textPlaceholder-id_text_1Target']
        const text2 = this['textPlaceholder-id_text_2Target']
        this.#hideCard()
        for (const leftTerm of text1.querySelectorAll('[data-selected]')) {
            this.#toggleSelect(leftTerm)
        }
        for (const rightTerm of text2.querySelectorAll('[data-selected]')) {
            this.#toggleSelect(rightTerm)
        }
    }

    cancelByKey(event) {
        if (event.code === 'KeyN') {
            this.cancel(event)
        }
    }

    #hideCard() {
        this.cardTarget.setAttribute('hidden', true)
        this.cardTarget.classList.remove('animated', 'fadeIn')
    }
    #showCard() {
        this.cardTarget.removeAttribute('hidden')
        this.cardTarget.classList.add('animated', 'fadeIn')
    }
}
