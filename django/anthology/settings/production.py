from . import *  # noqa

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

SECRET_KEY = "|WA82LPB9NOKQSk*XJu0^B(+bi^Uih?6vt><PE!*fXz3&mVkw=q*^^)X3Xz_<*!U5.X%mdT}^BZ;8(c~w%YNv5 k?3wQ HK/W+nW~E8R3}^meSO}@L:y?pa5vyA z+ha"

ALLOWED_HOSTS = [
    "prod-ap.ecrituresnumeriques.ca",
    "anthologiagraeca.org",
]

sentry_sdk.init(
    dsn="https://7be080984d5b415b88b2628181dbac2a@o1222326.ingest.sentry.io/6366129",
    integrations=[DjangoIntegration()],
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=0.1,
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,
    environment="production",
)
