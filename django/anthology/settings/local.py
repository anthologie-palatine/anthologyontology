from . import *  # noqa

DEBUG = True

DATABASES["default"]["HOST"] = "localhost"
ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
INTERNAL_IPS = ["127.0.0.1", "192.168.0.0"]
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"

# Debug toolbar configuration:
INSTALLED_APPS.append("debug_toolbar")
MIDDLEWARE.insert(0, "debug_toolbar.middleware.DebugToolbarMiddleware")
ROOT_URLCONF = "anthology.urls_local"
