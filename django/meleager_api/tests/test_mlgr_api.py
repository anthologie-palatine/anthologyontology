from django.test import TestCase


class TestGraphQL(TestCase):
    def test_graphql(self):
        resp = self.client.get("/graphql?query=%7B%0A%09passages%20%7B%0A%20%20%20%20book%20%7B%20number%20%7D%0A%20%20%20%20fragment%0A%20%20%20%20subFragment%0A%20%20%7D%0A%7D%0A")
        self.assertEqual(resp.status_code, 200)

class TestAPI(TestCase):
    fixtures = ["test_data.json"]

    def test_api_ok(self):
        resp = self.client.get("/api/")
        self.assertEqual(resp.status_code, 200)

    def test_api_passages_ok(self):
        resp = self.client.get("/api/passages/")
        self.assertEqual(resp.status_code, 200)

    def test_one_passage_ok(self):
        resp = self.client.get(
            "/api/passages/urn:cts:greekLit:tlg7000.tlg001.ag:42.12/"
        )
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        assert type(data["authors"]) is list
