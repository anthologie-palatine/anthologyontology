from rest_framework import viewsets

from meleager.models import Edition
from ..serializers.edition import EditionSerializer
from ..pagination import ApiPagination


class EditionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Edition.objects.all()
    serializer_class = EditionSerializer
    pagination_class = ApiPagination
