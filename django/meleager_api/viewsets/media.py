from rest_framework import viewsets

from meleager.models import Manuscript, Medium
from ..serializers.media import ManuscriptSerializer, MediumSerializer
from ..pagination import ApiPagination


class MediaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Medium.objects.all()
    serializer_class = MediumSerializer
    pagination_class = ApiPagination


class ManuscriptViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Manuscript.objects.all()
    serializer_class = ManuscriptSerializer
    pagination_class = ApiPagination
