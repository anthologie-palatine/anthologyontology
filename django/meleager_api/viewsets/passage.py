import re

from rest_framework import viewsets
from rest_framework.response import Response
import django_filters.rest_framework
from django.db.models import Prefetch

from meleager.models import Passage, Text, Name, Author, Keyword, Book
from ..serializers.passage import PassageSerializer, PassageDetailSerializer
from ..pagination import ApiPagination


class PassageFilter(django_filters.FilterSet):
    book__number = django_filters.ModelChoiceFilter(
        to_field_name='number',
        queryset=Book.objects.all(),
        method='filter_by_book',
        label='Book number'
    )

    author__main_name = django_filters.ModelChoiceFilter(
        field_name='main_name',
        to_field_name='main_name',
        queryset=Author.objects.filter(main_name__isnull=False).values_list('main_name', flat=True).distinct(),
        # queryset=Author.objects.all(),
        method='filter_by_author',
        label='Author Name'
    )

    keyword__number = django_filters.ModelChoiceFilter(
        to_field_name='pk',
        queryset=Keyword.objects.all().values_list('pk', flat=True),
        method='filter_by_keyword',
        label='Keyword PrimaryKey'
    )

    def filter_by_author(self, queryset, name, value):
        author_pks = Author.objects.filter(main_name=value).values_list('pk', flat=True)
        return queryset.filter(authors__in = author_pks)
        # return queryset.filter(authors = value)


    def filter_by_book(self, queryset, name, value):
        return queryset.filter(book = value)

    def filter_by_keyword(self, queryset, name, value):
        return queryset.filter(keywords = value)


class PassageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Passage.objects.all() \
        .select_related('book') \
        .prefetch_related(
            Prefetch('keywords', queryset=Keyword.objects.select_related('category')), \
            Prefetch('texts', queryset=Text.objects.select_related('language')), \
            Prefetch('authors', queryset=Author.objects.prefetch_related(Prefetch('names', queryset=Name.objects.select_related('language')))) \
        ) \
        .order_by('book', 'fragment', "sub_fragment")

    serializer_class = PassageDetailSerializer
    serializer_action_classes = {
        'list': PassageSerializer
    }
    filterset_class = PassageFilter
    pagination_class = ApiPagination

    def retrieve(self, request, **kwargs):
        pk = self.get_queryset().get_pk_from_urn('.'.join(kwargs.values()))
        self.kwargs['pk'] = pk
        return super().retrieve(self, request, pk=pk)

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
