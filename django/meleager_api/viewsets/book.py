from rest_framework import viewsets

from meleager.models import Book
from ..serializers.book import BookSerializer
from ..pagination import ApiPagination


class BookViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    pagination_class = ApiPagination
