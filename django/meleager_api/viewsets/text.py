from rest_framework import viewsets

from meleager.models import Text
from ..serializers.text import TextSerializer
from ..pagination import ApiPagination


class TextViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Text.objects.all().prefetch_related("meleager_alignment_text_1", "meleager_alignment_text_2", "passages")
    serializer_class = TextSerializer
    pagination_class = ApiPagination
