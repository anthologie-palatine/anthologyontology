from rest_framework import viewsets

from meleager.models import Language
from ..serializers.language import FullLanguageSerializer
from ..pagination import ApiPagination


class LanguageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Language.objects.all()
    serializer_class = FullLanguageSerializer
    pagination_class = ApiPagination
