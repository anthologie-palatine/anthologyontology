from rest_framework import viewsets

from meleager.models import Keyword, KeywordCategory
from ..serializers.keyword import KeywordCategorySerializer, KeywordSerializer
from ..pagination import ApiPagination


class KeywordViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Keyword.objects.all().prefetch_related("passages", "names", "passages__book").select_related("category")
    serializer_class = KeywordSerializer
    pagination_class = ApiPagination


class KeywordCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = KeywordCategory.objects.all().prefetch_related("names")
    serializer_class = KeywordCategorySerializer
    pagination_class = ApiPagination
