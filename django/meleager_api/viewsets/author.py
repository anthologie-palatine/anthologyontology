from rest_framework import viewsets
import django_filters.rest_framework

from meleager.models import Author
from ..serializers.author import AuthorSerializer
from ..pagination import ApiPagination


class AuthorFilter(django_filters.FilterSet):

    main_name = django_filters.ModelChoiceFilter(
        field_name='main_name',
        to_field_name='main_name',
        queryset=Author.objects.filter(main_name__isnull=False).exclude(main_name="").distinct(),
        label='Author Name'
    )


class AuthorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Author.objects.prefetch_related(
        "names", "names__language", "passages", "passages__book"
    )
    serializer_class = AuthorSerializer
    pagination_class = ApiPagination
    filterset_class = AuthorFilter
