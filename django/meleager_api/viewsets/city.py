from django_filters import rest_framework as filters
from django_filters.widgets import BooleanWidget
from rest_framework import viewsets

from meleager.models import City
from ..serializers.city import CitySerializer
from ..pagination import ApiPagination


class CityFilter(filters.FilterSet):

    latitude = filters.BooleanFilter(
        widget=BooleanWidget(), lookup_expr="isnull", exclude=True
    )
    longitude = filters.BooleanFilter(
        widget=BooleanWidget(), lookup_expr="isnull", exclude=True
    )


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = ApiPagination
    filterset_class = CityFilter
