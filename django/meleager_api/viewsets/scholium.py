from rest_framework import viewsets
from django.db.models import Prefetch

from ..serializers.scholium import ScholiumSerializer, ScholiumDetailSerializer
from meleager.models import Passage, Scholium, Text, Keyword
from ..pagination import ApiPagination


class ScholiumViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Scholium.objects.all() \
        .prefetch_related(
            Prefetch('keywords', queryset=Keyword.objects.select_related('category')), \
            Prefetch('texts', queryset=Text.objects.select_related('language')), \
        ) \
        .order_by('passage__book', 'passage__fragment', "passage__sub_fragment")

    serializer_class = ScholiumDetailSerializer
    serializer_action_classes = {
        'list': ScholiumSerializer
    }
    pagination_class = ApiPagination

    def retrieve(self, request, **kwargs):
        pk = self.get_queryset().get_pk_from_urn('.'.join(kwargs.values()))
        self.kwargs['pk'] = pk
        return super().retrieve(self, request, pk=pk)

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()
