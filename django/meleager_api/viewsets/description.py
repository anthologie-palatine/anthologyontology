from rest_framework import viewsets

from meleager.models import Description
from ..serializers.description import DescriptionSerializer
from ..pagination import ApiPagination


class DescriptionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Description.objects.all()
    serializer_class = DescriptionSerializer
    pagination_class = ApiPagination
