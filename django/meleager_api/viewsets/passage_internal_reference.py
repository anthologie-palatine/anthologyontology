from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

from meleager.models import PassageInternalReference
from ..serializers.passage_internal_reference import PassageInternalReferenceSerializer
from ..pagination import ApiPagination


class PassageInternalReferenceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = PassageInternalReference.objects.all()
    serializer_class = PassageInternalReferenceSerializer
    pagination_class = ApiPagination
    filterset_backends = [DjangoFilterBackend]
    filterset_fields = ['reference_type']