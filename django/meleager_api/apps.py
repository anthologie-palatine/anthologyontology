from django.apps import AppConfig


class MeleagerApiConfig(AppConfig):
    name = "meleager_api"
