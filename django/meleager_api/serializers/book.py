from rest_framework import serializers

from meleager.models import Book


class BookSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Book
        fields = [
            "url", "number",
        ]
