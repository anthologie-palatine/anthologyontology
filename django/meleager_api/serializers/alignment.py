from rest_framework import serializers

from meleager.models import Alignment
from .passage import PassageHyperlink, TextRelatedField


class AlignmentSerializer(serializers.HyperlinkedModelSerializer):

    passage = serializers.SerializerMethodField('passage_url')
    text_1 = TextRelatedField()
    text_2 = TextRelatedField()

    def passage_url(self, alignment):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"

        if alignment.text_1:
            passage = alignment.text_1.passages.first()
            passage_url = PassageHyperlink(view_name=view_name).get_url(
                obj=passage, view_name=view_name, request=request, format=format
            )
            return passage_url
        else:
            return ""

    class Meta:
        model = Alignment
        fields = [
            "url",
            "passage",
            "text_1",
            "text_2",
            "alignment_data"
        ]
