from rest_framework import serializers

from meleager.models import Description
from .language import LanguageSerializer


class DescriptionSerializer(serializers.HyperlinkedModelSerializer):
    language = LanguageSerializer()

    class Meta:
        model = Description
        fields = [
            "url",
            "language",
            "created_at",
            "updated_at",
            "description",
        ]
