from rest_framework import serializers
from rest_framework.reverse import reverse

from meleager.models import Scholium, Passage, City, Keyword, Comment
from .passage import (PassageHyperlink, ScholiumHyperlink,
                      ManuscriptsRelatedField, TextRelatedField,
                      CityRelatedField, CityDetailRelatedField,
                      KeywordRelatedField, KeywordDetailRelatedField,
                      CommentRelatedField, CommentDetailRelatedField,
                      ExternalReferenceRelatedField, MediumRelatedField)


class PassageRelatedField(serializers.StringRelatedField):

    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"

        passage = {
            'book' : value.book.number,
            'fragment' : value.fragment,
            'sub_fragment' : value.sub_fragment,
            'url': PassageHyperlink(view_name=view_name).get_url(
                obj=value, view_name=view_name, request=request, format=format
            ),
        }
        return passage


class ScholiumSerializer(serializers.HyperlinkedModelSerializer):

    web_url = serializers.URLField(
        source='get_absolute_url',
        label='URL sur Anthologia-Graeca')
    url = ScholiumHyperlink(
        view_name = "scholium-detail"
    )
    urn = serializers.URLField(
        source='urn_value')
    manuscripts = ManuscriptsRelatedField(
        many=True,
        )
    texts = TextRelatedField(
        many=True,
        )
    cities = CityRelatedField(
        many=True,
        )
    keywords = KeywordRelatedField(
        many=True,
        )
    passage = PassageRelatedField()
    comments = CommentRelatedField(
        many=True,
        )
    media = MediumRelatedField(
        many=True,
        source='images'
        )

    class Meta:
        model = Scholium
        fields = [
            'id', 'url', 'urn', 'web_url',
            'manuscripts', 'texts', 'cities',
            'keywords', 'passage', 'comments',
            'media',
        ]


class ScholiumDetailSerializer(serializers.HyperlinkedModelSerializer):

    web_url = serializers.URLField(
        source='get_absolute_url',
        label='URL sur Anthologia-Graeca')
    url = ScholiumHyperlink(
        view_name = "scholium-detail"
    )
    urn = serializers.URLField(
        source='urn_value')
    manuscripts = ManuscriptsRelatedField(
        many=True,
        )
    texts = TextRelatedField(
        many=True,
        )
    cities = CityDetailRelatedField(
        many=True,
        )
    keywords = KeywordDetailRelatedField(
        many=True,
        )
    passage = PassageRelatedField()
    comments = CommentDetailRelatedField(
        many=True,
        )
    media = MediumRelatedField(
        many=True,
        source='images'
        )

    class Meta:
        model = Scholium
        fields = [
            'id', 'url', 'urn', 'web_url',
            'manuscripts', 'texts', 'cities',
            'keywords', 'passage', 'comments',
            'media',
        ]
