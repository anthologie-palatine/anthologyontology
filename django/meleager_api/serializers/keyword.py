from rest_framework import serializers

from meleager.models import Keyword, KeywordCategory
from .name import MinimalLinkedNameSerializer
from .passage import PassageHyperlink


class KeywordCategorySerializer(serializers.HyperlinkedModelSerializer):
    names = MinimalLinkedNameSerializer(many=True)
    url = serializers.HyperlinkedIdentityField(view_name="keyword_cat-detail")

    class Meta:
        model = KeywordCategory
        fields = ["url", "names"]


class UrnRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        alternative_urns = {
            "urn": value.urn,
        }
        return alternative_urns


class KeywordSerializer(serializers.HyperlinkedModelSerializer):
    category = KeywordCategorySerializer()
    names = MinimalLinkedNameSerializer(many=True)
    alternative_urns = UrnRelatedField(
        many=True,
    )
    passages = PassageHyperlink(many=True, read_only=True, view_name="passage-detail")

    class Meta:
        model = Keyword
        fields = ["id", "url", "category", "names", "alternative_urns", "passages"]


class MinimalLinkedKeywordSerializer(serializers.ModelSerializer):
    names = MinimalLinkedNameSerializer(many=True)

    class Meta:
        model = Keyword
        fields = ("names", "url")
