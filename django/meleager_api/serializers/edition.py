from rest_framework import serializers

from meleager.models import Edition
from .description import DescriptionSerializer


class EditionSerializer(serializers.HyperlinkedModelSerializer):
    descriptions = DescriptionSerializer(many=True)

    class Meta:
        model = Edition
        fields = [
            "url",
            "descriptions",
            "edition_type",
            "metadata",
            "created_at",
            "updated_at",
        ]
