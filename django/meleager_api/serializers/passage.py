from rest_framework import serializers

from meleager.models import (
    Passage,
    City,
    Keyword,
    KeywordCategory,
    Comment,
    PassageInternalReference,
    Alignment,
)
from .book import BookSerializer


class ScholiumHyperlink(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            "book": obj.passage.book.number,
            "fragment": obj.passage.fragment,
            "sub_fragment": obj.passage.sub_fragment,
            "number": obj.number,
        }
        return self.reverse(
            view_name, kwargs=url_kwargs, request=request, format=format
        )


class PassageHyperlink(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            "book": obj.book.number,
            "fragment": obj.fragment,
            "sub_fragment": obj.sub_fragment,
        }
        return self.reverse(
            view_name, kwargs=url_kwargs, request=request, format=format
        )


class ModelHyperlink(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):

        return self.reverse(view_name, args=[obj], request=request, format=format)


class ManuscriptsRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        return value.url


class TextRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "text-detail"

        texts = {
            "url": ModelHyperlink(view_name=view_name).get_url(
                obj=value.pk, view_name=view_name, request=request, format=format
            ),
            "language": value.language.code,
            "text": value.text,
        }
        return texts


class AuthorRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "author-detail"

        authors = {
            "url": ModelHyperlink(view_name=view_name).get_url(
                obj=value.pk, view_name=view_name, request=request, format=format
            ),
            "tlg_id": value.tlg_id,
            "names": value.names.all().values("name", "language"),
        }
        return authors


class AlignmentRelatedField(serializers.HyperlinkedRelatedField):
    view_name = "alignment-detail"
    queryset = Alignment.objects.all()


class CityRelatedField(serializers.HyperlinkedRelatedField):
    view_name = "city-detail"
    queryset = City.objects.all()


class CityDetailRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "city-detail"

        cities = {
            "url": CityRelatedField(view_name=view_name).get_url(
                obj=value, view_name=view_name, request=request, format=format
            ),
            "names": value.names.all().values("name", "language"),
        }
        return cities


class KeywordCategoryRelatedField(serializers.HyperlinkedRelatedField):
    view_name = "keyword_cat-detail"
    queryset = KeywordCategory.objects.all()


class KeywordRelatedField(serializers.HyperlinkedRelatedField):
    view_name = "keyword-detail"
    queryset = Keyword.objects.all()


class KeywordDetailRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "keyword-detail"
        view_name_category = "keyword_cat-detail"

        keywords = {
            "url": KeywordRelatedField(view_name=view_name).get_url(
                obj=value, view_name=view_name, request=request, format=format
            ),
            "names": value.names.all().values("name", "language"),
            "category": {
                "url": KeywordCategoryRelatedField(
                    view_name=view_name_category
                ).get_url(
                    obj=value.category,
                    view_name=view_name_category,
                    request=request,
                    format=format,
                ),
                "names": value.category.names.all().values("name", "language"),
            },
        }
        return keywords


class ScholieRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "scholium-detail"

        scholia = {
            "book": value.passage.book.number,
            "fragment": value.passage.fragment,
            "sub_fragment": value.passage.sub_fragment,
            "number": value.number,
            "url": ScholiumHyperlink(view_name=view_name).get_url(
                obj=value, view_name=view_name, request=request, format=format
            ),
        }
        return scholia


class CommentRelatedField(serializers.HyperlinkedRelatedField):
    view_name = "comment-detail"
    queryset = Comment.objects.all()


class CommentDetailRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        comments = {
            "description": value.descriptions.all().values_list(
                "description", flat=True
            ),
            "language": value.descriptions.all().values_list("language", flat=True),
        }
        return comments


class ExternalReferenceRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        external_references = {"title": value.title, "url": value.url}
        return external_references


class MediumRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        media = {"type": value.type, "url": value.url}
        return media


class InternalReferenceRelatedField(serializers.StringRelatedField):
    def get_attribute(self, value):
        return value

    def to_representation(self, value):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"

        try:
            if isinstance(self.root, serializers.ListSerializer):
                internal_reference = {
                    "to_passage": PassageHyperlink(view_name=view_name).get_url(
                        obj=value, view_name=view_name, request=request, format=format
                    ),
                    "reference_type": PassageInternalReference.objects.get(
                        from_passage=self.root.child._instance, to_passage=value
                    ).get_reference_type_display(),
                }
            else:
                internal_reference = {
                    "to_passage": PassageHyperlink(view_name=view_name).get_url(
                        obj=value, view_name=view_name, request=request, format=format
                    ),
                    "reference_type": PassageInternalReference.objects.get(
                        from_passage=value, to_passage=self.root.instance
                    ).get_reference_type_display(),
                }
        except:
            internal_reference = None
        return internal_reference


class PassageSerializer(serializers.HyperlinkedModelSerializer):
    def to_representation(self, instance):
        # When we call Results with many=True, the serializer.instance is a list with several records,
        # we can't know which particular instance is spawning the nested serializers so we add it here.
        self._instance = instance
        return super(serializers.HyperlinkedModelSerializer, self).to_representation(
            instance
        )

    book = BookSerializer()
    url = PassageHyperlink(view_name="passage-detail")
    manuscripts = ManuscriptsRelatedField(
        many=True,
    )
    texts = TextRelatedField(
        many=True,
    )
    authors = AuthorRelatedField(
        many=True,
    )
    cities = CityRelatedField(
        many=True,
    )
    keywords = KeywordRelatedField(
        many=True,
    )
    scholia = ScholieRelatedField(
        many=True,
    )
    comments = CommentRelatedField(
        many=True,
    )
    external_references = ExternalReferenceRelatedField(
        many=True,
    )
    internal_references = InternalReferenceRelatedField(
        many=True,
    )
    media = MediumRelatedField(many=True, source="images")

    class Meta:
        model = Passage
        fields = [
            "id",
            "book",
            "fragment",
            "sub_fragment",
            "url",
            "manuscripts",
            "texts",
            "authors",
            "cities",
            "keywords",
            "scholia",
            "comments",
            "external_references",
            "internal_references",
            "media",
        ]


class PassageDetailSerializer(serializers.HyperlinkedModelSerializer):
    alignments = serializers.SerializerMethodField("alignment_url")

    def alignment_url(self, passage):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "alignment-detail"

        alignment_urls = []
        if passage.texts:
            for text in passage.texts.all():
                alignments = text.meleager_alignment_text_1.all()
                for alignment in alignments:
                    alignment_url = AlignmentRelatedField(view_name=view_name).get_url(
                        obj=alignment,
                        view_name=view_name,
                        request=request,
                        format=format,
                    )
                    alignment_urls.append(alignment_url)
            return set(alignment_urls)
        else:
            return ""

    book = BookSerializer()
    web_url = serializers.URLField(
        source="get_absolute_url", label="URL sur Anthologia-Graeca"
    )
    url = PassageHyperlink(view_name="passage-detail")
    urn = serializers.URLField(source="urn_value")
    manuscripts = ManuscriptsRelatedField(
        many=True,
    )
    texts = TextRelatedField(
        many=True,
    )
    authors = AuthorRelatedField(
        many=True,
    )
    cities = CityDetailRelatedField(
        many=True,
    )
    keywords = KeywordDetailRelatedField(
        many=True,
    )
    scholia = ScholieRelatedField(
        many=True,
    )
    comments = CommentDetailRelatedField(
        many=True,
    )
    external_references = ExternalReferenceRelatedField(
        many=True,
    )
    internal_references = InternalReferenceRelatedField(
        many=True,
    )
    media = MediumRelatedField(many=True, source="images")

    class Meta:
        model = Passage
        fields = [
            "id",
            "book",
            "fragment",
            "sub_fragment",
            "urn",
            "url",
            "web_url",
            "manuscripts",
            "texts",
            "authors",
            "cities",
            "keywords",
            "scholia",
            "comments",
            "external_references",
            "internal_references",
            "media",
            "alignments",
        ]
