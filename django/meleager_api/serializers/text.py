from rest_framework import serializers
from itertools import chain

from meleager.models import Text
from .language import LanguageSerializer
from .edition import EditionSerializer
from .passage import AlignmentRelatedField, PassageHyperlink


class TextSerializer(serializers.HyperlinkedModelSerializer):
    language = LanguageSerializer()
    edition = EditionSerializer()
    alignments = serializers.SerializerMethodField('alignments_url')
    passages = serializers.SerializerMethodField('passages_url')

    def alignments_url(self, text):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "alignment-detail"
        alignments = []

        for alignment in chain(text.meleager_alignment_text_1.all(), text.meleager_alignment_text_2.all()):
            alignment_url = AlignmentRelatedField(view_name=view_name).get_url(
                obj=alignment, view_name=view_name, request=request, format=format
            )
            alignments.append(alignment_url)
        return alignments

    def passages_url(self, text):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"
        passages = []

        for passage in text.passages.all():
            passage_url = PassageHyperlink(view_name=view_name).get_url(
                obj=passage, view_name=view_name, request=request, format=format
            )
            passages.append(passage_url)
        return passages


    class Meta:
        model = Text
        fields = [
            "url",
            "language",
            "edition",
            "unique_id",
            "created_at",
            "updated_at",
            "validation",
            "status",
            "text",
            "comments",
            "alignments",
            "passages",
        ]
