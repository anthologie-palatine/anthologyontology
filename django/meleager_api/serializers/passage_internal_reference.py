from rest_framework import serializers

from meleager.models import PassageInternalReference
from .passage import PassageHyperlink


class PassageInternalReferenceSerializer(serializers.HyperlinkedModelSerializer):

    to_passage = serializers.SerializerMethodField('to_passage_url')
    from_passage = serializers.SerializerMethodField('from_passage_url')


    def to_passage_url(self, passage_internal_reference):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"

        to_passage = passage_internal_reference.to_passage
        to_passage_url = PassageHyperlink(view_name=view_name).get_url(
            obj=to_passage, view_name=view_name, request=request, format=format
        )
        return to_passage_url

    def from_passage_url(self, passage_internal_reference):
        request = self.context["request"]
        format = self.context.get("format")
        view_name = "passage-detail"

        from_passage = passage_internal_reference.from_passage
        from_passage_url = PassageHyperlink(view_name=view_name).get_url(
            obj=from_passage, view_name=view_name, request=request, format=format
        )
        return from_passage_url

    class Meta:
        model = PassageInternalReference
        fields = [
            "from_passage",
            "to_passage",
            "reference_type",
        ]
