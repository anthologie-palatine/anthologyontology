from rest_framework import serializers

from meleager.models import Manuscript, Medium


class MediumSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Medium
        fields = [
            "url",
            "unique_id",
            "created_at",
            "updated_at",
            "title",
            "type",
            "descriptions",
            "keywords",
        ]


class ManuscriptSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Manuscript
        fields = [
            "url",
            "created_at",
            "updated_at",
            "title",
            "credit",
            "descriptions",
            "keywords"
        ]
