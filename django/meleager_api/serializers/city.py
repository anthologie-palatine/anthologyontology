from rest_framework import serializers

from meleager.models import City
from .name import MinimalLinkedNameSerializer
from .passage import PassageHyperlink


class UrnRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        alternative_urns = value.urn
        return alternative_urns


class CitySerializer(serializers.HyperlinkedModelSerializer):
    names = MinimalLinkedNameSerializer(many=True)
    alternative_urns = UrnRelatedField(
        many=True,
    )
    passages = PassageHyperlink(many=True, read_only=True, view_name="passage-detail")

    class Meta:
        model = City
        fields = [
            "url",
            "names",
            "alternative_urns",
            "unique_id",
            "longitude",
            "latitude",
            "descriptions",
            "created_at",
            "updated_at",
            "passages",
        ]
