from rest_framework import serializers
from .passage import PassageHyperlink

from meleager.models import Comment


class DescriptionRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        description = {
            'content' : value.description,
            'language': value.language.code,
        }

        return description
  

class CommentSerializer(serializers.HyperlinkedModelSerializer):
    passages = PassageHyperlink(
        many=True, read_only=True, view_name="passage-detail"
    )
    descriptions = DescriptionRelatedField(
        many=True
    )
    
    class Meta:
        model = Comment
        fields = [
            "url",
            "passages",
            "descriptions",
            "unique_id",
            "created_at",
            "updated_at",
            "comment_type",
            "images",
        ]
