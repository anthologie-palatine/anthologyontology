from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.urls import reverse


class CustomUserManager(UserManager):
    def get_by_natural_key(self, username):
        return self.get(username=username)


class User(AbstractUser):
    institution = models.TextField(help_text="The institution this user belongs to.")
    objects = CustomUserManager()

    def natural_key(self):
        return (self.username,)

    def get_absolute_url(self):
        return reverse('user-history', args=[self.pk])
