from django.urls import path
from .views import (
    LoginView,
    LogoutView,
    PasswordResetView,
    RegisterView,
    RegisterSuccessView
)

urlpatterns = [
    path('login/', LoginView.as_view(), name="login"),
    path('register/', RegisterView.as_view(), name="register"),
    path('register-success/', RegisterSuccessView.as_view(), name="register-success"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('reset/', PasswordResetView.as_view(), name="password_reset"),
]
