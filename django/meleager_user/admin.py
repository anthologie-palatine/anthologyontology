from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User


class CustomUserAdmin(UserAdmin):
    list_display = list(UserAdmin.list_display) + ["date_joined"]
    ordering = ("-date_joined",)

    fieldsets = list(UserAdmin.fieldsets) + [("Extra", {"fields":("institution",)})]


admin.site.register(User, CustomUserAdmin)
