from django.contrib.syndication.views import Feed
from django.urls import reverse

from .models import User


class LatestUsersFeed(Feed):
    title = "[Anthology] Latest created users"
    link = "/admin/"
    description = "Latest created Users to validate by an admin."

    def items(self):
        return User.objects.order_by("-date_joined")[:50]

    def item_title(self, item):
        return f"{item.pk} New anthology user!"

    item_description = "You have to validate the user in the admin."

    def item_link(self, item):
        return reverse("admin:meleager_user_user_change", args=[item.pk])

    def item_pubdate(self, item):
        return item.date_joined
