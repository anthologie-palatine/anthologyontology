from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _

from django import forms
from .models import User


class RegisterForm(UserCreationForm):

    institution = forms.CharField(
        max_length=200,
        required=False,
        label=_("Institution")
    )
    email = forms.EmailField(
        label=_("Email adress"),
        required=True,
    )

    class Meta:
        model = User
        fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "password1",
            "password2",
            "institution",
        ]

    def clean_email(self):
        email = self.cleaned_data["email"]
        return email.lower()
