from django.test import TestCase
from django.urls import reverse
from meleager_user.models import User


class CommentsForms(TestCase):
    fixtures = ["test_data.json"]

    @classmethod
    def setUpTestData(cls):
        pass

    def test_login_form_post(self):
        response = self.client.post(
            reverse("login"),
            data={
                "username": "superuser",
                "password": "superuser",
            },
            follow=True,
        )

        self.assertTrue(response.context['user'].is_authenticated)
