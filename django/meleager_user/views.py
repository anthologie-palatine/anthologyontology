from django.views.generic import DetailView, CreateView, TemplateView
from django.views.generic.list import MultipleObjectMixin
from django.contrib.auth.views import LoginView as DLoginView
from django.contrib.auth.views import LogoutView as DLogoutView
from django.contrib.auth.views import PasswordResetView as DPwdResetView
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.core.paginator import Paginator

from reversion.models import Version
from .user_forms import RegisterForm


class RegisterView(CreateView):
    template_name = "registration/register.html"
    form_class = RegisterForm
    success_url = reverse_lazy("register-success")

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        response = super().form_valid(form)
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class RegisterSuccessView(TemplateView):
    template_name = "registration/register_success.html"


class LoginView(DLoginView):
    template_name = "registration/login.html"


class LogoutView(DLogoutView):
    pass


class PasswordResetView(DPwdResetView):
    template_name = "registration/password_reset.html"


class UserVersionsMixin:
    def get_versions_for(self, user):
        return Version.objects.filter(revision__user=user.pk).filter(
            Q(
                content_type=ContentType.objects.get(
                    app_label="meleager", model="passage"
                )
            )
            | Q(
                content_type=ContentType.objects.get(
                    app_label="meleager", model="scholium"
                )
            )
            | Q(
                content_type=ContentType.objects.get(
                    app_label="meleager", model="keyword"
                )
            )
            | Q(
                content_type=ContentType.objects.get(app_label="meleager", model="city")
            )
        )


class ProfileView(UserVersionsMixin, DetailView):
    template_name = "user/profile.html"
    model = get_user_model()

    def get_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context["page_title"] = _("Profile of %(user)s") % {"user": user}
        context["url_to_all_modifications"] = reverse("user-history", args=(user.pk,))
        context["modifications_without_user"] = True
        context["versions"] = self.get_versions_for(user)[:5]
        return context


class UserHistoryView(UserVersionsMixin, DetailView, MultipleObjectMixin):
    template_name = "web/history/list.html"
    context_object_name = "user_history"
    model = get_user_model()
    paginate_by = 20
    paginator_class = Paginator

    def get_context_data(self, **kwargs):
        user_history = self.get_object()
        object_list = self.get_versions_for(user_history)

        context = super().get_context_data(object_list=object_list, **kwargs)
        context["page_title"] = _("Modifications by user %(user_history)s") % {
            "user_history": user_history
        }
        context["modifications_without_user"] = True
        # The object_list *from the context* is now paginated.
        context["versions"] = context["object_list"]
        return context
