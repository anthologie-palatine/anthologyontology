import re

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from meleager.models import Language
from meleager_import.perseus.helpers import get_perseus_epigrams
from meleager_import.perseus.importer import import_epigram


class Command(BaseCommand):
    help = "Imports Perseus data into Django DB - v2"

    def handle(self, *args, **options):
        call_command("migrate")
        if Language.objects.count() <= 4:
            call_command("loaddata", "languages")

        print("=== Import Perseus")
        perseus = get_perseus_epigrams(display=False)

        for epigram in perseus:
            import_epigram(epigram)