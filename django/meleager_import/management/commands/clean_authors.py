from django.core.management.base import BaseCommand, CommandError
from django.db.models.aggregates import Count
from meleager.models import Author


class Command(BaseCommand):
    help = "Cleans potential duplicate authors (tlg_id)"

    def handle(self, *args, **options):
        # Quick fix for issue #184
        duplicate_tlgs = Author.objects.values('tlg_id').annotate(num=Count('tlg_id')).filter(num__gt=1)

        if duplicate_tlgs.count():
            self.stdout.write("+++ Found the following duplicated TLGs:")

        for tlg in duplicate_tlgs:
            self.stdout.write(f"--> {tlg['tlg_id']}")
            author_to_keep, *others = Author.objects.filter(tlg_id=tlg["tlg_id"])
            passages_in_kept = author_to_keep.passages.values_list('pk', flat=True)

            for author_to_remove in others:
                # Get the names of the author to remove
                names = author_to_remove.names.all()

                self.stdout.write(f"Adding names {names} to {author_to_keep}")
                # Add them to the author to keep
                author_to_keep.names.add(*names)

                passages_not_in_kept = author_to_remove.passages.exclude(pk__in=passages_in_kept)
                self.stdout.write(f"... Found passages {passages_not_in_kept}")
                author_to_keep.passages.add(*passages_not_in_kept)
                self.stdout.write(f"--- Now deleting author: {author_to_remove}")
                author_to_remove.delete()