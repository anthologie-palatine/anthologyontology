from django.apps import AppConfig


class MeleagerImportConfig(AppConfig):
    name = 'meleager_import'
