import datetime
import time

import pytz


def to_date(s):
    if isinstance(s, str):
        return datetime.datetime(
            *(time.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ")[0:6]), tzinfo=pytz.utc
        )
    else:
        return s
