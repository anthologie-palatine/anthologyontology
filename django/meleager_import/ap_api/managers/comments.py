import logging

from meleager.models import Comment, Description

from ..constants import API_URL, LANG_API2CODE
from ..helpers import to_date
from .data_manager import DataManager
from .users import UserFromApi

logger = logging.getLogger(__name__)


class CommentFromApi:
    _notes = dict()

    def __new__(cls, id_note):
        if id_note not in cls._notes:
            action = "NEW"
            note_obj = cls._create_note(id_note)
            cls._notes[id_note] = note_obj
        else:
            action = "OK"
            note_obj = cls._notes[id_note]

        logger.info("%s | Note PK %s (AP %s)", action, note_obj.pk, id_note)
        return note_obj

    @classmethod
    def _create_note(cls, id_note):
        json_data = DataManager.get(f"{API_URL}/v1/notes/{id_note}")

        descriptions = list()

        for version in json_data["versions"]:
            user_obj = UserFromApi(version["id_user"])
            markdown_desc = "# {}\n\n {}".format(json_data["title"], version["text"])
            desc_obj = Description.objects.create(
                description=markdown_desc,
                language_id=LANG_API2CODE[version["id_language"]],
                creator=user_obj,
                last_editor=user_obj,
                created_at=to_date(version["createdAt"]),
                updated_at=to_date(version["updatedAt"]),
            )
            descriptions.append(desc_obj)

        user_obj = UserFromApi(json_data["id_user"]["id_user"])
        note_obj = Comment.objects.create(
            unique_id=id_note,
            creator=user_obj,
            last_editor=user_obj,
            created_at=to_date(json_data["createdAt"]),
            updated_at=to_date(json_data["updatedAt"]),
        )

        note_obj.descriptions.add(*descriptions)

        return note_obj

    @classmethod
    def _create_all(cls):
        json_data = DataManager.get(f"{API_URL}/v1/notes/")
        for note in json_data:
            cls._create_note(note["id_note"])
