from meleager.models import Description, Work


class WorkFromApi:
    _works = dict()

    def __new__(cls, description):
        if description not in cls._works:
            # Fetch the Work object, create it if it does not exist
            possible_works = Work.objects.filter(descriptions__description=description)
            if not possible_works:
                # Create the object
                desc_obj = Description.objects.create(
                    description=description, language_id="eng"
                )
                work_obj = Work.objects.create()
                work_obj.descriptions.add(desc_obj)
            else:
                work_obj = possible_works.first()

            cls._works[description] = work_obj
        else:
            work_obj = cls._works[description]

        return work_obj
