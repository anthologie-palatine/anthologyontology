from meleager.models import Manuscript, Medium
from meleager.models.medium import MediaTypes

from ..constants import API_URL
from ..helpers import to_date
from .base_manager import BaseManager
from .data_manager import DataManager
from .users import UserFromApi


class ImageFromApi(BaseManager):
    @classmethod
    def _create_object(cls, id_image, json_data):
        image_obj = Medium.objects.create(
            type=MediaTypes.IMAGE, url=json_data["URL"], unique_id=id_image
        )
        cls.update_obj_user_dates_from_json(image_obj, json_data)
        return image_obj


class ManuscriptFromApi(BaseManager):
    @classmethod
    def _create_object(cls, id_image, json_data):
        image_obj = Manuscript.objects.create(
            title=json_data["title"], url=json_data["URL"], credit=json_data["credit"]
        )

        cls.update_obj_user_dates_from_json(image_obj, json_data)
        return image_obj
