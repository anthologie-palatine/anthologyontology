from abc import ABC, abstractclassmethod

from ..helpers import to_date
from .users import UserFromApi


class BaseManager(ABC):
    def __new__(cls, key, json_data=None, key_name=None):
        if key is None:
            key = json_data[key_name]

        if not getattr(cls, "_objects", None):
            cls._objects = dict()

        if key in cls._objects:
            return cls._objects[key]
        else:
            obj = cls._create_object(key, json_data=json_data)
            cls._objects[key] = obj

        return obj

    @abstractclassmethod
    def _create_object(self, *args, **kwargs):
        pass

    @staticmethod
    def update_obj_user_dates_from_json(obj, json_data):
        if type(json_data["id_user"]) is dict:
            json_data = json_data["id_user"]

        user_obj = UserFromApi(json_data["id_user"])

        obj.creator = user_obj
        obj.last_editor = user_obj
        obj.created_at = to_date(json_data["createdAt"])
        obj.updated_at = to_date(json_data["updatedAt"])

        obj.save()
