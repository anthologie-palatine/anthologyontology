from meleager.models import Book, Description, Work


class BookFromApi:
    _books = dict()

    def __new__(cls, work, number):
        if (work, number) not in cls._books:
            # Fetch the Book object, create it if it does not exist - let's consider numbers are kind of unique
            books = Book.objects.filter(
                work__descriptions__description=work, number=number
            )
            if not books.count():
                work_obj_pk = Work.objects.get(descriptions__description=work).pk
                book_obj = Book.objects.create(work_id=work_obj_pk, number=number)
            else:
                book_obj = books.first()

            cls._books[(work, number)] = book_obj
        else:
            book_obj = cls._books[(work, number)]

        return book_obj
