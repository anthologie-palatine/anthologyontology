import logging
import re

from meleager.models import URN, Alignment, ExternalReference, Passage

from ..constants import API_URL, ANTHOLOGIA_URN_SOURCE
from ..helpers import to_date
from .authors import AuthorFromApi
from .base_manager import BaseManager
from .books import BookFromApi
from .comments import CommentFromApi
from .data_manager import DataManager
from .keywords import KeywordFromApi
from .media import ImageFromApi, ManuscriptFromApi
from .scholies import ScholiumFromApi
from .texts import TextFromApi
from .users import UserFromApi
from .works import WorkFromApi

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format="%(message)s")


class PassageFromApi:
    _passages = dict()

    def __new__(cls, id_entity):
        if id_entity not in cls._passages:
            passage_obj = cls._create_passage(id_entity)
            if passage_obj:
                cls._passages[id_entity] = passage_obj
        else:
            passage_obj = cls._passages[id_entity]

        return passage_obj

    @classmethod
    def get_by_id(cls, id_entity):
        return cls._passages[id_entity]

    @classmethod
    def _create_passage(cls, id_entity):
        passage_json = DataManager.get(f"{API_URL}/v1/entities/{id_entity}")
        matches = re.match(r"([\w\W]+) (\d+)\.(\d+)\.?(\w*)", passage_json["title"])
        if not matches:
            logger.info("!!! Cannot match title %s", passage_json["title"])
            return None

        work, book, fragment, sub_fragment = matches.groups()
        work_obj = WorkFromApi(work)
        if book in ("23", "123"):
            return None

        book_obj = BookFromApi(work, book)
        user_obj = UserFromApi(passage_json["id_user"]["id_user"])

        # Manual override - see issue #149
        if work == "2" and fragment == "1":
            # a = 96 in ASCII - convert number into letter
            sub_fragment = chr(int(sub_fragment) + 96)

        # CREATE THE PASSAGE OBJECT
        passage_obj = Passage.objects.create(
            unique_id=id_entity,
            book_id=book_obj.pk,
            fragment=fragment,
            sub_fragment=sub_fragment,
            last_editor=user_obj,
            creator=user_obj,
            created_at=to_date(passage_json["createdAt"]),
            updated_at=to_date(passage_json["updatedAt"]),
        )

        logger.info(
            "NEW | Passage %s.%s%s (AP API: %s)",
            book_obj.number,
            passage_obj.fragment,
            passage_obj.sub_fragment,
            id_entity,
        )

        # CREATE THE AUTHORS
        for author in passage_json["authors"]:
            author_obj = AuthorFromApi(author["id_author"])
            if author_obj:
                passage_obj.authors.add(author_obj)

        # CREATE THE TEXTS
        for version in passage_json["versions"]:
            text_obj = TextFromApi(version, book_obj.work.pk)
            if text_obj:
                passage_obj.texts.add(text_obj)

        # CREATE THE ALIGNMENTS
        for item in passage_json["alignements"]:
            text_1 = None
            text_2 = None

            try:
                text_1 = TextFromApi.get_by_id(item["source"])
            except KeyError:
                logger.info(
                    "Warning - cannot find text ID %s in alignment for entity ID %s",
                    item["source"],
                    passage_json["id_entity"],
                )

            try:
                text_2 = TextFromApi.get_by_id(item["target"])
            except KeyError:
                logger.info(
                    "Warning - cannot find text ID %s in alignment for entity ID %s",
                    item["target"],
                    passage_json["id_entity"],
                )

            if text_1 or text_2:
                align_obj = Alignment.objects.create(
                    text_1=text_1, text_2=text_2, alignment_data=item["json"], unique_id=item['id_align'],
                )
                BaseManager.update_obj_user_dates_from_json(align_obj, item)
            else:
                logger.info(
                    "Both texts ID (%s, %s) from alignment do not exist",
                    item["source"],
                    item["target"],
                )

        # CREATE THE IMAGES
        for image_json in passage_json["images"]:
            image_obj = ImageFromApi(
                key=None, json_data=image_json, key_name="id_image"
            )
            passage_obj.images.add(image_obj)

        # CREATE THE MANUSCRIPTS
        for image_json in passage_json["imagesManuscript"]:
            image_obj = ManuscriptFromApi(
                key=None, json_data=image_json, key_name="id_image"
            )
            passage_obj.manuscripts.add(image_obj)

        # CREATE THE SCHOLIES (RELATION ADDED IN SCHOLIUM)
        for scholium in passage_json["scholies"]:
            scholium_obj = ScholiumFromApi(scholium["id_scholie"], passage_obj.pk)

        # CREATE THE NOTES
        for note in passage_json["notes"]:
            note_obj = CommentFromApi(note["id_note"])
            passage_obj.comments.add(note_obj)

        # ALTERNATIVE URIS

        # PERSEUS
        for url in passage_json["uris"]:
            url_obj, _ = URN.objects.update_or_create(
                urn=url["value"], defaults={"source": "perseus"}
            )
            passage_obj.alternative_urns.add(url_obj)

        # ANTHOLOGIA
        anthologia_urn, _ = URN.objects.update_or_create(
            urn=f"{API_URL}/v1/entities/{id_entity}", defaults={"source": ANTHOLOGIA_URN_SOURCE}
        )
        passage_obj.alternative_urns.add(anthologia_urn)

        # EXTERNAL REFERENCES
        for extref in passage_json["externalRef"]:
            ref_obj = ExternalReference.objects.create(
                title=extref["title"], url=extref["url"]
            )
            passage_obj.external_references.add(ref_obj)

        # INTERNAL REFERENCES
        for intref in (
            passage_json["internalRef_sources"] + passage_json["internalRef_targets"]
        ):
            try:
                ref_obj = cls.get_by_id(intref["id_entity"])
                passage_obj.internal_references.add(ref_obj)
            except KeyError:
                logger.info(
                    "!!! Looks like Passage (API ID %s) does not exist yet!",
                    intref["id_entity"],
                )

        # KEYWORDS
        for kw in passage_json["keywords"]:
            kw_obj = KeywordFromApi(kw["id_keyword"])
            passage_obj.keywords.add(kw_obj)
        return passage_obj
