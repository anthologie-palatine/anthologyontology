import logging

from meleager.models import Description, Edition, Text, Work

from ..constants import LANG_API2CODE
from ..helpers import to_date
from .editions import EditionFromApi

logger = logging.getLogger(__name__)


class AbstractTextFromApi:
    # To be overriden by children classes
    json_key = None
    text_key = None

    def __new__(cls, json_data, work_obj_pk):
        if json_data[cls.json_key] not in cls._texts:
            text_obj = cls._create_text(json_data, work_obj_pk)
            cls._texts[json_data[cls.json_key]] = text_obj
        else:
            text_obj = cls._texts[json_data[cls.json_key]]

        return text_obj

    @classmethod
    def get_by_id(cls, text_id):
        try:
            return cls._texts[text_id]
        except KeyError as exc:
            logging.warn("!!! Error: cannot find text %s", text_id)
            raise KeyError from exc

    @classmethod
    def _create_text(cls, json_data, work_obj_pk):
        text = json_data[cls.text_key]
        lang_code = LANG_API2CODE[json_data["id_language"]]

        edition_obj = EditionFromApi(
            json_data["edition"], json_data["id_language"], work_obj_pk
        )

        text_obj, created = Text.objects.get_or_create(
            # This is the worst #173
            unique_id=int("9999" + str(json_data[cls.json_key])),
            text=text,
            language_id=lang_code,
            edition=edition_obj,
            created_at=to_date(json_data["createdAt"]),
            updated_at=to_date(json_data["updatedAt"]),
        )

        action = "NEW" if created else "OK"

        # TODO: add edition information
        logger.info("%s | Text (%s) %s", action, lang_code.upper(), text[0:25])
        return text_obj


class TextFromApi(AbstractTextFromApi):
    json_key = "id_entity_version"
    text_key = "text_translated"
    # This is very poor (#116)
    _texts = dict()


class ScholiumTextFromApi(AbstractTextFromApi):
    json_key = "id_scholie_version"
    text_key = "text"
    # This is very poor (#116)
    _texts = dict()

    def __new__(cls, json_data):
        """ We don't use work_obj_pk here because scholia have a set work / edition (see issue #116) """
        return super().__new__(cls, json_data, None)

    @classmethod
    def _create_text(cls, json_data, work_obj_pk=None):
        """ We don't use work_obj_pk here because scholia have a set work / edition (see issue #116) """

        text = json_data[cls.text_key]
        lang_code = LANG_API2CODE[json_data["id_language"]]

        desc, _ = Description.objects.get_or_create(
            description="Work Scholia", language_id="eng"
        )

        if not desc.works.count():
            work = Work.objects.create()
            work.descriptions.add(desc)
        else:
            work = desc.works.first()

        if not work.editions.count():
            desc, _ = Description.objects.get_or_create(
                description="Edition Scholia", language_id="eng"
            )
            editions = Edition.objects.filter(descriptions=desc).count()
            if not editions:
                edition, _ = Edition.objects.get_or_create(
                    work=work, edition_type=Edition.EditionType.SCHOLIA
                )
                edition.descriptions.add(desc)
            else:
                edition = editions.first()
        else:
            edition = work.editions.first()

        text_obj, created = Text.objects.get_or_create(
            text=text,
            language_id=lang_code,
            edition=edition,
            unique_id=json_data[cls.json_key],
            created_at=to_date(json_data["createdAt"]),
            updated_at=to_date(json_data["updatedAt"]),
        )

        action = "NEW" if created else "OK"

        # TODO: add edition information
        logger.info("%s | Text (%s) %s", action, lang_code.upper(), text[0:25])

        return text_obj
