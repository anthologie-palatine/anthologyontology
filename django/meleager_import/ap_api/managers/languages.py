import logging

from iso639 import languages

from meleager.models import Language

from ..constants import API_URL
from .data_manager import DataManager

logger = logging.getLogger(__name__)


def create_languages():
    for lang in languages:
        if not lang.part3:
            continue

        _, created = Language.objects.update_or_create(
            code=lang.part3, defaults={"iso_name": lang.name}
        )
        logger.info("Language: %s %s", lang.part3, ["(OK)", "(NEW)"][created])
