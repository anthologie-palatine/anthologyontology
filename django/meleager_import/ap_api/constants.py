from django.conf import settings

API_URL = "https://anthologia.ecrituresnumeriques.ca/api"
IMPORT_DIR = settings.AP_API_IMPORT_DIR
ANTHOLOGIA_URN_SOURCE = "anthologia"

LANG_API2CODE = {
    1: "ita",
    2: "fra",
    11: "spa",
    3: "eng",
    4: "grc",
    12: "por",
    # code grec ancien
    # TODO : definir des langues supplementaires
    5: "fra",
    6: "fra",
    7: "grc",
    8: "grc",
    9: "grc",
    10: "grc",
    13: "lat",
}
