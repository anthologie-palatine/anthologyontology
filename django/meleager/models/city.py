# from django.contrib.gis.db import models as gis_models
from django.db import models
from django.urls import reverse

from meleager.helpers.wikidata import get_wiki_names_from_url

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractUniqueIdentifier,
    AlternativeURNs,
)
from .language import Language
from .name import Name, name_choices_from_dict
from .urn import URN


class CityQuerySet(models.QuerySet):
    def get_name_choices(self):
        return name_choices_from_dict(
            self.filter(names__isnull=False).values(
                "pk", "names__name", "names__language__code2"
            )
        )


class CityManager(models.Manager):
    def get_queryset(self):
        return CityQuerySet(self.model, using=self._db)

    def get_or_create_from_names(self, source, url, names):
        # If we have a matching URL without a source, update it
        urn, created = URN.objects.get_or_create(urn=url)
        if source and not urn.source:
            urn.source = source
            urn.save()

        # If we have an existing city with that URN, do not create it
        existing_city_with_urn = self.filter(alternative_urns__urn=urn)
        if existing_city_with_urn.count():
            return existing_city_with_urn.first()

        name_objs = [
            Name.objects.create(name=name, language=lang) for lang, name in names
        ]

        city = self.create()
        city.alternative_urns.add(urn)
        city.names.add(*name_objs)
        return city

    def get_or_create_from_url(self, url):
        names = get_wiki_names_from_url(url)

        if not names:
            # Nothing in Wikidata - don't do anything
            return None

        return self.get_or_create_from_names(
            source="wikidata",
            url=url,
            names=[
                (Language.objects.get(code=lang_code), name)
                for lang_code, name in names.items()
            ],
        )


class City(
    AbstractUniqueIdentifier,
    AbstractEditableResource,
    AbstractDescriptableResource,
    AlternativeURNs,
    models.Model,
):
    """City model

    A city has names.
    The location is stored using GIS extensions.

    """

    objects = CityManager()
    names = models.ManyToManyField(
        "meleager.Name",
        related_name="cities",
        help_text="The multilingual names for this object.",
    )

    # location = gis_models.PointField(null=True)
    longitude = models.DecimalField(
        max_digits=9, decimal_places=5, null=True, blank=True, default=None
    )
    latitude = models.DecimalField(
        max_digits=9, decimal_places=5, null=True, blank=True, default=None
    )

    def get_absolute_url(self):
        return reverse("web:city-detail", args=(self.pk,))

    def __str__(self):
        first_name = self.names.first()
        if first_name:
            return first_name.name
        else:
            return f"City PK {self.pk}"

    class Meta:
        verbose_name_plural = "cities"
        ordering = [
            "pk",
        ]
