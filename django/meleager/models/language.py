from django.db import models
from iso639 import languages


class LanguageManager(models.Manager):
    def preferred(self):
        return self.filter(preferred=True)


class Language(models.Model):
    objects = LanguageManager()

    code = models.CharField("Language code (ISO639)", max_length=3, primary_key=True)
    code2 = models.CharField("Language code (ISO639) - 2 letters", max_length=2, db_index=True, default="")
    iso_name = models.CharField(
        "Language name", max_length=100, null=False, blank=False
    )
    preferred = models.BooleanField(
        "Favorite language", null=False, default=False, db_index=True
    )

    class Meta:
        ordering = ("-preferred", "code")

    def __str__(self):
        return f"Lang: {self.code}"
