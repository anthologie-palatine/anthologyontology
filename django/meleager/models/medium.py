from django.db import models

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractUniqueIdentifier,
)


class MediaTypes(models.TextChoices):
    IMAGE = ("image", "Image")
    VIDEO = ("video", "Video")
    AUDIO = ("audio", "Audio")


class Medium(
    AbstractEditableResource,
    AbstractDescriptableResource,
    AbstractUniqueIdentifier,
    models.Model,
):
    title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
    )
    keywords = models.ManyToManyField(
        "meleager.Keyword",
        related_name="media",
        help_text="The keywords that tag this resource.",
    )
    type = models.CharField(
        choices=MediaTypes.choices, default=MediaTypes.IMAGE, max_length=8
    )
    url = models.URLField(help_text="The URL of this image", max_length=500)

    def __str__(self):
        return f"Medium “{self.title}”"

    def get_absolute_url(self):
        return f"{self.passages.first().get_absolute_url()}#media"


class Manuscript(AbstractEditableResource, AbstractDescriptableResource, models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    url = models.URLField(help_text="The URL of this image", max_length=500)
    credit = models.CharField(max_length=255, null=False, blank=True, default="")
    keywords = models.ManyToManyField(
        "meleager.Keyword",
        related_name="manuscripts",
        help_text="The keywords that tag this resource.",
    )

    def __str__(self):
        return f"Manuscript {self.pk}"

    def first_related_object(self):
        return self.passages.first() or self.scholia.first()

    def get_absolute_url(self):
        return f"{self.first_related_object().get_absolute_url()}#manuscripts"
