from django.db import models
from django.template.defaultfilters import truncatewords
from django.utils.safestring import mark_safe

from ..templatetags.markdownify import markdownify

from .abstract import AbstractEditableResource


class Description(AbstractEditableResource, models.Model):
    """
    Model that describes a mono-lingual description
    """

    description = models.TextField(null=False, default="", blank=True)
    language = models.ForeignKey(
        "meleager.Language", on_delete=models.SET_DEFAULT, null=False, default="und"
    )

    def __str__(self):
        return f"{truncatewords(self.description, 5)} [{self.language}]"

    def description_html(self, level=3):
        description_html = markdownify(self.description)
        # Convert levels of titles indentation given `level` parameter,
        # we do not want to have multiple <h1>’s on the page for instance.
        description_html = (
            description_html.replace("<h1>", f"<h{level}>")
            .replace("</h1>", f"</h{level}>")
            .replace("<h2>", f"<h{level + 1}>")
            .replace("</h2>", f"</h{level + 1}>")
        )
        return mark_safe(description_html)

    def first_related_object(self):
        return self.meleager_passage.first() or self.meleager_scholium.first()

    def get_absolute_url(self):
        return f"{self.first_related_object().get_absolute_url()}#description-{self.pk}"
