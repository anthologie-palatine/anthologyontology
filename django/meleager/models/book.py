""" Makapuf

Mahuf.

"""
from django.apps import apps
from django.db import models

from .abstract import AbstractDescriptableResource, AbstractEditableResource


class BookManager(models.Manager):
    """ Custom manager for books """

    def get_by_natural_key(self, number, work_pk):
        """ Used by loaddata to create books with natural keys """
        work = apps.get_model("meleager", "Work").objects.get(pk=work_pk)
        return self.get(number=number, work=work)


class Book(AbstractDescriptableResource, AbstractEditableResource, models.Model):
    """Book model

    A book "belongs to" a Work.

    """

    objects = BookManager()

    work = models.ForeignKey("meleager.Work", on_delete=models.CASCADE)
    number = models.IntegerField("Book number", null=False, db_index=True)

    def natural_key(self):
        """ Required to use natural keys in dumpdata """
        return (self.number, self.work.pk)

    natural_key.dependencies = ["meleager.Work"]

    def __str__(self):
        return f"Book {self.number}"

    class Meta:
        ordering = ['number']
