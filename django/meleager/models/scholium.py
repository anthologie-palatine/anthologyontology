import re

from urllib.parse import urlparse
from pathlib import Path

from django.db import models
from django.db.models import BooleanField, Case, When
from django.urls import reverse
from django.utils.translation import gettext as _

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
    AlternativeURNs,
)


class ScholiumQuerySet(models.QuerySet):

    def get_pk_from_urn(self, *args):
        if not args:
            return None

        values = list()

        if type(args[0]) == str:
            values = args[0].split(".")
            if not values[-1].isdigit() and len(values) < 4:
                match = re.match(r"(\d+)(\w+)", values[-1])
                if match:
                    values[-1] = match[1]
                    values.append(match[2])
                else:
                    raise AttributeError("No match found for %s" % args[0])
        elif len(args) >= 3:
            values = list(args)
        else:
            raise AttributeError("Invalid attributes")

        if len(values) == 2:
            values.append("")

        book_number, fragment, sub_fragment, number = int(values[0]), int(values[1]), values[2], int(values[3])

        return self.filter(
            passage__book__number=book_number, passage__fragment=fragment, passage__sub_fragment=sub_fragment, number=number
        ).values_list('pk', flat=True).first()


class Scholium(
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractDescriptableResource,
    AbstractUniqueIdentifier,
    AlternativeURNs,
    models.Model,
):

    objects = ScholiumQuerySet.as_manager()

    number = models.IntegerField("Scholium ID", db_index=True, null=True)

    passage = models.ForeignKey(
        "meleager.Passage", related_name="scholia", on_delete=models.CASCADE, null=True
    )

    cities = models.ManyToManyField(
        "meleager.City", related_name="scholia", through="meleager.CityScholium"
    )

    texts = models.ManyToManyField(
        "meleager.Text", help_text="Text for this scholium", related_name="scholia"
    )

    manuscripts = models.ManyToManyField(
        "meleager.Manuscript",
        related_name="scholia",
        help_text="The image of the manuscript for this passage.",
        blank=True,
    )

    images = models.ManyToManyField(
        "meleager.Medium",
        related_name="scholia",
        help_text="The images associated to this resource.",
        blank=True,
    )

    comments = models.ManyToManyField(
        "meleager.Comment",
        related_name="scholia",
        help_text="The comments of this resource.",
        blank=True,
    )

    keywords = models.ManyToManyField(
        "meleager.Keyword",
        related_name="scholia",
        help_text="The keywords that tag this resource.",
        through="meleager.KeywordScholium",
        blank=True,
    )

    def get_absolute_url(self):
        return reverse(
            "web:scholium-detail",
            args=(
                self.passage.book.number,
                self.passage.fragment,
                self.passage.sub_fragment,
                self.number,
            ),
        )

    @property
    def urn_value(self):
        return Path(urlparse(self.get_absolute_url()).path).name

    def __str__(self):
        return _("Scholium %(book)s.%(fragment)s%(sub_fragment)s.%(number)s") % {
            "book": self.passage.book.number,
            "fragment": self.passage.fragment,
            "sub_fragment": f".{self.passage.sub_fragment}"
            if self.passage.sub_fragment
            else "",
            "number": self.number,
        }

    class Meta:
        verbose_name_plural = "Scholia"
        ordering = ['passage__book__number', 'passage__fragment', 'passage__sub_fragment', 'number']

    def get_m2m_objects_with_perms(self, user, passage_field, m2m_field):
        """Return a list of tuples (object, bool)

        The objects are taken from the model (based on `passage_field`, ex: authors).
        The m2m_field is the name of the model field on the M2M model (ex: `author`).
        The permissions are checked for user `user`.
        """
        if type is None:
            return []

        objects = getattr(self, passage_field)

        default_perm = False
        if user.is_authenticated:
            if user.has_perm("meleager.manage_all_mlgr"):
                default_perm = True

        # If the user is an "admin editor" and can unlink anything, just make all perms True
        # If the user is the anonymous user, make all perms False
        if default_perm or not user.is_authenticated:
            return [
                {"object": object, "can_be_unlinked": default_perm}
                for object in objects.all()
            ]

        m2m_model = objects.through

        # From the through model, check if the user in the m2m relation is the same as the user provided
        m2m_objects = (
            m2m_model.objects.filter(scholium_id=self.pk)
            .annotate(
                can_be_unlinked=Case(
                    When(user_id=user.pk, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("can_be_unlinked", m2m_field)
        )

        # Create a mapping dictionary: {PK of related object: bool (can_be_unlinked)}
        mapping = {d[m2m_field]: d["can_be_unlinked"] for d in m2m_objects}

        # Loop through the objects and match with the permissions
        out = [
            {"object": object, "can_be_unlinked": mapping[object.pk]}
            for object in objects.all()
        ]
        return out
