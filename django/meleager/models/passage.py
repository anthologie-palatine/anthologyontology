import re
from urllib.parse import urlparse
from pathlib import Path

from django.apps import apps
from django.db import models
from django.db.models import BooleanField, Case, When
from django.urls import reverse
from django.utils.translation import gettext as _

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
    AlternativeURNs,
)
from .abstract.editable import WithCreatorMixin


class PassageQuerySet(models.QuerySet):
    def get_passage_choices(self):
        return [
            (pk, f"{book_number}.{fragment}{sub_fragment}")
            for (pk, book_number, fragment, sub_fragment) in self.values_list(
                "pk", "book__number", "fragment", "sub_fragment"
            )
        ]

    def get_by_natural_key(self, fragment, sub_fragment, *book_natural_key):
        book = apps.get_model("meleager", "Book").objects.get_by_natural_key(
            *book_natural_key
        )
        return self.get(fragment=fragment, sub_fragment=sub_fragment, book=book)

    def get_pk_from_urn(self, *args):
        if not args:
            return None

        values = list()

        if type(args[0]) == str:
            values = args[0].split(".")
            if not values[-1].isdigit() and len(values) < 3:
                match = re.match(r"(\d+)(\w+)", values[-1])
                if match:
                    values[-1] = match[1]
                    values.append(match[2])
                else:
                    raise AttributeError("No match found for %s" % args[0])
        elif len(args) >= 2:
            values = list(args)
        else:
            raise AttributeError("Invalid attributes")

        if len(values) == 2:
            values.append("")

        book_number, fragment, sub_fragment = int(values[0]), int(values[1]), values[2]

        return self.filter(
            book__number=book_number, fragment=fragment, sub_fragment=sub_fragment
        ).values_list('pk', flat=True).first()


class Passage(
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractDescriptableResource,
    AlternativeURNs,
    AbstractUniqueIdentifier,
    models.Model,
):
    """ Passage model """

    objects = PassageQuerySet.as_manager()

    authors = models.ManyToManyField(
        "meleager.Author", through="meleager.AuthorPassage", related_name="passages"
    )

    cities = models.ManyToManyField(
        "meleager.City", through="meleager.CityPassage", related_name="passages"
    )

    book = models.ForeignKey(
        "meleager.Book",
        related_name="passages",
        help_text="Book the passage is in",
        on_delete=models.CASCADE,
        db_index=True,
    )

    fragment = models.IntegerField(
        help_text="The fragment number in book.", db_index=True
    )

    sub_fragment = models.CharField(
        help_text="The sub fragment number in book.",
        blank=True,
        null=False,
        default="",
        max_length=3,
        db_index=True,
    )

    images = models.ManyToManyField(
        "meleager.Medium",
        related_name="passages",
        help_text="The images associated to this resource.",
    )

    comments = models.ManyToManyField(
        "meleager.Comment",
        related_name="passages",
        help_text="The comments of this resource.",
    )

    texts = models.ManyToManyField(
        "meleager.Text",
        help_text="Text for this passage",
        related_name="passages",
        through="meleager.TextPassage",
    )

    external_references = models.ManyToManyField(
        "meleager.ExternalReference", related_name="passages"
    )
    internal_references = models.ManyToManyField(
        "self", symmetrical=True, through="PassageInternalReference"
    )

    manuscripts = models.ManyToManyField(
        "meleager.Manuscript",
        help_text="The image of the manuscript for this passage.",
        related_name="passages",
    )

    keywords = models.ManyToManyField(
        "meleager.Keyword",
        related_name="passages",
        help_text="The keywords that tag this resource.",
        through="meleager.KeywordPassage",
        blank=True,
    )

    def get_m2m_objects_with_perms(self, user, passage_field, m2m_field):
        """Return a list of tuples (object, bool)

        The objects are taken from the model (based on `passage_field`, ex: authors).
        The m2m_field is the name of the model field on the M2M model (ex: `author`).
        The permissions are checked for user `user`.
        """
        if type is None:
            return []

        objects = getattr(self, passage_field)
        all_objects = objects.all().prefetch_related("names__language")
        if passage_field == "keywords":
            all_objects = all_objects.select_related("category")

        default_perm = False
        if user.is_authenticated:
            if user.has_perm("meleager.manage_all_mlgr"):
                default_perm = True

        # If the user is an "admin editor" and can unlink anything, just make all perms True
        # If the user is the anonymous user, make all perms False
        if default_perm or not user.is_authenticated:
            return [
                {"object": object, "can_be_unlinked": default_perm}
                for object in all_objects
            ]

        m2m_model = objects.through

        # From the through model, check if the user in the m2m relation is the same as the user provided
        m2m_objects = (
            m2m_model.objects.filter(passage_id=self.pk)
            .annotate(
                can_be_unlinked=Case(
                    When(user_id=user.pk, then=True),
                    default=False,
                    output_field=BooleanField(),
                )
            )
            .values("can_be_unlinked", m2m_field)
        )

        # Create a mapping dictionary: {PK of related object: bool (can_be_unlinked)}
        mapping = {d[m2m_field]: d["can_be_unlinked"] for d in m2m_objects}

        # Loop through the objects and match with the permissions
        out = [
            {"object": object, "can_be_unlinked": mapping[object.pk]}
            for object in all_objects
        ]
        return out

    def check_user_perms(self, user):
        return (
            user.has_perm("meleager.manage_all_passages")
            or user == self.creator
            and user.has_perm("meleager.manage_own_passages")
        )

    def _get_url(self, **kwargs):
        """Generic method to get previous, next, or instance URL"""

        # We build the rules dynamically to keep the logic together
        if kwargs.get("next", False):
            rule_operator = "gt"
            order = ("book__number", "fragment")
        elif kwargs.get("previous", False):
            rule_operator = "lt"
            order = ("-book__number", "-fragment")
        else:
            return reverse(
                "web:passage-detail",
                args=(self.book.number, self.fragment, self.sub_fragment),
            )

        # There are the comparison rules (next = higher value, prev = lower value)
        book_rule = f"book__number__{rule_operator}"
        fragment_rule = f"fragment__{rule_operator}"
        sub_fragment_rule = f"sub_fragment__{rule_operator}"

        # We combine the comparisons above with the rules below
        same_book = models.Q(book__number=self.book.number)
        same_fragment = models.Q(fragment=self.fragment)

        # This is the prev/next logic in the database
        rule_1 = models.Q(**{book_rule: self.book.number})
        rule_2 = same_book & models.Q(**{fragment_rule: self.fragment})
        rule_3 = (
            same_book
            & same_fragment
            & models.Q(**{sub_fragment_rule: self.sub_fragment})
        )

        # Let's look for a match
        combined = rule_1 | rule_2 | rule_3
        passages = Passage.objects.filter(combined).order_by(*order)

        if not passages:
            return ""

        return passages.first().get_absolute_url()

    def get_previous_url(self):
        return self._get_url(previous=True)

    def get_next_url(self):
        return self._get_url(next=True)

    def get_absolute_url(self):
        return self._get_url()

    @property
    def urn_value(self):
        return Path(urlparse(self.get_absolute_url()).path).name

    def natural_key(self):
        return (self.fragment, self.sub_fragment) + self.book.natural_key()

    natural_key.dependencies = ["meleager.Work", "meleager.Book", "meleager.Author"]

    def __str__(self):
        return _("Passage %(book)s.%(fragment)s%(sub_fragment)s") % {
            "book": self.book.number,
            "fragment": self.fragment,
            "sub_fragment": f".{self.sub_fragment}" if self.sub_fragment else "",
        }

    class Meta:
        permissions = [
            ("manage_own_mlgr", "Can manage (edit/delete) their own Passage items"),
            ("manage_all_mlgr", "Can manage (edit/delete) all Passage items"),
            ("can_add_mlgr", "Can create Meleager artefacts (Author, Text, etc)"),
        ]
        ordering = ['book__number', 'fragment', 'sub_fragment']


class PassageInternalReference(WithCreatorMixin, models.Model):

    REFERENCE_TYPE = [
        ('DEFAULT', _('Default')),
        ('STYLISTIC', _('Stylistic variation')),
        ('RHETORIC', _('Rhetoric variation')),
        ('PARADIGMATIC', _('Paradigmatic variation')),
    ]

    from_passage = models.ForeignKey(
        Passage, on_delete=models.CASCADE, related_name="from_passage"
    )
    to_passage = models.ForeignKey(
        Passage, on_delete=models.CASCADE, related_name="to_passage"
    )
    reference_type = models.CharField(
        max_length=32,
        choices=REFERENCE_TYPE,
        default='DEFAULT')

    class Meta:
        unique_together = ("from_passage", "to_passage")
        ordering = ['pk']
