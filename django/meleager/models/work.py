from django.db import models

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AlternativeURNs,
)


class Work(
    AbstractEditableResource,
    AbstractDescriptableResource,
    AlternativeURNs,
    models.Model,
):

    descriptions = models.ManyToManyField(
        "meleager.Description",
        related_name="works",
        help_text="Descriptions for the work.",
    )

    def __str__(self):
        first_description = self.descriptions.first()
        if first_description:
            return first_description.description
        else:
            return f"Work {self.pk}"
