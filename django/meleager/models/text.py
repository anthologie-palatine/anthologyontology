from django.db import models
from django.template.defaultfilters import truncatewords

from .abstract import (
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
)


class CommentTextAnchor(models.Model):
    text = models.ForeignKey("meleager.Text", on_delete=models.CASCADE, null=False)
    comment = models.ForeignKey(
        "meleager.Comment", on_delete=models.CASCADE, null=False
    )
    anchor_word = models.CharField(max_length=255, default=" ", null=False)
    occurrence = models.IntegerField(default=0)


class Text(
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
    models.Model,
):
    class Status(models.IntegerChoices):
        DRAFT = 0
        PUBLISHED = 1
        DELETED = 2

    language = models.ForeignKey(
        "meleager.Language", on_delete=models.SET_DEFAULT, null=False, default="und"
    )

    edition = models.ForeignKey(
        "meleager.Edition",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="The edition of this text.",
    )

    status = models.IntegerField(choices=Status.choices, default=Status.PUBLISHED)
    text = models.TextField(help_text="The string of this text")

    comments = models.ManyToManyField(
        "meleager.Comment",
        related_name="texts",
        help_text="The comments of this resource.",
        through=CommentTextAnchor,
    )

    def __str__(self):
        return f"[{self.language.code}] {truncatewords(self.text, 6)}"

    def first_related_object(self):
        return self.passages.first() or self.scholia.first()

    def get_absolute_url(self):
        return f"{self.first_related_object().get_absolute_url()}#texts"

    def alignment_tokens(self):
        punctuations_prefixes = ("«", '"')
        punctuations_middles = ("'", "’", "-")
        punctuations_suffixes = (".", "!", "?", ",", ";", ":", "—", "»", '"')

        def split_punctuations(pre, word, post):
            if word.startswith(punctuations_prefixes) or word.endswith(
                punctuations_suffixes
            ):
                if word.startswith(punctuations_prefixes):
                    for punctuations_prefixe in punctuations_prefixes:
                        if word.startswith(punctuations_prefixe):
                            word = word[1:].strip()
                            pre.append(punctuations_prefixe)
                            break

                if word.endswith(punctuations_suffixes):
                    for punctuations_suffixe in punctuations_suffixes:
                        if word.endswith(punctuations_suffixe):
                            word = word[:-1].strip()
                            post.insert(0, punctuations_suffixe)
                            break

                return split_punctuations(pre, word, post)
            return pre, word.strip(), post

        def split_words(sentence):
            # Split the sentence on empty spaces + middles (kept).
            words = []
            for word in sentence.split():
                has_middle = False
                for middle in punctuations_middles:
                    if middle in word:
                        has_middle = True
                        word = word.replace(middle, f" {middle} ")
                        words += word.split()
                if has_middle:
                    has_middle = False
                else:
                    words.append(word)
            return words

        tokenized_text = []
        i = 1
        for paragraph in self.text.split("\n\n"):
            for sentence in paragraph.splitlines():
                tokenized_line = []
                sentence = sentence.strip()
                for word in split_words(sentence):
                    if (
                        word
                        in punctuations_middles
                        + punctuations_prefixes
                        + punctuations_suffixes
                    ):
                        tokenized_line.append(
                            {
                                "p": word,
                            }
                        )
                        continue

                    pre_punctuations, word, post_punctuations = split_punctuations(
                        [], word, []
                    )
                    for pre_punctuation in pre_punctuations:
                        tokenized_line.append(
                            {
                                "p": pre_punctuation,
                            }
                        )
                    tokenized_line.append(
                        {
                            "t": word,
                            "h": [],
                            "pos": [1, i],
                            "parent": 0,
                            "children": i,
                        }
                    )
                    i += 1
                    for post_punctuation in post_punctuations:
                        tokenized_line.append(
                            {
                                "p": post_punctuation,
                            }
                        )

                tokenized_text.append(tokenized_line)
            tokenized_text.append([])  # Creates a new paragraph.
        return [tokenized_text]

    class Meta(AbstractValidableResource.Meta):
        ordering = ["id"]
