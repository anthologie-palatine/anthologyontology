from django.db import models
from django_extensions.db.fields.json import JSONField
from django.utils.translation import gettext as _

from .abstract import (
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
)


class Alignment(
    AbstractEditableResource,
    AbstractValidableResource,
    AbstractUniqueIdentifier,
    models.Model,
):
    """Alignment model

    Represents the M2M relationship between two texts.
    The alignment data is in JSON format.

    """

    text_1 = models.ForeignKey(
        "meleager.Text",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_text_1",
        related_query_name="%(app_label)s_%(class)ss_text_1",
        help_text="The first text, if null, the text is missing (deleted)",
    )

    text_2 = models.ForeignKey(
        "meleager.Text",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_text_2",
        related_query_name="%(app_label)s_%(class)ss_text_2",
        help_text="The second text, if null, the text is missing (deleted)",
    )

    alignment_data = JSONField(help_text="The alpheios alignment json.")

    class Meta(AbstractValidableResource.Meta):
        ordering = ["id"]

    def __str__(self):
        return (
            f"Alignment {self.language_code_1.upper()}-{self.language_code_2.upper()}"
        )

    def get_absolute_url(self):
        return self.text_1.first_related_object().get_absolute_url() + "#alignments"

    @property
    def language_code_1(self):
        return self.text_1 and self.text_1.language.code or _("Unknown")

    @property
    def language_code_2(self):
        return self.text_2 and self.text_2.language.code or _("Unknown")

    @property
    def is_orphan(self):
        return not self.text_1 or not self.text_2

    @property
    def is_outdated(self):
        if self.is_orphan:
            return False
        return (
            self.updated_at < self.text_1.updated_at
            or self.updated_at < self.text_2.updated_at
        )
