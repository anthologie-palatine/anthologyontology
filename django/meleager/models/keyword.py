import operator
from itertools import groupby

from django.db import models
from django.urls import reverse

from meleager.helpers.unicode import strip_accents
from meleager.helpers.wikidata import get_wiki_names_from_url

from .abstract import (
    AbstractEditableResource,
    AbstractUniqueIdentifier,
    AlternativeURNs,
)
from .language import Language
from .name import Name, name_choices_from_dict
from .urn import URN


class KeywordCategory(
    AlternativeURNs, AbstractEditableResource, AbstractUniqueIdentifier, models.Model
):
    names = models.ManyToManyField(
        "meleager.Name",
        related_name="keyword_categories",
        help_text="The multilingual names for this object.",
    )

    def __str__(self):
        first_name = self.names.first()
        if first_name:
            return first_name.name
        else:
            return f"Category PK {self.pk}"

    def current_language_name(self):
        current_language_name = self.names.get_current_language_name()
        return f"{current_language_name.name} ({current_language_name.language.code})"

    class Meta:
        verbose_name_plural = "Keyword categories"


class KeywordQuerySet(models.QuerySet):
    def get_keyword_choices(self, together=True):
        kw_without_cats = self.filter(category=None)
        kw_with_cats = self.exclude(category=None)
        # .select_related("names__name", "category__names__name")

        fields = (
            "pk",
            "names__name",
            "names__language__code2",
            "category__names__name",
        )

        choices_no_cats = []
        choices_cats = []

        # WE NEED TO SORT THE QUERYSETS BEFORE USING GROUPBY!

        if kw_with_cats.count():
            choices_cats = sorted(
                [
                    (cat_name, name_choices_from_dict(kw_group))
                    for cat_name, kw_group in groupby(
                        kw_with_cats.order_by("category__names__name").values(*fields),
                        key=operator.itemgetter("category__names__name"),
                    )
                ],
                key=lambda cat_name_names: strip_accents(cat_name_names[0]),
            )

        if kw_without_cats.count():
            choices_no_cats = name_choices_from_dict(kw_without_cats.values(*fields))

        if together:
            return choices_no_cats + choices_cats
        else:
            return choices_no_cats, choices_cats


class KeywordManager(models.Manager):
    def get_queryset(self):
        return KeywordQuerySet(self.model, using=self._db)

    def get_or_create_from_names(self, category, source, url, names):
        # If we have a matching URL without a source, update it
        urn, created = URN.objects.get_or_create(urn=url)
        if source and not urn.source:
            urn.source = source
            urn.save()

        # If we have an existing keyword with that URN, do not create it
        existing_kw_with_urn = self.filter(alternative_urns__urn=urn)
        if existing_kw_with_urn.count():
            return existing_kw_with_urn.first()

        name_objs = [
            Name.objects.create(name=name, language=lang) for lang, name in names
        ]

        kw = self.create(category=category)
        kw.alternative_urns.add(urn)
        kw.names.add(*name_objs)
        return kw

    def get_or_create_from_url(self, url, category):
        names = get_wiki_names_from_url(url)

        if not names:
            # Nothing in Wikidata - don't do anything
            return None

        return self.get_or_create_from_names(
            category=category,
            source="wikidata",
            url=url,
            names=[
                (Language.objects.get(code=lang_code), name)
                for lang_code, name in names.items()
            ],
        )


class Keyword(
    AlternativeURNs, AbstractEditableResource, AbstractUniqueIdentifier, models.Model
):
    objects = KeywordManager()
    names = models.ManyToManyField(
        "meleager.Name",
        related_name="keywords",
        help_text="The multilingual names for this object.",
    )

    category = models.ForeignKey(
        KeywordCategory,
        on_delete=models.SET_NULL,
        related_name="keywords",
        help_text="The category this keyword belongs to.",
        null=True,
    )

    def get_absolute_url(self):
        return reverse("web:keyword-detail", args=(self.pk,))

    def __str__(self):
        first_name = self.names.first()
        if first_name:
            return f"{first_name.name} ({self.pk})"
        else:
            return f"Keyword PK {self.pk}"

    class Meta:
        ordering = ["pk"]
