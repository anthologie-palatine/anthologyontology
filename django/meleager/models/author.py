import logging

from django.contrib.postgres.fields import IntegerRangeField
from django.db import models
from django.urls import reverse

from .abstract import (
    AbstractUniqueIdentifier,
    AbstractDescriptableResource,
    AbstractValidableResource,
    AbstractEditableResource,
    AlternativeURNs,
)
from .language import Language
from .name import Name, name_choices_from_dict


logger = logging.getLogger("console_log")
logger.setLevel(logging.INFO)


class AuthorQuerySet(models.QuerySet):
    def get_name_choices(self):
        return name_choices_from_dict(
            self.filter(names__isnull=False).values(
                "pk", "names__name", "names__language__code2"
            )
        )


class Author(
    AbstractUniqueIdentifier,
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractValidableResource,
    AlternativeURNs,
    models.Model,
):
    """Author model

    Authors are "historical" authors.

    """

    objects = AuthorQuerySet.as_manager()
    tlg_id = models.CharField(max_length=20, null=True, blank=True, db_index=True)

    main_name = models.CharField("Main name", max_length=150, null=True, blank=True)

    names = models.ManyToManyField(
        "meleager.Name",
        related_name="authors",
        help_text="The multilingual names for this object.",
    )

    city_born = models.ForeignKey(
        "meleager.City",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="authors_born_at",
        related_query_name="%(app_label)s_%(class)ss_city_born",
        help_text="The city where this author was born.",
    )

    born_range_year_date = IntegerRangeField(
        null=True, blank=True, help_text="The date range for the author born date."
    )

    city_died = models.ForeignKey(
        "meleager.City",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="authors_died_at",
        help_text="The city where this author died.",
    )

    images = models.ManyToManyField(
        "meleager.Medium",
        related_name="authors",
        help_text="The images associated to this resource.",
        blank=True,
    )

    died_range_year_date = IntegerRangeField(
        null=True, blank=True, help_text="The date range for the author dead date."
    )

    def get_absolute_url(self):
        return reverse("web:author-detail", args=(self.pk,))

    def __str__(self):
        if self.main_name:
            return self.main_name
        first_name = self.names.first()
        if first_name:
            return f"{first_name.name} ({self.pk})"
        else:
            return f"Author PK {self.pk}"

    def save(self, *args, **kwargs):
        if self.pk:
            main_name = self.main_name or ""
            for name in self.names.all():
                if name.language.code == "lat":
                    main_name = name.name
                elif name.name == "Anonimo":
                    main_name = "Anonymous"
            self.main_name = main_name
        return super(Author, self).save(*args, **kwargs)

    def update_names(self, wiki_names):
        languages = Language.objects.preferred()

        # We unlink all previous Names, orphans will be clean up at the end.
        # This strategy is way simpler than dealing with shared Names
        # across other/different Authors and Keywords.
        self.names.clear()

        for code_3, name_wikidata in wiki_names.items():
            language = languages.get(code=code_3)
            name = Name.objects.create(name=name_wikidata.strip(), language=language)
            self.names.add(name)

        Name.objects.filter(
            authors=None, keywords=None, cities=None, keyword_categories=None
        ).delete()

    class Meta:
        ordering = ["main_name", "pk"]
