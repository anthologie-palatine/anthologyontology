from django.db import models

from ..helpers import ValidationLevel, ValidationLevelPermission


class AbstractValidableResource(models.Model):
    """
    Mixin that set validation permission level to a resource.
    """

    validation = models.IntegerField(
        choices=ValidationLevel.choices,
        default=ValidationLevel.VALIDATION_LEVEL_0,
        help_text="The current validation level of this resource.",
    )

    class Meta:
        abstract = True
        permissions = ValidationLevelPermission.choices
