from django.db import models

# from ..description import Description

# use enum functional API to generate language choices, only if it have a iso639_3 representation
# we use iso639_3 to be able to differentiate the ancients greeks


class AbstractDescriptableResource(models.Model):
    """
    Mixin that set a descriptions field to a resource.
    """

    descriptions = models.ManyToManyField(
        "meleager.Description",
        related_name="%(app_label)s_%(class)s",
        related_query_name="%(app_label)s_%(class)ss",
        help_text="The multilingual descriptions for this object.",
    )

    class Meta:
        abstract = True
