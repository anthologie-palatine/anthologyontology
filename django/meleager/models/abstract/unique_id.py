from django.db import models, transaction


class AbstractUniqueIdentifier(models.Model):
    unique_id = models.IntegerField(null=False, blank=True, db_index=True, unique=True)

    def save(self, **kwargs):
        if self.unique_id:
            super().save(**kwargs)
            return

        manager = self.__class__._default_manager
        max_id = manager.select_for_update().aggregate(max=models.Max("unique_id"))["max"]
        with transaction.atomic():
            if max_id is None:
                max_id = 0

            self.unique_id = max_id + 1
            super().save(**kwargs)


    class Meta:
        abstract = True
