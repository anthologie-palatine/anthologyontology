from django.conf import settings
from django.db import models
from django.utils.timezone import now


# Manually vendored from:
# https://github.com/jazzband/django-model-utils/blob/master/model_utils/fields.py


class AutoCreatedField(models.DateTimeField):
    """
    A DateTimeField that automatically populates itself at
    object creation.
    By default, sets editable=False, default=datetime.now.
    """

    def __init__(self, *args, **kwargs):
        kwargs.setdefault("editable", False)
        kwargs.setdefault("default", now)
        super().__init__(*args, **kwargs)


class AutoLastModifiedField(AutoCreatedField):
    """
    A DateTimeField that updates itself on each save() of the model.
    By default, sets editable=False and default=datetime.now.
    """

    def get_default(self):
        """Return the default value for this field."""
        if not hasattr(self, "_default"):
            self._default = self._get_default()
        return self._default

    def pre_save(self, model_instance, add):
        value = now()
        if add:
            current_value = getattr(model_instance, self.attname, self.get_default())
            if current_value != self.get_default():
                # when creating an instance and the modified date is set
                # don't change the value, assume the developer wants that
                # control.
                value = getattr(model_instance, self.attname)
            else:
                for field in model_instance._meta.get_fields():
                    if isinstance(field, AutoCreatedField):
                        value = getattr(model_instance, field.name)
                        break
        setattr(model_instance, self.attname, value)
        return value


class WithCreatorMixin(models.Model):
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_creator",
        related_query_name="%(app_label)s_%(class)ss_creator",
        help_text="The creator of this resource.",
        null=True,
    )

    class Meta:
        abstract = True


class AbstractEditableResource(WithCreatorMixin, models.Model):
    """
    A resource that holds metadata on its edition.
    """

    created_at = AutoCreatedField()
    updated_at = AutoLastModifiedField()

    last_editor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_last_editor",
        related_query_name="%(app_label)s_%(class)ss_last_editor",
        help_text="The last editor of this resource.",
        null=True,
    )

    class Meta:
        abstract = True
