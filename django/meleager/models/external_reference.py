from django.db import models
from .abstract.editable import WithCreatorMixin


class ExternalReference(WithCreatorMixin, models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    url = models.URLField()

    def __str__(self):
        return f"ExtRef: {self.title}"

    def first_related_object(self):
        return self.passages.first()

    def get_absolute_url(self):
        return f"{self.first_related_object().get_absolute_url()}#external-reference-{self.pk}"
