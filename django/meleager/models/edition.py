from django.db import models

from .abstract import AbstractDescriptableResource, AbstractEditableResource


class Edition(AbstractEditableResource, AbstractDescriptableResource, models.Model):
    class EditionType(models.IntegerChoices):
        """ Choices for edition types """

        MANUSCRIPT = 0
        TRANSLATION = 1
        COMMENTARY = 2
        SCHOLIA = 3
        ALIGNMENT = 4

    work = models.ForeignKey(
        "meleager.Work", on_delete=models.CASCADE, related_name="editions"
    )
    edition_type = models.IntegerField(choices=EditionType.choices)
    metadata = models.JSONField("Metadata for Edition", null=False, default=dict)

    def __str__(self):
        first_description = self.descriptions.first()
        if first_description:
            return f"{first_description.description} ({first_description.language})"

        return f"Edition (PK {self.pk})"

    class Meta:
        ordering = ["id"]
