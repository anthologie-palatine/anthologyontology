from django.db import models


class URN(models.Model):
    # example of perseus URN: urn:cts:greekLit:tlg7000.tlg001.perseus-grc2:7.281
    source = models.CharField(max_length=255, blank=True, null=False, default="")
    urn = models.URLField(db_index=True, unique=True)

    def __str__(self):
        return f"{self.urn}"

    def get_relations(self):
        return {
            "authors": self.author_set.all(),
            "cities": self.city_set.all(),
            "keywords": self.keyword_set.all(),
            "keyword_categories": self.keywordcategory_set.all(),
            "passages": self.passage_set.all(),
            "scholia": self.scholium_set.all(),
            "works": self.work_set.all(),
        }
