from django.conf import settings
from django.db import models

from .abstract import AbstractDescriptableResource, AbstractEditableResource


class Editor(AbstractEditableResource, AbstractDescriptableResource, models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
