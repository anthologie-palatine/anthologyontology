import operator
from itertools import groupby

from django.conf import settings
from django.db import models
from django.utils.translation import get_language

from .abstract import AbstractEditableResource


def sort_names_by_languages(items, key="names__language__code2"):
    """Sort names given the order provided in `settings.LANGUAGES`."""
    settings_languages_codes = [lang[0] for lang in settings.LANGUAGES]

    def language_index(item):
        try:
            return settings_languages_codes.index(item.get(key))
        except ValueError:
            # Put unknown languages at the end (highest possible index).
            return len(settings_languages_codes)

    return sorted(items, key=language_index)


def name_choices_from_dict(values):
    """Return a list of tuples from a dictionary

    The dictionary typically comes from .values("pk", "names__name").
    Used to group names/translations together for the Name form field.
    """
    if not values:
        return []

    return sorted(
        [
            (
                pk,
                ", ".join(
                    [item["names__name"] for item in sort_names_by_languages(group)]
                ).strip(),
            )
            for pk, group in groupby(
                sorted(values, key=operator.itemgetter("pk")),
                key=operator.itemgetter("pk"),
            )
        ],
        key=operator.itemgetter(1),
    )


class NameQuerySet(models.QuerySet):
    def get_current_language_name(self):
        """Return the Name with the closest language matching.

        The tricky bit is that the data might be missing so we rely on
        the order of declared LANGUAGES’ setting to determine which one
        to fallback on if that is the case.
        """
        current_language = get_language()
        settings_languages_codes = [lang[0] for lang in settings.LANGUAGES]

        def language_index(name):
            try:
                return settings_languages_codes.index(name.language.code2)
            except ValueError:
                # Put unknown languages at the end (highest possible index).
                return len(settings_languages_codes)

        # First, we sort the available names against the selected languages.
        sorted_names = sorted(self.all(), key=language_index)

        # Then, we iterate to get the current one or fallback on the first
        # available given the LANGUAGES’ setting order.
        selected_name = sorted_names[0]
        for name in sorted_names:
            if name.language.code2 == current_language:
                selected_name = name
                break
        return selected_name


class Name(AbstractEditableResource, models.Model):
    name = models.TextField()
    language = models.ForeignKey(
        "meleager.Language", null=False, default="und", on_delete=models.SET_DEFAULT
    )
    objects = NameQuerySet.as_manager()

    def __str__(self):
        return f"{self.name} ({self.language})"
