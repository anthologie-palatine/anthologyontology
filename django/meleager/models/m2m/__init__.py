from .m2m_passage import AuthorPassage, CityPassage, KeywordPassage, TextPassage
from .m2m_scholium import CityScholium, KeywordScholium

__all__ = [
    "AuthorPassage",
    "CityPassage",
    "TextPassage",
    "KeywordPassage",
    "CityScholium",
    "KeywordScholium",
]
