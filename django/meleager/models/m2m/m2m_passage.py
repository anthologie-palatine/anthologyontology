from django.db import models
from .abstract_m2m import AbstractM2MPassage


class AuthorPassage(AbstractM2MPassage):
    author = models.ForeignKey("meleager.Author", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["author", "passage"],
                name="unique association between author and passage",
            )
        ]


class KeywordPassage(AbstractM2MPassage):
    keyword = models.ForeignKey("meleager.Keyword", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["keyword", "passage"],
                name="unique association between keyword and passage",
            )
        ]


class CityPassage(AbstractM2MPassage):
    city = models.ForeignKey("meleager.City", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["city", "passage"],
                name="unique association between city and passage",
            )
        ]


class TextPassage(AbstractM2MPassage):
    text = models.ForeignKey("meleager.Text", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["text", "passage"],
                name="unique association between text and passage",
            )
        ]
