from django.db import models


class AbstractM2MScholium(models.Model):
    scholium = models.ForeignKey("meleager.Scholium", on_delete=models.CASCADE)
    user = models.ForeignKey("meleager_user.User", on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField("timestamp", auto_now=True)

    class Meta:
        abstract = True


class AbstractM2MPassage(models.Model):
    passage = models.ForeignKey("meleager.Passage", on_delete=models.CASCADE)
    user = models.ForeignKey("meleager_user.User", on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField("timestamp", auto_now=True)

    class Meta:
        abstract = True
