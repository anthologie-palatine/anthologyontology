from django.db import models
from .abstract_m2m import AbstractM2MScholium


# class AuthorScholium(AbstractM2MScholium):
#     author = models.ForeignKey("meleager.Author", on_delete=models.CASCADE)

#     class Meta:
#         constraints = [
#             models.UniqueConstraint(
#                 fields=["author", "scholium"],
#                 name="unique association between author and scholium",
#             )
#         ]


class KeywordScholium(AbstractM2MScholium):
    keyword = models.ForeignKey("meleager.Keyword", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["keyword", "scholium"],
                name="unique association between keyword and scholium",
            )
        ]


class CityScholium(AbstractM2MScholium):
    city = models.ForeignKey("meleager.City", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["city", "scholium"],
                name="unique association between city and scholium",
            )
        ]


# class TextScholium(AbstractM2MScholium):
#     text = models.ForeignKey("meleager.Text", on_delete=models.CASCADE)

#     class Meta:
#         constraints = [
#             models.UniqueConstraint(
#                 fields=["text", "scholium"],
#                 name="unique association between text and scholium",
#             )
#         ]
