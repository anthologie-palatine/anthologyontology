from django.db import models
from django.urls import reverse

from .abstract import (
    AbstractDescriptableResource,
    AbstractEditableResource,
    AbstractUniqueIdentifier,
)


class CommentType(models.TextChoices):
    USER_NOTE = ("user_note", "A note by an user")


class Comment(
    AbstractEditableResource,
    AbstractDescriptableResource,
    AbstractUniqueIdentifier,
    models.Model,
):
    images = models.ManyToManyField(
        "meleager.Medium",
        related_name="comments",
        help_text="The images associated to this resource.",
        blank=True,
    )
    comment_type = models.CharField(
        max_length=20,
        choices=CommentType.choices,
        default=CommentType.USER_NOTE,
        null=False,
        db_index=True,
    )

    def __str__(self):
        return f"Comment (PK {self.pk})"

    def first_related_object(self):
        return self.passages.first() or self.scholia.first()

    def get_absolute_url(self):
        return f"{self.first_related_object().get_absolute_url()}#comment-{self.pk}"
