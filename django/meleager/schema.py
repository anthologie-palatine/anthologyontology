import graphene
from graphene_django import DjangoObjectType, DjangoListField

from meleager.models import (
    Book,
    Passage,
    Scholium,
    City,
    Name,
    Author,
    Work,
    Description,
    Language,
)


class LanguageNode(DjangoObjectType):
    class Meta:
        model = Language


class WorkNode(DjangoObjectType):
    class Meta:
        model = Work


class DescriptionNode(DjangoObjectType):
    class Meta:
        model = Description


class BookNode(DjangoObjectType):
    class Meta:
        model = Book
        # filter_fields = ["id", "number"]
        # interfaces = [relay.Node]


class ScholiumNode(DjangoObjectType):
    class Meta:
        model = Scholium
        # filter_fields = ["id", "number", "passage"]
        # interfaces = [relay.Node]


class PassageNode(DjangoObjectType):
    class Meta:
        model = Passage
        # filter_fields = {
        #     "id": ["exact"],
        #     "fragment": ["exact"],
        #     "sub_fragment": ["exact"],
        #     "mlgr_scholia": ["exact"],
        # }
        # interfaces = [relay.Node]


class CityNode(DjangoObjectType):
    class Meta:
        model = City


class NameNode(DjangoObjectType):
    class Meta:
        model = Name


class AuthorNode(DjangoObjectType):
    class Meta:
        model = Author


class Query(graphene.ObjectType):
    scholia = DjangoListField(ScholiumNode)
    passages = DjangoListField(PassageNode)
    names = DjangoListField(NameNode)
    cities = DjangoListField(CityNode)
    authors = DjangoListField(AuthorNode)


schema = graphene.Schema(query=Query)
