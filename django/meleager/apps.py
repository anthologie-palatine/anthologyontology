from django.apps import AppConfig
from django.db.models.signals import pre_save, m2m_changed

from .signals import add_unique_identifier, update_timestamp_m2m


class MeleagerConfig(AppConfig):
    name = "meleager"
    default_auto_field = "django.db.models.BigAutoField"

    def ready(self):
        # Unique identifiers
        model_names = (
            "City",
            "Author",
            "Text",
            "Passage",
            "Comment",
            "Scholium",
            "Keyword",
        )
        for model in model_names:
            klass = self.get_model(model)
            pre_save.connect(add_unique_identifier, sender=klass)

        TextPassage = self.get_model("Passage").texts.through
        AuthorPassage = self.get_model("Passage").authors.through
        m2m_changed.connect(update_timestamp_m2m, sender=TextPassage)
        m2m_changed.connect(update_timestamp_m2m, sender=AuthorPassage)
