from django.contrib import admin
from reversion_compare.admin import CompareVersionAdmin
from meleager.models import Passage
from meleager.models.m2m import TextPassage


@admin.register(TextPassage)
class TextPassageAdmin(CompareVersionAdmin):
    pass


@admin.register(Passage)
class PassageAdmin(CompareVersionAdmin):
    list_display = ("pk", "get_reference", "get_authors")
    list_display_links = ("pk", "get_reference")
    ordering = ("book__number", "fragment", "sub_fragment")
    list_filter = ("book__number",)
    list_select_related = ("book",)
    search_fields = ("descriptions__description",)
    autocomplete_fields = ("descriptions", "authors", "keywords")
    fields = (
        "descriptions",
        "creator",
        "last_editor",
        "book",
        "fragment",
        "sub_fragment",
        "alternative_urns",
    )

    @admin.display(description="Passage")
    def get_reference(self, obj):
        return f"{obj.book.number}.{obj.fragment}{obj.sub_fragment}"

    @admin.display(description="Authors")
    def get_authors(self, obj):
        # We exclude authors without names, it happens with `anonymous`.
        names = obj.authors.exclude(names__name__isnull=True).values_list(
            "names__name", flat=True
        )
        return ", ".join(names)
