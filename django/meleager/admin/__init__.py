from django.contrib import admin
from reversion_compare.admin import CompareVersionAdmin

from ..models import (
    URN,
    Author,
    Book,
    City,
    Comment,
    Description,
    Edition,
    Editor,
    Keyword,
    KeywordCategory,
    Medium,
    Name,
    Scholium,
    Text,
    ValidationLevelName,
    Work,
)
from .alignment import AlignmentAdmin
from .language import LanguageAdmin
from .passage import PassageAdmin


@admin.register(Author)
class AuthorAdmin(CompareVersionAdmin):
    search_fields = ("names__name",)
    list_display = ("tlg_id", "__str__")
    fields = [
        "tlg_id",
        "main_name",
        "names",
        "city_born",
        "born_range_year_date",
        "city_died",
        "died_range_year_date",
        "images",
    ]
    autocomplete_fields = ["names", "city_born", "city_died", "images"]


@admin.register(City)
class CityAdmin(CompareVersionAdmin):
    search_fields = ("names__name",)
    list_display = ("pk", "__str__")
    autocomplete_fields = ["names", "descriptions"]
    exclude = ["creator", "last_editor", "unique_id"]


@admin.register(Description)
class DescriptionAdmin(CompareVersionAdmin):
    search_fields = ("description",)


@admin.register(Keyword)
class KeywordAdmin(CompareVersionAdmin):
    search_fields = ("names__name",)
    list_display = ("pk", "__str__")
    autocomplete_fields = ["names"]
    exclude = ["creator", "last_editor", "unique_id"]


@admin.register(KeywordCategory)
class KeywordCategoryAdmin(CompareVersionAdmin):
    search_fields = ("names__name",)
    list_display = ("pk", "__str__")
    autocomplete_fields = ["names"]
    exclude = ["creator", "last_editor", "unique_id"]


@admin.register(Name)
class NameAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(Scholium)
class ScholiumAdmin(CompareVersionAdmin):
    list_display = ("pk", "__str__")
    list_select_related = ("passage", "passage__book")
    search_fields = ("descriptions__description",)
    autocomplete_fields = ("descriptions", "keywords")
    fields = (
        "descriptions",
        "creator",
        "last_editor",
        "alternative_urns",
    )


@admin.register(Text)
class TextAdmin(CompareVersionAdmin):
    exclude = ["creator", "last_editor", "unique_id", "validation"]
    search_fields = ["text"]


@admin.register(Medium)
class MediumAdmin(CompareVersionAdmin):
    search_fields = ["title"]


admin.site.register(Comment)
admin.site.register(Book)
admin.site.register(Edition)
admin.site.register(Editor)
admin.site.register(URN)
admin.site.register(ValidationLevelName)
admin.site.register(Work)
