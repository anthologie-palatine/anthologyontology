from django.contrib import admin
from reversion_compare.admin import CompareVersionAdmin
from meleager.models import Scholium


@admin.register(Scholium)
class ScholiumAdmin(CompareVersionAdmin):
    pass
