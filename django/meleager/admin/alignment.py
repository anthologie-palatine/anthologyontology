from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from reversion_compare.admin import CompareVersionAdmin
from meleager.models import Alignment


@admin.register(Alignment)
class AlignmentAdmin(CompareVersionAdmin):
    list_display = ("pk", "text_1", "text_2", "first_passage_link")
    list_select_related = ("text_1", "text_1__language", "text_2", "text_2__language")
    fields = ("first_passage_link", "text_1", "text_1_raw", "text_2", "text_2_raw")
    autocomplete_fields = ["text_1", "text_2"]
    readonly_fields = ["first_passage_link", "text_1_raw", "text_2_raw"]

    @admin.display(description="Passage")
    def first_passage_link(self, obj):
        passage = obj.text_1.passages.select_related("book").first()
        url = reverse(
            f"admin:{passage._meta.app_label}_{passage._meta.model_name}_change",
            args=[passage.id],
        )
        return mark_safe(f'<a href="{url}">{passage}</a>')

    @admin.display(description="Full Text 1 (before modification)")
    def text_1_raw(self, obj):
        return obj.text_1.text

    @admin.display(description="Full Text 2 (before modification)")
    def text_2_raw(self, obj):
        return obj.text_2.text
