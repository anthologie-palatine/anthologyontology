from django.contrib import admin
from meleager.models import Language


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    list_display = ("code", "iso_name", "preferred")
    list_editable = ("preferred",)
    list_filter = ("preferred",)
    search_fields = ("code", "iso_name")
