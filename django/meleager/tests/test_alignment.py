import pytest


@pytest.mark.django_db
class TestAlignment:
    def test_alignment_orphan_from_deleted_text(self, alignment_1, text_2):
        assert alignment_1.text_2 is not None
        assert alignment_1.is_orphan is False
        text_2.delete()
        alignment_1.refresh_from_db()
        assert alignment_1.text_2 is None
        assert alignment_1.is_orphan is True

    def test_alignment_outdated_from_updated_text(self, alignment_1, text_2):
        assert alignment_1.is_outdated is False
        text_2.text = "Contenu du texte mis à jour"
        text_2.save()
        alignment_1.refresh_from_db()
        assert alignment_1.is_outdated is True
