import pytest


@pytest.fixture
@pytest.mark.django_db
def languages():
    from meleager.models import Language

    preferred_languages = (
        ("eng", "en", "English"),
        ("fra", "fr", "French"),
        ("grc", "", "Ancient Greek (to 1453)"),
        ("ita", "it", "Italian"),
        ("por", "pt", "Portuguese"),
        ("spa", "es", "Spanish"),
        ("lat", "", "Latin"),
    )

    for lang in preferred_languages:
        Language.objects.get_or_create(
            code=lang[0], code2=lang[1], iso_name=lang[2], preferred=True
        )


@pytest.fixture
@pytest.mark.django_db
def language_english():
    from meleager.models import Language

    return Language.objects.create(
        code="eng", code2="en", iso_name="English", preferred=True
    )


@pytest.fixture
@pytest.mark.django_db
def language_french():
    from meleager.models import Language

    return Language.objects.create(
        code="fra", code2="fr", iso_name="French", preferred=True
    )


@pytest.fixture
@pytest.mark.django_db
def language_italian():
    from meleager.models import Language

    return Language.objects.create(
        code="ita", code2="it", iso_name="Italian", preferred=True
    )


@pytest.fixture
@pytest.mark.django_db
def superuser(django_user_model):
    superuser = django_user_model.objects.create_superuser(
        "superuser", "superuser@example.com", "superuser"
    )
    return superuser


@pytest.fixture
@pytest.mark.django_db
def editors():
    from django.contrib.auth.models import Group, Permission

    editors = Group.objects.create(name="Editors")
    perm_own = Permission.objects.get(codename="manage_own_mlgr")
    perm_create = Permission.objects.get(codename="can_add_mlgr")
    editors.permissions.add(perm_own, perm_create)
    return editors


@pytest.fixture
@pytest.mark.django_db
def editor(django_user_model, editors):
    editor = django_user_model.objects.create_user(
        "editor", "editor@example.com", "editor"
    )
    editor.groups.add(editors)
    return editor


@pytest.fixture
@pytest.mark.django_db
def regular(django_user_model):
    regular = django_user_model.objects.create_user(
        "regular", "regular@example.com", "regular"
    )
    return regular


@pytest.fixture
@pytest.mark.django_db
def work_1(editor, language_english, language_french):
    from meleager.models import Description, Work

    work = Work.objects.create(creator=editor, last_editor=editor)
    work_descriptions = (
        Description.objects.create(
            description="Work desc FR", language=language_french
        ),
        Description.objects.create(
            description="Work desc EN", language=language_english
        ),
    )
    work.descriptions.add(*work_descriptions)
    return work


@pytest.fixture
@pytest.mark.django_db
def book_1(work_1, language_french):
    from meleager.models import Book, Description

    book = Book.objects.create(work=work_1, number=1)
    book.descriptions.add(
        Description.objects.create(description="Book desc FR", language=language_french)
    )
    return book


@pytest.fixture
@pytest.mark.django_db
def passage_1(book_1):
    from meleager.models import Passage

    passage = Passage.objects.create(book=book_1, fragment=1, unique_id=1)
    return passage


@pytest.fixture
@pytest.mark.django_db
def passage_2(book_1):
    from meleager.models import Passage

    passage = Passage.objects.create(book=book_1, fragment=2, unique_id=2)
    return passage


@pytest.fixture
@pytest.mark.django_db
def scholium_1(passage_1, language_french, editor):
    from meleager.models import Scholium

    scholium = Scholium.objects.create(
        passage=passage_1, number=1, unique_id=1, creator=editor
    )
    passage_1.scholia.add(scholium)
    return scholium


@pytest.fixture
@pytest.mark.django_db
def city_1(passage_1, language_english, language_french):
    from meleager.models import City, Description, Name

    city = City.objects.create(longitude=0, latitude=0, unique_id=1)
    city.names.add(Name.objects.create(name="City name EN", language=language_english))
    city.descriptions.add(
        Description.objects.create(description="City desc FR", language=language_french)
    )

    return city


@pytest.fixture
@pytest.mark.django_db
def author_1(passage_1, city_1, language_english, language_french):
    from meleager.models import Author, Description, Name

    author = Author.objects.create(city_born=city_1, unique_id=1)
    author.names.add(Name.objects.create(name="Author FR", language=language_french))
    author.save()
    author.descriptions.add(
        Description.objects.create(
            description="Author desc EN", language=language_english
        )
    )

    return author


@pytest.fixture
@pytest.mark.django_db
def keyword_category_1(passage_1, language_french, editor):
    from meleager.models import KeywordCategory, Name

    keyword_category = KeywordCategory.objects.create(creator=editor, unique_id=1)
    keyword_category.names.add(
        Name.objects.create(name="KW cat FR", language=language_french)
    )

    return keyword_category


@pytest.fixture
@pytest.mark.django_db
def keyword_1(passage_1, keyword_category_1, language_french, editor):
    from meleager.models import Keyword, Name

    keyword = Keyword.objects.create(creator=editor, category=keyword_category_1)
    keyword.names.add(Name.objects.create(name="Keyword FR", language=language_french))
    return keyword


@pytest.fixture
@pytest.mark.django_db
def comment_1(language_french, editor):
    from meleager.models import Comment, Description

    comment = Comment.objects.create(unique_id=1, creator=editor)
    comment.descriptions.add(
        Description.objects.create(description="Comment FR", language=language_french)
    )
    return comment


@pytest.fixture
@pytest.mark.django_db
def external_reference_1(passage_1, editor):
    from meleager.models import ExternalReference

    external_reference = ExternalReference.objects.create(
        title="ExternalReference content.",
        url="https://example.com",
        creator=editor,
    )
    passage_1.external_references.add(external_reference)
    return external_reference


@pytest.fixture
@pytest.mark.django_db
def internal_reference_1(passage_1, passage_2, editor):
    from meleager.models import PassageInternalReference

    internal_reference = PassageInternalReference.objects.create(
        from_passage=passage_1,
        to_passage=passage_2,
        creator=editor,
    )
    # Symmetrical internal reference.
    PassageInternalReference.objects.create(
        from_passage=passage_2,
        to_passage=passage_1,
        creator=editor,
    )
    return internal_reference


@pytest.fixture
@pytest.mark.django_db
def medium_1(passage_1, editor):
    from meleager.models import Medium

    medium = Medium.objects.create(
        title="Medium content.",
        url="https://example.com",
        type="image",
        creator=editor,
    )
    passage_1.images.add(medium)
    return medium


@pytest.fixture
@pytest.mark.django_db
def text_1(passage_1, language_english, editor):
    from meleager.models import Text

    text = Text.objects.create(
        text="Text content.", language=language_english, unique_id=1, creator=editor
    )
    passage_1.texts.add(text)
    return text


@pytest.fixture
@pytest.mark.django_db
def text_2(passage_1, language_french, editor):
    from meleager.models import Text

    text = Text.objects.create(
        text="Contenu du texte.", language=language_french, unique_id=2, creator=editor
    )
    passage_1.texts.add(text)
    return text


@pytest.fixture
@pytest.mark.django_db
def alignment_1(text_1, text_2, editor):
    from meleager.models import Alignment

    alignment = Alignment.objects.create(
        text_1=text_1,
        text_2=text_2,
        unique_id=1,
        creator=editor,
        alignment_data=[[], []],
    )
    return alignment
