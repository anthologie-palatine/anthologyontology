from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse
from reversion.views import RevisionMixin

from ..models import Passage, Scholium


class MlgrRelatedMixin(RevisionMixin):
    """Related mixin

    Defines all kinds of properties. Example with:
    - related_name = "author"
    - base_name = "passage"
    - action = "create"

    Then:
    - context_base_object_name = "passage"
    - base_form_field = "passage"
    - fk_base = "passage_id"
    - base_pk_url_kwarg = "passage_pk"
    - base_pk = kwargs["passage_pk"]
    - context_object_name = "author"
    - pk_url_kwarg = "author_pk"
    - success_url_anchor_name = "author"
    - fk_related = "author_id"
    - base_field = "authors"
    - submit_view = "web:passage-author-create"
    - context_submit_url = "create_url"
    """

    @property
    def context_object_name(self):
        return self.related_name

    @property
    def base_pk(self):
        return self.kwargs.get(self.base_pk_url_kwarg)

    @property
    def pk_url_kwarg(self):
        return f"{self.related_name}_pk"

    @property
    def base_pk_url_kwarg(self):
        return f"{self.base_name}_pk"

    @property
    def context_base_object_name(self):
        return self.base_name

    @property
    def success_url_anchor_name(self):
        return self.related_name

    @property
    def fk_base(self):
        return f"{self.base_name}_id"

    @property
    def fk_related(self):
        return f"{self.related_name}_id"

    @property
    def base_field(self):
        return self.related_name + "s"

    @property
    def base_form_field(self):
        return self.base_name

    @property
    def submit_view(self):
        return f"web:{self.base_name}-{self.related_name}-{self.action}"

    @property
    def context_submit_url(self):
        return f"{self.action}_url"

    def get_base_object(self):
        """Return the base object from the relation (Passage or Scholium)

        Caches the result using `_base_object`
        """
        if hasattr(self, "_base_object"):
            return self._base_object

        # TODO: catch exceptions
        self._base_object = self.base_model.objects.get(pk=self.base_pk)
        return self._base_object

    def get_context_data(self, **kwargs):
        """Adds context data to the view

        - context["passage"] contains the Passage instance
        - context["create_url"] contains the reversed URL to submit the view
        """
        context = super().get_context_data(**kwargs)
        context[self.context_base_object_name] = self.get_base_object()

        url_kwargs = {self.base_pk_url_kwarg: self.base_pk}
        if hasattr(self, "pk_url_kwarg") and self.kwargs.get(self.pk_url_kwarg):
            url_kwargs[self.pk_url_kwarg] = self.kwargs.get(self.pk_url_kwarg)

        context[self.context_submit_url] = reverse(self.submit_view, kwargs=url_kwargs)

        return context


class PassageRelatedMixin(MlgrRelatedMixin):
    base_name = "passage"
    base_model = Passage


class ScholiumRelatedMixin(MlgrRelatedMixin):
    base_name = "scholium"
    base_model = Scholium


class AddContentPermReqMixin(PermissionRequiredMixin):
    """Mixin class to require users to have the right permissions to create content"""

    permission_required = "meleager.can_add_mlgr"


class MlgrPermissionsMixin(LoginRequiredMixin):
    """Mixin class to require users to own the objects they want to edit, or be an admin editor"""

    def check_user_perm_for_obj(self, user, obj, field="creator"):
        if not user.is_authenticated:
            return False

        if not hasattr(obj, field):
            raise NotImplementedError(f"Instance {obj} does not have a '{field}' field")

        obj_user = getattr(obj, field)
        if user.has_perm("meleager.manage_all_mlgr") or user == obj_user:
            return True

        return False
