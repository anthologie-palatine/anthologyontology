

import csv  # pas sûr que cela soit nécessaire
import io
import urllib.request

from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, Keyword, Language, KeywordCategory, Name, Passage, City
from meleager_user.models import User
from meleager.helpers.wikidata import get_wiki_names_from_url

def keywordsdicts():
    url = "http://docs.google.com/feeds/download/spreadsheets/Export?key=13X6Im1LORf-RQ2Fk9j3q2nyPxOdrp_ME5N4SSaaCk-A&exportFormat=csv"
    webpage = urllib.request.urlopen(url)
    datareader = csv.DictReader(io.TextIOWrapper(webpage))
    return datareader

def associatenames(my_object, wikiurn):
    object_names=get_wiki_names_from_url(wikiurn)
    for lang, object_name in object_names.items():
        print(lang)
        language = Language.objects.get(code=str(lang))
        my_name = Name.objects.create(name=object_name, language=language)
        my_object.names.add(my_name)
    # s'il y en a sans nom il faudra debugguer    
    # if len(keyword_names) < 1:
    #    print('No name!')
    #    my_keyword.names.add(no_name)

def associatepassages(my_object,my_epigrams):
    epigrams_list=[epigram.strip() for epigram in my_epigrams.split(',')]
    for epigram in epigrams_list:
        my_epigram=Passage.objects.get(book__number='9', fragment=epigram, sub_fragment='')

        print(my_epigram)
        if isinstance(my_object, City):
            my_epigram.cities.add(my_object)
        elif isinstance(my_object, Keyword):
            my_epigram.keywords.add(my_object)

class Command(BaseCommand):
    help = "Bash import keywords from google doc"




    def handle(self, *args, **options):
        no_name_lang=Language.objects.get(code='eng')
        no_name = Name.objects.create(name='No name', language=no_name_lang) 
        jack = User.objects.get(pk='91')
        for keyword in keywordsdicts():
            print('Treating '+keyword['Wikidata Entry'])
            urn = keyword['Wikidata Entry']
            my_urn, created = URN.objects.get_or_create(urn=urn, defaults={'source':'wikidata'})
            if keyword['place'] == '1':
                print('It is a place')
                my_city, created = City.objects.get_or_create(alternative_urns__urn=my_urn.urn, defaults={'creator':jack})

                if created:
                    my_city.alternative_urns.add(my_urn)
                associatenames(my_city,urn)
                associatepassages(my_city,keyword['epigram'])
            else:
                my_keyword, created = Keyword.objects.get_or_create(alternative_urns__urn=my_urn.urn, defaults={'creator':jack})
                if created:
                    my_keyword.alternative_urns.add(my_urn)
                my_keyword.category=KeywordCategory.objects.get(names__name=keyword['category'])
                my_keyword.save()
                associatenames(my_keyword,urn)
                associatepassages(my_keyword,keyword['epigram'])
