import csv  # pas sûr que cela soit nécessaire
import io
import urllib.request

from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, Keyword

CATEGORY_LIST= ['divinities','citedPoets','epitheteEpiclese','famousCharacters','genres','metric','mythicCharacters','period']

def keywordsreader(category):

    url = 'https://gitlab.huma-num.fr/ecrinum/anthologia/dataap/-/raw/master/data/keywords/'+category+'.csv'
    webpage = urllib.request.urlopen(url)
    datareader = csv.DictReader(io.TextIOWrapper(webpage))
    return datareader


class Command(BaseCommand):
    help = "Add wikidata URN to existing keywords"


    def add_arguments(self, parser):
       parser.add_argument('--category')

    def handle(self, *args, **options):
         
        category = options['category']
        if category:
            category_list=[category]
        else:
            category_list = CATEGORY_LIST
            
        
        errors_list=[]
        no_wikidata=[]
        for category in category_list:
            for keyword_row in keywordsreader(category):
                keyword_id = keyword_row['id']
                print('# Treating:\n')
                print(keyword_row['names'])
                print(keyword_id)
                if not keyword_id:
                    continue
                try:
                    keyword = Keyword.objects.get(unique_id=str(keyword_id))
                except Keyword.DoesNotExist:
                    print('This kw does not exist')
                    errors_list.append(keyword_row)
                    continue
                if keyword_row['wikidata'] not in ['','-']:
                    print(keyword_row['wikidata'])
                   
                    print('Adding wikidata: ')
                    urn_wiki, created = URN.objects.get_or_create(urn=keyword_row["wikidata"])
                    urn_wiki.source='Wikidata'
                    urn_wiki.save()
                    urn_wiki.keyword_set.add(keyword)
                    print('Wikidata added: '+keyword_row['wikidata']+'\n')
                    if not created:
                        print(keyword_row['wikidata']+' already exists')
                    print('Added wikidata urn')
                else:
                    print('NO WIKIDATA!!!!')
                    no_wikidata.append(keyword_row)
                if category == 'citedPoets':    
                    if keyword_row['TLG no.'] not in ['','-']:
                        print('tlg-'+keyword_row['TLG no.'].replace(' ', ''))
                        tlg = keyword_row['TLG no.'].replace(' ', '')
                        tlgurn = 'http://data.perseus.org/catalog/urn:cts:greekLit:tlg'+tlg
                        urn, created = URN.objects.get_or_create(source='tlg', urn = tlgurn)                                                                
                        urn.keyword_set.add(keyword)
                    else:
                        print('NO TLG!!!!')
        print(errors_list)    
