import logging
import codecs
import csv
from contextlib import closing

import requests
from django.core.management.base import BaseCommand
from meleager.helpers.wikidata import get_wiki_names_from_url


class Command(BaseCommand):
    help = "Update authors’ objects from distant CSV."

    def add_arguments(self, parser):
        parser.add_argument("--url")

    def handle(self, *args, **options):
        from meleager.models import Author, URN

        logger = logging.getLogger("console_log")
        verbosity = int(options["verbosity"])
        if verbosity > 1:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        logger.debug("Command import authors from CSV starting")

        csv_url = options["url"]

        with closing(requests.get(csv_url, stream=True)) as response:
            csv_content = codecs.iterdecode(response.iter_lines(), "utf-8")
            authors_reader = csv.DictReader(csv_content, delimiter=";")

            for csv_author in authors_reader:
                author_id = csv_author["id"].strip()
                action = csv_author["action"].strip()
                logger.info(f"Processing author {author_id} ({action})")
                tlg_id = csv_author["tlg_id"].strip()
                wikidata = csv_author["wikidata"].strip()
                perseus = csv_author["url_tlg"].strip()
                try:
                    author = Author.objects.get(pk=author_id)
                # The ValueError happens if author_id == ''
                except (Author.DoesNotExist, ValueError):
                    author = None
                if action == "D":
                    if author is None:
                        # We do not have this Author in the database.
                        print(f"WARNING: author to delete is missing: {author_id}")
                        continue
                    if author.passages.exists():
                        print(f"ERROR: author to delete has passages: {author.pk}")
                    else:
                        author.delete()
                else:
                    if action == "C":
                        author = Author.objects.create()

                    author.tlg_id = tlg_id

                    if wikidata:
                        wikidata_urn, created = URN.objects.get_or_create(urn=wikidata)
                        wikidata_urn.source = "wikidata"
                        wikidata_urn.save()
                        author.alternative_urns.add(wikidata_urn)

                    if perseus:
                        perseus_urn, created = URN.objects.get_or_create(urn=perseus)
                        perseus_urn.source = "perseus"
                        perseus_urn.save()
                        author.alternative_urns.add(perseus_urn)

                    for alternative_urn in author.alternative_urns.all():
                        if alternative_urn.source == "wikidata":
                            if alternative_urn.urn.strip() != wikidata:
                                author.alternative_urns.remove(alternative_urn)
                        elif alternative_urn.source == "perseus":
                            if alternative_urn.urn.strip() != perseus:
                                author.alternative_urns.remove(alternative_urn)

                    if wikidata:
                        wiki_names = get_wiki_names_from_url(wikidata)
                        author.update_names(wiki_names)

                    author.save()
