from django.core.management.base import BaseCommand
from django.db.models import Q

from meleager.models import City
from meleager.helpers.wikidata import get_coordinates_from_url


class Command(BaseCommand):
    help = "Load GPS coordinates from wikidata and updates cities in the DB"

    def handle(self, *args, **options):
        # Find cities in the DB that are missing GPS data
        cities = City.objects.filter(Q(longitude=None) | Q(latitude=None))

        updated = 0

        for city in cities:
            # Find the alternative URL for wikidata
            urns = city.alternative_urns.filter(urn__contains="wikidata.org")

            if urns:
                # The Wiki ID is the last part of the URL
                coords = get_coordinates_from_url(urns.first().urn)
                if not coords:
                    self.stdout.write(
                        self.style.NOTICE(
                            f"City with PK {city.pk} ({city.names.first().name}) has no coordinates: {urns.first()}"
                        )
                    )
                    continue

                city.latitude = coords["latitude"]
                city.longitude = coords["longitude"]
                # self.stdout.write(self.style.SUCCESS("."), ending="")
                city.save()
                updated = updated + 1

        self.stdout.write(self.style.SUCCESS(f"\n\n{updated} cities updated."))
