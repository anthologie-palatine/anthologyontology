from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Check orphans objects in the database (no deletion)."

    def handle(self, *args, **options):
        from meleager.models import Description, Name, URN

        self.stdout.write(
            self.style.SUCCESS("\nChecking orphans objects in the database…")
        )

        orphan_urns = (
            URN.objects.filter(author__isnull=True)
            .filter(city__isnull=True)
            .filter(keyword__isnull=True)
            .filter(keywordcategory__isnull=True)
            .filter(passage__isnull=True)
            .filter(scholium__isnull=True)
            .filter(work__isnull=True)
        )
        orphan_urns_count = orphan_urns.count()

        if orphan_urns_count:
            self.stdout.write(self.style.WARNING(f"\n{orphan_urns_count} orphan URNs:"))
            for orphan_urn in orphan_urns:
                self.stdout.write(f"{orphan_urn.pk} {orphan_urn.urn}")
        else:
            self.stdout.write(self.style.SUCCESS("\nNo orphan URN so far."))

        orphan_names = (
            Name.objects.filter(authors__isnull=True)
            .filter(cities__isnull=True)
            .filter(keywords__isnull=True)
            .filter(keyword_categories__isnull=True)
        )
        orphan_names_count = orphan_names.count()

        if orphan_names_count:
            self.stdout.write(
                self.style.WARNING(f"\n{orphan_names_count} orphan Names:")
            )
            for orphan_name in orphan_names:
                self.stdout.write(f"{orphan_name.pk} {orphan_name.name}")
        else:
            self.stdout.write(self.style.SUCCESS("\nNo orphan Name so far."))

        orphan_descriptions = (
            Description.objects.filter(meleager_authors__isnull=True)
            .filter(meleager_books__isnull=True)
            .filter(meleager_citys__isnull=True)
            .filter(meleager_comments__isnull=True)
            .filter(meleager_editions__isnull=True)
            .filter(meleager_editors__isnull=True)
            .filter(meleager_manuscripts__isnull=True)
            .filter(meleager_mediums__isnull=True)
            .filter(meleager_passages__isnull=True)
            .filter(meleager_scholiums__isnull=True)
            .filter(works__isnull=True)
        )
        orphan_descriptions_count = orphan_descriptions.count()

        if orphan_descriptions_count:
            self.stdout.write(
                self.style.WARNING(
                    f"\n{orphan_descriptions_count} orphan Descriptions:"
                )
            )
            for orphan_description in orphan_descriptions:
                self.stdout.write(
                    f"{orphan_description.pk} {orphan_description.description}"
                )
        else:
            self.stdout.write(self.style.SUCCESS("\nNo orphan Description so far."))

        self.stdout.write(self.style.SUCCESS("\n\nDone."))
