import csv  # pas sûr que cela soit nécessaire
import io
import urllib.request

from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, City

def citiesreader():
    url = "https://gitlab.huma-num.fr/ecrinum/anthologia/dataap/-/raw/master/data/cities_manual.csv"
    webpage = urllib.request.urlopen(url)
    datareader = csv.DictReader(io.TextIOWrapper(webpage))
    return datareader


class Command(BaseCommand):
    help = "Add wikidata and pleiades URN to existing cities"


    def handle(self, *args, **options):
        #URN.objects.all().delete()#à effacer
        errors_list=[]
        for city_row in citiesreader():
            print('Treating '+city_row['names'])
            try:
                city = City.objects.get(unique_id=city_row["id"])
            except City.DoesNotExist:
                print('This city does not exist in the db')
                errors_list.append(city_row)
                continue

            print('Adding wikidata: ')
            urn_wiki, created = URN.objects.get_or_create(source="Wikidata", urn=city_row["wikidata"])
            urn_wiki.city_set.add(city)
            print('Wikidata added: '+city_row['wikidata']+'\n')
            if not created:
                print(city_row['wikidata']+' already exists')
            
            
            if city_row['pleiades'] != 'None':
                print('Treating pleiades: ')
                urn_pleiades, created = URN.objects.get_or_create(
                    source="Pleiades",
                    urn="https://pleiades.stoa.org/places/" + city_row["pleiades"],
                )
                urn_pleiades.city_set.add(city)
                print('Pleiades added'+city_row['pleiades']+'\n')
                if not created:
                    print('Pleiades URN already exists')
        print(errors_list)            
