from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.core.management import call_command
from django.core.management.base import BaseCommand
from meleager.models import (
    Author,
    AuthorPassage,
    Book,
    City,
    CityPassage,
    KeywordCategory,
    CityScholium,
    Comment,
    Description,
    Keyword,
    KeywordPassage,
    KeywordScholium,
    Language,
    Name,
    Passage,
    Scholium,
    Work,
    ExternalReference,
    Text,
    TextPassage,
)
from meleager_user.models import User
import reversion


class Command(BaseCommand):
    help = "Inserts dummy content in the database"

    def handle(self, *args, **options):
        call_command("flush", "--noinput")
        fixture_file = settings.BASE_DIR / "meleager" / "fixtures" / "test_data.json"
        with fixture_file.open("w") as fp:
            create_fixtures()
            create_reversion_fixtures()
            call_command(
                "dumpdata", "meleager", "auth", "meleager_user", "--indent=2", stdout=fp
            )


def create_fixtures():
    # CREATE LANGUAGES
    Language.objects.all().delete()
    languages = (
        ("eng", "en", "English"),
        ("fra", "fr", "French"),
        ("grc", "", "Ancient Greek (to 1453)"),
        ("ita", "it", "Italian"),
        ("por", "pt", "Portuguese"),
        ("spa", "es", "Spanish"),
        ("lat", "", "Latin"),
    )

    for lang in languages:
        Language.objects.create(
            code=lang[0], code2=lang[1], iso_name=lang[2], preferred=True
        )

    # GROUPS AND PERMISSIONS
    editors, _ = Group.objects.get_or_create(name="Editors")
    admin_editors, _ = Group.objects.get_or_create(name="Admin editors")
    perm_own = Permission.objects.get(codename="manage_own_mlgr")
    perm_all = Permission.objects.get(codename="manage_all_mlgr")
    perm_create = Permission.objects.get(codename="can_add_mlgr")

    editors.permissions.add(perm_own, perm_create)
    admin_editors.permissions.add(perm_all, perm_create)

    User.objects.all().delete()
    superuser = User.objects.create_user(
        "superuser", "admin@admin.com", "superuser", is_superuser=True, is_staff=True
    )
    admin_editor = User.objects.create_user(
        "editor_admin", "editor@admin.com", "editor_admin"
    )
    admin_editor.groups.add(admin_editors)
    editor = User.objects.create_user("editor", "editor@editor.com", "editor")
    editor.groups.add(editors)

    # WORK
    work = Work.objects.create(creator=editor, last_editor=editor)
    work_descriptions = (
        Description.objects.create(description="Work desc FR", language_id="fra"),
        Description.objects.create(description="Work desc EN", language_id="eng"),
    )
    work.descriptions.add(*work_descriptions)

    # BOOK
    book = Book.objects.create(work=work, number=42)
    book.descriptions.add(
        Description.objects.create(description="Book desc GRC", language_id="grc")
    )

    # PASSAGE
    passage = Passage.objects.create(book=book, fragment=12, unique_id=66666)
    passage.descriptions.add(
        Description.objects.create(description="Passage 1 desc FR", language_id="fra")
    )

    # CITY
    city = City.objects.create(longitude=42, latitude=-12, unique_id=12345)
    city.names.add(Name.objects.create(name="City name EN", language_id="eng"))
    city.descriptions.add(
        Description.objects.create(description="City desc FR", language_id="eng")
    )
    CityPassage.objects.create(
        city_id=city.pk, passage_id=passage.pk, user_id=editor.pk
    )

    # EXTERNAL REFERENCE
    extref = ExternalReference.objects.create(
        title="External ref", url="https://perdu.com", creator=editor
    )
    passage.external_references.add(extref)

    # AUTHOR
    author = Author.objects.create(city_born=city, unique_id=22222)
    author.names.add(Name.objects.create(name="Author ITA", language_id="ita"))
    author.save()
    author.descriptions.add(
        Description.objects.create(description="Author desc EN", language_id="eng")
    )
    AuthorPassage.objects.create(
        user=editor, passage_id=passage.pk, author_id=author.pk
    )

    # KEYWORDS / CATEGORIES
    kw_cat1 = KeywordCategory.objects.create(creator=editor, unique_id=1234)
    kw_cat1.names.add(Name.objects.create(name="CAT1", language_id="spa"))
    kw_cat2 = KeywordCategory.objects.create(creator=editor, unique_id=5678)
    kw_cat2.names.add(Name.objects.create(name="CAT2", language_id="eng"))

    kw = Keyword.objects.create(creator=editor, category=kw_cat1)
    kw.names.add(Name.objects.create(name="Keyword FR", language_id="fra"))
    KeywordPassage.objects.create(passage=passage, keyword=kw, user=editor)

    kw2 = Keyword.objects.create(creator=editor, category=kw_cat2)
    kw2.names.add(Name.objects.create(name="Keyword SCHOLIUM", language_id="lat"))

    # COMMENT
    comment = Comment.objects.create(unique_id=333333)
    comment.descriptions.add(
        Description.objects.create(description="Comment passage FR", language_id="fra")
    )
    passage.comments.add(comment)

    passage2 = Passage.objects.create(
        book=book, fragment=69, sub_fragment="abc", unique_id=444444
    )
    passage2.descriptions.add(
        Description.objects.create(description="Passage 2 desc EN", language_id="eng")
    )
    passage2.authors.add(author)
    passage2.cities.add(city)

    # SCHOLIUM
    scholium = Scholium.objects.create(
        passage=passage2, number=666, unique_id=777777, creator=editor
    )
    scholium.descriptions.add(
        Description.objects.create(description="Scholium desc GRC", language_id="grc")
    )

    passage.scholia.add(scholium)
    scholium.comments.add(comment)

    CityScholium.objects.create(
        city_id=city.pk, scholium_id=scholium.pk, user_id=editor.pk
    )

    KeywordScholium.objects.create(
        keyword_id=kw2.pk, scholium_id=scholium.pk, user_id=editor.pk
    )

    all_updates = [
        (Passage, "creator"),
        (City, "creator"),
        (Keyword, "creator"),
        (Description, "creator"),
        (Comment, "creator"),
        (AuthorPassage, "user"),
        (KeywordPassage, "user"),
        (CityPassage, "user"),
        (CityScholium, "user"),
    ]
    # Everything is created by "editor"
    for (model, field) in all_updates:
        args = {field: editor}
        model.objects.all().update(**args)


def create_reversion_fixtures():
    passage = Passage.objects.get(pk=1)
    editor = User.objects.get(username="editor")
    with reversion.create_revision():
        text = Text.objects.create(text="New text added", language_id="eng")
        TextPassage.objects.create(text=text, user=editor, passage=passage)
        reversion.add_to_revision(passage)
        reversion.set_user(editor)
        reversion.set_comment("Added through make_fixtures")

    with reversion.create_revision():
        text = Text.objects.create(text="Nouveau text - FR", language_id="fra")
        TextPassage.objects.create(text=text, user=editor, passage=passage)
        reversion.add_to_revision(passage)
        reversion.set_user(editor)
        reversion.set_comment("Added through make_fixtures (change 2)")
