import requests
from django.core.management.base import BaseCommand

from meleager.helpers.wikidata import get_wiki_names_from_url


class Command(BaseCommand):
    help = "Update authors’ objects from wikidata URN."

    def handle(self, *args, **options):
        from meleager.models import Author

        self.stdout.write(
            self.style.SUCCESS("Command import Authors from wikidata starting…")
        )

        updated = 0

        for author in Author.objects.all():
            # Find the alternative URL for wikidata
            urns = author.alternative_urns.filter(urn__contains="wikidata.org")

            if urns:
                wikidata_urn = urns.first().urn
                try:
                    wiki_names = get_wiki_names_from_url(wikidata_urn)
                except requests.exceptions.HTTPError:
                    self.stdout.write(
                        self.style.WARNING(f"{wikidata_urn} does not exist (anymore?)")
                    )
                    continue
                if not wiki_names:
                    self.stdout.write(
                        self.style.NOTICE(
                            (
                                f"Author with PK {author.pk} ({author}) "
                                f"has no wiki names in wikidata: {wikidata_urn}"
                            )
                        )
                    )
                    continue

                author.update_names(wiki_names)
                author.save()
                updated = updated + 1

        self.stdout.write(self.style.SUCCESS(f"\n\n{updated} authors updated."))
