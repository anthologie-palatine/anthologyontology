from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import Scholium


def title_tuple2str(title):
    title = tuple(map(str, title))
    return ".".join((title[0], "".join((title[1], title[2])), title[3]))


def merge(scholium1, scholium2):
    if scholium1.passage.pk != scholium2.passage.pk:
        raise RuntimeError(
            f"Scholium {scholium1.pk} and {scholium2.pk} do not link to the same passage!"
        )

    relations = [
        "cities",
        "texts",
        "manuscripts",
        "images",
        "comments",
        "keywords",
        "alternative_urns",
        "descriptions",
    ]

    for relation in relations:
        objects = getattr(scholium2, relation).all()
        getattr(scholium1, relation).add(*objects)


def find_duplicated_scholia(keys):
    """
    Retrieves duplicates by looking at their titles (book number + fragment + ...)
    Returns a dictionary:
        keys are the titles
        values are the Scholium instances
    """
    all_titles = Scholium.objects.values_list(*keys)
    seen = set()
    dups = set()

    for scholium in all_titles:
        if scholium not in seen:
            seen.add(scholium)
        else:
            dups.add(scholium)

    # No duplicates - stop here
    if not len(dups):
        return False

    def tuple2dict(tup):
        return dict(zip(keys, [tup[index] for index in range(len(keys))]))

    dup_objs = {
        duplicate: Scholium.objects.filter(**tuple2dict(duplicate))
        for duplicate in dups
    }

    return dup_objs


def print_dups(dup_objs, full=False):
    """ self.stdout.write PKs for each key (title) in the dictionary """
    if not full:
        print(", ".join([title_tuple2str(title) for title in dup_objs.keys()]))
    else:
        for title, objects in dup_objs.items():
            print("-", title_tuple2str(title))
            for obj in objects:
                print("  -> PK", obj.pk)


class Command(BaseCommand):
    help = "Checks for duplicate scholia"

    keys = (
        "passage__book__number",
        "passage__fragment",
        "passage__sub_fragment",
        "number",
    )

    def handle(self, *args, **options):
        duplicated = find_duplicated_scholia(self.keys)

        if not duplicated:
            self.stdout.write(self.style.SUCCESS("No duplicate scholia found!"))
            return None

        self.stdout.write(self.style.WARNING("Duplicate scholia found!"))
        print_dups(duplicated)

        for title, objects in duplicated.items():
            self.stdout.write(
                colorize(f"Looking at scholia for {title_tuple2str(title)}:", fg="cyan")
            )
            base_scholium, *others = objects
            for other_scholium in others:
                self.stdout.write(
                    self.style.NOTICE(
                        f"- Merging scholium {base_scholium.pk} with {other_scholium.pk}..."
                    )
                )
                merge(base_scholium, other_scholium)
                self.stdout.write(
                    self.style.NOTICE(f"- Deleting scholium {other_scholium.pk}...")
                )
                other_scholium.delete()
