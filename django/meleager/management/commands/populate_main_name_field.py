from django.core.management.base import BaseCommand
from meleager.models import Author


class Command(BaseCommand):

    def handle(self, *args, **options):
        authors = Author.objects.all()
        for author in authors:
            author.save()
