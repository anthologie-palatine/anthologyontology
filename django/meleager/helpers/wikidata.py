import re

import requests

from django.apps import apps

WIKIDATA_ENTITY_URL_REGEXP = r"https?://.+\.?wikidata.org/wiki/(?P<wiki_id>Q\d+)"


def get_wiki_id_from_url(wiki_url):
    matches = re.match(WIKIDATA_ENTITY_URL_REGEXP, wiki_url, flags=re.I)

    if not matches:
        return None

    return matches["wiki_id"]


def get_coordinates_from_url(wiki_url):
    """Fetches information from wikidata, the URL is supposed to be of a "place"

    Returns a dictionary for the coordinates of the place, or None if something went wrong
    """

    wiki_id = get_wiki_id_from_url(wiki_url)

    wikidata_request = requests.get(
        "https://www.wikidata.org/w/api.php",
        params={
            "action": "wbgetentities",
            "ids": wiki_id,
            "format": "json",
        },
    )
    wikidata_request.raise_for_status()
    wikidata = wikidata_request.json()

    # This is where the GPS coordinates are
    coords_data = wikidata["entities"][wiki_id]["claims"].get("P625")
    if not coords_data:
        return None

    coords_data = coords_data[0]["mainsnak"]["datavalue"]["value"]
    return {"latitude": coords_data["latitude"], "longitude": coords_data["longitude"]}


def get_wiki_names_from_url(wiki_url):
    """Fetches information from wikidata, to be used in a form / during model creation

    Returns a dictionary:
    {<3-letter language code>: <keyword name>}
    """

    wiki_id = get_wiki_id_from_url(wiki_url)
    if not wiki_id:
        print(f"WARNING: wiki_id cannot be extracted from {wiki_url}")
        return {}

    wiki_url = f"https://www.wikidata.org/wiki/Special:EntityData/{wiki_id}.json"
    resp = requests.get(wiki_url)
    resp.raise_for_status()
    # Warning, wikidata redirections are not handled at the API level.
    # For instance https://www.wikidata.org/wiki/Special:EntityData/Q97620996.json
    # contains the wiki_id Q12877290 (the frontend does redirect though).
    data = resp.json()["entities"].get(wiki_id)
    if data is None:
        print(f"WARNING: wiki_id {wiki_id} cannot be found in {wiki_url}")
        return {}

    Language = apps.get_model("meleager.Language")
    preferred = Language.objects.preferred()
    code2_preferred = preferred.exclude(code2="").values_list("code2", flat=True)

    names = {
        preferred.get(code2=lang["language"]).code: lang["value"]
        for lang in data["labels"].values()
        if lang["language"] in code2_preferred
    }

    # Ancient Greek is a special case. There is no 2-letters code for it.
    # By default we look for the `grc` key in languages (labels) but
    # we fallback to the `P1559` claim if present.
    grc_name = data["labels"].get("grc", {}).get("value")
    if grc_name:
        names["grc"] = grc_name
    else:
        try:
            claim = data["claims"]["P1559"][0]["mainsnak"]["datavalue"]["value"]
            if claim["language"] == "grc":
                names["grc"] = claim["text"]
        except KeyError:
            # No ancient Greek here
            pass

    return names
