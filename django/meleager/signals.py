from django.db.models import Max
from django.utils.timezone import now as tznow


def add_unique_identifier(sender, instance, **kwargs):
    if not instance.unique_id:
        max_value = sender.objects.aggregate(max=Max("unique_id"))["max"]
        if not max_value:
            max_value = 0

        instance.unique_id = max_value + 1


def update_timestamp_m2m(sender, instance, action, reverse, **kwargs):
    if action in ("post_add", "post_remove", "post_clear"):
        if reverse:
            instance.passages.update(updated_at=tznow())
        else:
            instance.updated_at = tznow()
            instance.save()
