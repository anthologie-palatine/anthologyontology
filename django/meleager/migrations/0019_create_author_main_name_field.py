from django.db import migrations, models
from django.conf import settings


def populate_main_name_field(apps, schema_editor):
    Author = apps.get_model("meleager", "Author")
    authors = Author.objects.all()
    for author in authors:
        main_name =  ''
        for name in author.names.all():
            if name.language.code == 'eng':
                main_name = name.name
            elif name.name == 'Anonimo':
                main_name = 'Anonymous'
        author.main_name = main_name
        author.save()


class Migration(migrations.Migration):

    dependencies = [
        ("meleager", "0018_auto_20211114_1710"),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='main_name',
            field=models.CharField('Main name', max_length=150, blank=True, null=True),
        ),

        migrations.RunPython(populate_main_name_field, reverse_code=migrations.RunPython.noop),
    ]
