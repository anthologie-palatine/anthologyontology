from django.db import migrations, models
from iso639 import languages


def add_2_letter_code(apps, schema_editor):
    Language = apps.get_model("meleager", "Language")
    for lang in list(languages):
        Language.objects.filter(code=lang.part3).update(code2=lang.part1)


class Migration(migrations.Migration):

    dependencies = [
        ('meleager', '0015_auto_20210618_0041'),
    ]

    operations = [
        migrations.AddField(
            model_name='language',
            name='code2',
            field=models.CharField(db_index=True, default='', max_length=2, verbose_name='Language code (ISO639) - 2 letters'),
        ),
        migrations.RunPython(add_2_letter_code, reverse_code=migrations.RunPython.noop),
    ]
