from django import forms
from meleager.models import Text

from .fields import PreferredLanguageField


class TextForm(forms.ModelForm):
    language = PreferredLanguageField()

    class Meta:
        model = Text
        fields = ("text", "language", "edition")
