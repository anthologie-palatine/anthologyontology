from django_select2 import forms as s2forms

from django import forms

from ..models import Description
from .fields import PreferredLanguageField


class DescriptionWidget(s2forms.ModelSelect2MultipleWidget):
    search_fields = ["description__icontains"]


class DescriptionForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea(attrs={"rows": 2, "cols": 40}))
    language = PreferredLanguageField()

    class Meta:
        model = Description
        fields = ("description", "language")
