from django import forms
from meleager.models import ExternalReference, Medium

from .fields import MlgrCityField, MlgrKeywordField, MlgrNameField


class RelatedForm(forms.Form):
    """
    ! WARNING !
    Expects a `queryset` keyword argument - from which it derives the choices for the field.
    """

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop("queryset")
        next_url = kwargs.pop("next_url", "")
        super().__init__(*args, **kwargs)
        for (field_name, field_class) in self.Meta.fields:
            self.fields[field_name] = field_class(queryset=queryset, next_url=next_url)


class AuthorForm(RelatedForm):
    """Form to associate an author"""

    class Meta:
        fields = (("author", MlgrNameField),)


class CityForm(RelatedForm):
    """Form to associate a city"""

    class Meta:
        fields = (("city", MlgrCityField),)


class KeywordForm(RelatedForm):
    """Form to associate a keyword"""

    class Meta:
        fields = (("keyword", MlgrKeywordField),)


class ExternalReferenceForm(forms.ModelForm):
    class Meta:
        model = ExternalReference
        fields = ["title", "url"]


class MediumForm(forms.ModelForm):
    class Meta:
        model = Medium
        fields = ["title", "url", "type"]
