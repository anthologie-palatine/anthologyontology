from urllib.parse import urlencode
from django.forms import ModelChoiceField
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from meleager.models import Language


class PreferredLanguageField(ModelChoiceField):
    """Custom field to display only preferred languages"""

    def __init__(self, *args, **kwargs):
        super().__init__(queryset=Language.objects.preferred())


class MlgrNameField(ModelChoiceField):
    """Custom field to display names with translations, neatly organized

    It creates the `choices` from the queryset keyword argument.
    """

    def __init__(self, queryset, next_url, **kwargs):
        super().__init__(queryset, **kwargs)
        self.choices = [("", "")] + queryset.get_name_choices()


class MlgrCityField(ModelChoiceField):
    """Custom field to display names with translations, neatly organized

    It creates the `choices` from the queryset keyword argument.
    """

    def __init__(self, queryset, next_url, **kwargs):
        super().__init__(queryset, **kwargs)
        create_url = reverse("web:city-create-wikidata")
        url_params = urlencode({"next": next_url})
        self.help_text = mark_safe(
            _(
                "If you cannot find a City entry that suits you, "
                f'you can <a href="{create_url}?{url_params}">+ create a new one</a>.'
            )
        )
        self.choices = [("", "")] + queryset.get_name_choices()


class MlgrKeywordField(ModelChoiceField):
    """Custom field to display keywords with translations, neatly organized into categories

    It creates the `choices` from the queryset keyword argument.
    """

    def __init__(self, queryset, next_url, **kwargs):
        super().__init__(queryset, **kwargs)
        create_url = reverse("web:keyword-create-wikidata")
        url_params = urlencode({"next": next_url})
        self.help_text = mark_safe(
            _(
                "If you cannot find a Keyword entry that suits you, "
                f'you can <a href="{create_url}?{url_params}">+ create a new one</a>.'
            )
        )
        self.choices = [("", "")] + queryset.get_keyword_choices()
