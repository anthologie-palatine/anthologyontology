from urllib.parse import parse_qsl

from django import template
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ngettext, gettext as _
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape

register = template.Library()


@register.filter
def convert_revision_comment(text, version_object=None):
    ACTION_TO_LABEL = {
        "create": _("Creation of"),
        "add": _("Addition of"),
        "associate": _("Association of"),
        "modify": _("Modification of"),
        "delete": _("Removal of"),
        "deassociate": _("Removal of"),
        "initial revision": _("First revision"),
    }

    MODEL_TO_STRING = {
        "ExternalReference": "External reference",
    }

    if text in ACTION_TO_LABEL:
        return ACTION_TO_LABEL[text]
    elif "&" in text:
        data = dict(parse_qsl(text))
        model = data["model"]
        action_type = data["action_type"]
        label = ACTION_TO_LABEL[action_type]
        model_type = ContentType.objects.get(
            app_label=data["app_label"], model=model.lower()
        )
        if model in MODEL_TO_STRING:
            model = MODEL_TO_STRING[model]
        model = ngettext(model, model, 1)
        relation_type = data.get("relation_type")

        def _link_to_object(related_object):
            return (
                f'<a href="{related_object.get_absolute_url()}">'
                f"{conditional_escape(related_object)}"
                "</a>"
            )

        if relation_type == "self":
            # Special case with symmetrical relations having a from/to parameter.
            pk_from = int(data["pk_from"])
            pk_to = int(data["pk_to"])

            try:
                if version_object.pk == pk_from:
                    related_object = model_type.get_object_for_this_type(pk=pk_to)
                    internal_reference = _("internal reference to")
                elif version_object.pk == pk_to:
                    related_object = model_type.get_object_for_this_type(pk=pk_from)
                    internal_reference = _("internal reference from")
            except model_type.model_class().DoesNotExist:
                # Dealing with deleted objects.
                return f"{label} {model}"

            related_object_str = _link_to_object(related_object)
            related_object_str = f"{internal_reference} {related_object_str}"
        else:
            pk = data["pk"]
            try:
                related_object = model_type.get_object_for_this_type(pk=pk)
            except model_type.model_class().DoesNotExist:
                # Dealing with deleted objects.
                return f"{label} {model}"

            if relation_type in ("m2m", "fk"):
                related_object_str = _link_to_object(related_object)
            elif not relation_type:
                # In case of a creation/deletion the object is already displayed,
                # for instance a City or a Keyword.
                related_object_str = ""
            else:
                related_object_str = str(related_object)

        return mark_safe(f"{label} {related_object_str}")
    else:
        return text
