from functools import partial

from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

import markdown
import bleach


register = template.Library()


@register.filter
def markdownify(text, custom_settings="default"):
    try:
        markdownify_settings = settings.MARKDOWNIFY[custom_settings]
    except (AttributeError, KeyError):
        markdownify_settings = {}

    # Bleach settings
    whitelist_tags = markdownify_settings.get(
        "WHITELIST_TAGS", bleach.sanitizer.ALLOWED_TAGS
    )
    whitelist_attrs = markdownify_settings.get(
        "WHITELIST_ATTRS", bleach.sanitizer.ALLOWED_ATTRIBUTES
    )
    whitelist_styles = markdownify_settings.get(
        "WHITELIST_STYLES", bleach.sanitizer.ALLOWED_STYLES
    )
    whitelist_protocols = markdownify_settings.get(
        "WHITELIST_PROTOCOLS", bleach.sanitizer.ALLOWED_PROTOCOLS
    )

    # Markdown settings
    strip = markdownify_settings.get("STRIP", True)
    extensions = markdownify_settings.get("MARKDOWN_EXTENSIONS", [])

    # Bleach Linkify
    linkify = None
    linkify_text = markdownify_settings.get("LINKIFY_TEXT", {"PARSE_URLS": True})
    if linkify_text.get("PARSE_URLS"):
        linkify_parse_email = linkify_text.get("PARSE_EMAIL", False)
        linkify_callbacks = linkify_text.get("CALLBACKS", [])
        linkify_skip_tags = linkify_text.get("SKIP_TAGS", [])
        linkifyfilter = bleach.linkifier.LinkifyFilter

        linkify = [
            partial(
                linkifyfilter,
                callbacks=linkify_callbacks,
                skip_tags=linkify_skip_tags,
                parse_email=linkify_parse_email,
            )
        ]

    # Convert markdown to html
    html = markdown.markdown(text or "", extensions=extensions)

    # Sanitize html if wanted
    if markdownify_settings.get("BLEACH", True):
        cleaner = bleach.Cleaner(
            tags=whitelist_tags,
            attributes=whitelist_attrs,
            styles=whitelist_styles,
            protocols=whitelist_protocols,
            strip=strip,
            filters=linkify,
        )

        html = cleaner.clean(html)

    return mark_safe(html)
