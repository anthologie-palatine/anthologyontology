#!/bin/bash

PROD_FOLDER=/docker/antholont/ap_production
PROD_COMPOSE_FILE=${PROD_FOLDER}/docker-compose.yml

PREPROD_FOLDER=/docker/antholont/ap_preprod
PREPROD_COMPOSE_FILE=${PREPROD_FOLDER}/docker-compose.yml

DB_USER=anthology_django
DB_NAME=anthology

set -ex

DUMP_DIR=/docker/antholont/dumps
mkdir -p ${DUMP_DIR}

# Export DB from prod
DUMP_FILE=${DUMP_DIR}/ap_prod_$(date +%s).sql
docker-compose -f ${PROD_COMPOSE_FILE} exec -T db pg_dump -U anthology_django anthology > ${DUMP_FILE}

# Import DB into preprod
# Drop the database and recreate it
docker-compose -f ${PREPROD_COMPOSE_FILE} exec -T db dropdb -U ${DB_USER} ${DB_NAME} || /bin/true
docker-compose -f ${PREPROD_COMPOSE_FILE} exec -T db createdb -U ${DB_USER} ${DB_NAME}

# Import the dump file
docker-compose -f ${PREPROD_COMPOSE_FILE} exec -T db psql -q -U ${DB_USER} ${DB_NAME} < ${DUMP_FILE} > /dev/null

# Run Django migrations
docker-compose -f ${PREPROD_COMPOSE_FILE} exec -T app python manage.py migrate

# Compress the dump file
gzip ${DUMP_FILE}