#!/bin/bash

set -uxe

# Set up folders if required
DEST_FOLDER=/docker/antholont/ap_${CI_COMMIT_BRANCH}
mkdir -p ${DEST_FOLDER}/persistent

# Copy source files

cp -rp django/ docker/ ${DEST_FOLDER}
cp docker/dockerignore ${DEST_FOLDER}/.dockerignore

# docker-compose.yml uses $CI_ENVIRONMENT_URL, which has https://domain.com/ format
# First we replace it using envsubst, then we strip the unnecessary characters
envsubst < docker-compose.yml > ${DEST_FOLDER}/docker-compose.yml
sed -i 's|http.\?://\([^/]\+\)/\?|\1|' ${DEST_FOLDER}/docker-compose.yml

# Rebuild/start the containers
docker-compose -f ${DEST_FOLDER}/docker-compose.yml up --build --remove-orphans -d