#!/bin/bash

set -eu

DOCKER_COMPOSE_FOLDER=/docker/antholont/ap_production
COMPOSE_FILE=${DOCKER_COMPOSE_FOLDER}/docker-compose.yml
DB_CONTAINER=db
DB_USER=anthology_django
DB_NAME=anthology

DUMP_FILE_NAME=ap-prod-db.sql.gz

DK_COMP=$(which docker-compose)


${DK_COMP} -f ${COMPOSE_FILE} exec -T ${DB_CONTAINER} pg_dump -U ${DB_USER} ${DB_NAME} | gzip > ${DUMP_FILE_NAME}
