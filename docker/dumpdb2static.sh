#!/bin/bash

set -eu

DOCKER_COMPOSE_FOLDER=/docker/antholont-prod/src
COMPOSE_FILE=docker-compose.prod.yml
DB_CONTAINER=db
DB_USER=anthology_django
DB_NAME=anthology
HTTP_CONTAINER=http

STATIC_FOLDER=/static
DUMP_FILE_NAME=__dump.sql.gz

DK_COMP=$(which docker-compose)

cd ${DOCKER_COMPOSE_FOLDER}
${DK_COMP} -f ${COMPOSE_FILE} exec ${DB_CONTAINER} pg_dump -U ${DB_USER} ${DB_NAME} | ${DK_COMP} -f ${COMPOSE_FILE} exec -T ${HTTP_CONTAINER} bash -c "gzip > ${STATIC_FOLDER}/${DUMP_FILE_NAME}"